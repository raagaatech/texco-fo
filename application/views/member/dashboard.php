<div class="parallax-container" style="background-color: rgba(0, 12, 78, 0.9)">
  <div class="row">
    <div class="container">
      <div class="col s12 m6 l6">
        <div class="pagebannertext white-text" >Dashboard</div>
      </div>
      <div class="col s12 m6 l6 right-align">
        <div class="dumheight hide-on-small-only"> </div>
        <div class=""> <a href="<?php echo base_url('member/dashboard')?>" class="breadcrumb">Home</a> <span class="breadcrumb">Dashboard</span> </div>
      </div>
    </div>
  </div>
  <div class="parallax"><img src="<?php echo base_url("assets/images/breadcrumbbanner.jpg")?>"></div>
</div>
 
