<script type="text/javascript">
var base_url = "<?php echo base_url()?>"
</script>
<script type="text/javascript">
var api_url = "<?php echo config_item('api_url')?>"
</script>
<script type="text/javascript">var atoken = "<?php echo $this->session->usertoken('atoken')?>"</script>
<script type="text/javascript" src="<?php echo base_url("assets/js/lib/jquery-2.1.1.min.js")?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/lib/angular.js")?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/app/printbulkprint.js")?>"></script>
<script type="text/javascript">
var fromdate = "<?php echo $_GET['fromdate'];?>";
var todate = "<?php echo $_GET['todate'];?>";
var regionid = "<?php echo $_GET['regionid'];?>";
var type = "<?php echo $_GET['type'];?>"
</script>
<link href="<?php echo base_url("assets/css/texco.css")?>" rel="Stylesheet" type="text/css" />
<div ng-app="appBulkPrint" ng-controller="ctrlBulkPrint">
    <!-- Salary Bill Starting -->
    <div ng-if="printtype == 1"> 
        <style> 
            @page {
                size: 15in 12in;
                margin: 27mm 16mm 27mm 16mm;
            }
            table { page-break-inside:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group } 
            table {
                -fs-table-paginate: paginate;
            }
            thead.report-header {
                display: table-header-group;
            }
        </style>
        <div ng-repeat="invoiceprint in printdata">
            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="1"
                style="page-break-before: always; page-break-after: always;border: 1;">
                <tbody>
                    <tr>
                        <td width="100%" align="center">
                         <!--   <table width="99%" align="center" cellpadding="0" cellspacing="0" border="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" align="center">
                                            <img style="height: 50%;margin-top: 5px;"
                                                src="<?php echo base_url("assets/images/clientletter.jpg")?>">
                                        </td>
                                        <td width="100%" align="center">
                                            <b>TAMILNADU EX-SERVICEMEN’S CORPORATION LIMITED (TEXCO)</b><br>
                                            <b>(A Government of Tamil Nadu Undertaking)</b><br>
                                            <b>Major Parameswaran Memorial Building,</b><br>
                                            <b>No.2, West Mada Street, Srinagar Colony, Saidapet, Chennai-600
                                                015.</b><br>
                                            <b>Phone: 044-22352947, 22301792, 22301793, 22350900 Fax:
                                                044-22301791</b><br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr /> -->
                            <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <div ng-init="type=1"></div>
                                        <div ng-show="invoiceprint[0].ssprintcount > 0"> Duplicate Copy</div>
                                        <div ng-show="invoiceprint[0].ssprintcount == 0">Original Copy</div> <br>
                                        CONSOLIDATED PAYMENT TO TEXCO PERSONNEL IN THE PROJECT OF
                                        {{invoiceprint[0].projectname}} FOR THE PERIOD OF
                                        {{invoiceprint[0].monthandyear}}
                                        <br>
                                    </tr>
                                    <br>
                                    <tr>
                                        <td align="left"><b>{{invoiceprint[0].wagetype}} {{invoiceprint[0].wageyear}}
                                            </b>
                                        </td>
                                        <td align="right" style="font-size: 13px;">
                                            <b> Project Address : </b>
                                            <span
                                                ng-hide="invoiceprint[0].designation == '' || invoiceprint[0].designation == null">
                                                &nbsp; &nbsp; &nbsp;{{invoiceprint[0].designation}},
                                            </span>
                                            <br>
                                            <span
                                                ng-hide="invoiceprint[0].addressline1 == '' || invoiceprint[0].addressline1 == null">{{invoiceprint[0].addressline1}}</span>
                                            <span
                                                ng-hide="invoiceprint[0].addressline2 == '' || invoiceprint[0].addressline2 == null">,
                                                <br>&nbsp; &nbsp; &nbsp;{{invoiceprint[0].addressline2}} </span>
                                            <span
                                                ng-hide="invoiceprint[0].addressline3 == '' || invoiceprint[0].addressline3 == null">,
                                                <br>&nbsp; &nbsp; &nbsp;{{invoiceprint[0].addressline3}} </span> <span
                                                ng-hide="invoiceprint[0].pincode == '' || invoiceprint[0].pincode == null">
                                                - {{invoiceprint[0].pincode}}&nbsp; &nbsp; </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <strong>Payslip No &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; :
                                                &nbsp;</strong>{{invoiceprint[0].payslipno}}
                                            <br>
                                            <strong>Project No &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; :
                                                &nbsp;</strong>{{invoiceprint[0].projectno}}
                                            <br>
                                            <strong>Date of Recp &nbsp; &nbsp; &nbsp;:
                                                &nbsp;</strong><?php echo  date("d-m-Y");?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table style="border-collapse: collapse;" border="1" width="99%" cellspacing="0"
                                cellpadding="0" align="center">
                                <thead style="font-size: 14px;" class="report-header">
                                    <tr height="30px">
                                        <td align="left"><strong>&nbsp;S.No</strong></td>
                                        <td align="left"><strong>&nbsp;Texco/ UAN No / Service No</strong></td>
                                        <td align="left"><strong>&nbsp;Employee's Name / &nbsp; Bank A/C No</strong>
                                        </td>
                                        <td align="left"><strong>&nbsp;Days</strong></td>
                                        <td align="left"><strong>&nbsp;Basic + <br>&nbsp;Vda</strong></td>
                                        <td align="left"><strong>&nbsp;ED Days</strong></td>
                                        <td align="left"><strong>&nbsp;ED Amt</strong></td>
                                        <td align="left"><strong>&nbsp;Hra</strong></td>
                                        <td align="left"><strong>&nbsp;Medical</strong></td>
                                        <td align="left"><strong>&nbsp;Uniform</strong></td>
                                        <td align="left"><strong>&nbsp;Leave Reser</strong></td>
                                        <td align="left"><strong>&nbsp;Bon/Inct</strong></td>
                                        <td align="left"><strong>&nbsp;Wash Allw</strong></td>
                                        <td align="left"><strong>&nbsp;Others</strong></td>
                                        <td align="left"><strong>&nbsp;Gross</strong></td>
                                        <td align="left"><strong>&nbsp;Epf 12 %</strong></td>
                                        <td align="left"><strong>&nbsp;Others</strong></td>
                                        <td align="left"><strong>&nbsp;Total</strong></td>
                                        <td align="left"><strong>&nbsp;Net Pay</strong></td>
                                        <!-- <td align="left" ><strong>&nbsp;Bank A/C No</strong></td> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="x in invoiceprint" style="font-size: 14px;">
                                        <td align="center">&nbsp;{{$index +1}}</td>
                                        <td align="center">
                                            &nbsp;{{x.texcono}}&nbsp;
                                            <hr>
                                            &nbsp;{{x.uanno}}&nbsp;
                                            <hr>
                                            &nbsp;{{x.serviceno}}&nbsp;
                                        </td>
                                        <td><span
                                                class="capitalize">&nbsp;{{x.firstname}}</span>&nbsp;{{x.jobcode}}<br>&nbsp;Basic
                                            = ({{x.ncbasic | number:2}})
                                            <hr>&nbsp;{{x.accountno}}</td>
                                        <td align="center">&nbsp;{{x.presentdays}}</td>
                                        <td align="right">&nbsp;{{x.basic | number:2}}&nbsp; &nbsp;</td>
                                        <td align="center">&nbsp;<span ng-show="x.jobcode!='DVR'"> {{x.eddays}}</span>
                                            <span ng-show="x.jobcode=='DVR'"> {{x.othours/8 |  number:0}}&nbsp;
                                                &nbsp;</span> </td>
                                        <td align="right">&nbsp;{{x.edamount | number:2}}&nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.hra | number:2}}&nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.ma | number:2}} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.unifdt | number:2}} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.leapay | number:2}} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.bonus | number:2}} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.washallow | number:2}} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.other1 + x.otherallowance | number:0}} &nbsp; &nbsp;
                                        </td>
                                        <td align="right">&nbsp;{{x.gross | number:0 }} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.epf | number:0}} &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.other2 + x.otherdeductions | number:2}} &nbsp;
                                            &nbsp;</td>
                                        <td align="right">&nbsp;{{x.epf + x.other2 + x.otherdeductions | number:0}}
                                            &nbsp; &nbsp;</td>
                                        <td align="right">&nbsp;{{x.netpay | number:0}}&nbsp; &nbsp;</td>
                                    </tr>
                                    <tr style="page-break-inside: avoid;">
                                        <td style="text-align: center;" colspan="3">Total</td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].presentdays}}</b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].basic | number:2}}&nbsp; &nbsp;</b></td>
                                        <td><b>&nbsp; {{invoiceprint[0].total[0].eddays}} </b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].edamount | number:2}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].hra | number:2}}&nbsp; &nbsp;</b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].ma | number:2}}&nbsp; &nbsp;</b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].unifdt | number:2}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].leapay | number:2}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].bonus | number:2}}&nbsp; &nbsp;</b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].washallow | number:2}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].other1 | number:2}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].totalgross | number:0 }}&nbsp;
                                                &nbsp;</b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].epf | number:0}}&nbsp; &nbsp;</b></td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].other2 | number:2}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].totaldeduc | number:0}}&nbsp; &nbsp;</b>
                                        </td>
                                        <td><b>&nbsp;{{invoiceprint[0].total[0].netpay | number:0}}&nbsp; &nbsp;</b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table>
                                <tr>
                                    <td>
                                        (*) Gross = {{invoiceprint[0].total[0].totalgross | number:2}}/- Total
                                        Deductions = {{invoiceprint[0].total[0].totaldeduc | number:2}}/- Net =
                                        {{invoiceprint[0].total[0].netpay | number:2}}/-
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        (*) {{invoiceprint[0].total[0].inwords}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- (*) Basic Pay   SG - 3682   HSG - 4897    ASO - 6259  -->
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>
                                        Certified that the :-
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        (a) Wages have been prepared strictly as per the certified attendance received
                                        from the project and not paid previously
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        (b) Bonus paid along with monhtly wages w.e.f Oct-Nov'99
                                    </td>
                                </tr>
                            </table>
                            <br>

                            <br>
                            <br>
                            <br>
                            <table>
                                <tr>
                                    <td>
                                        Project Clerk
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        Accountant
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        Accounts Officer
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <span style="text-align:center;">*** Computer-generated document, Signature is Not
                                required.</span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br><br><br>
            <table width="97%" align="center" cellpadding="0" cellspacing="0" border="1"
                style="border-collapse: collapse">
                <tbody>
                    <tr>
                        <td width="100%" align="center">
                        <!--<table width="99%" align="center" cellpadding="0" cellspacing="0" border="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" align="center">
                                            <img style="height: 50%;margin-top: 5px;"
                                                src="<?php echo base_url("assets/images/clientletter.jpg")?>">
                                        </td>
                                        <td width="100%" align="center">
                                            <b>TAMILNADU EX-SERVICEMEN’S CORPORATION LIMITED (TEXCO)</b><br>
                                            <b>(A Government of Tamil Nadu Undertaking)</b><br>
                                            <b>Major Parameswaran Memorial Building,</b><br>
                                            <b>No.2, West Mada Street, Srinagar Colony, Saidapet, Chennai-600
                                                015.</b><br>
                                            <b>Phone: 044-22352947, 22301792, 22301793, 22350900 Fax:
                                                044-22301791</b><br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                            <hr /> -->
                            <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <div ng-init="type=2"></div>
                                        <div ng-show="invoiceprint[0].pfprintcount>0"> Duplicate Copy</div>
                                        <div ng-show="invoiceprint[0].pfprintcount==0">Original Copy</div> <br>
                                        RESERVE FUND STATEMENT TO TEXCO WORKERS IN THE PROJECT OF
                                        {{invoiceprint[0].projectname}} FOR THE MONTH OF
                                        {{invoiceprint[0].monthandyear}}
                                        <br>
                                    </tr>
                                    <br>
                                    <td>
                                        <strong>Project No : </strong>{{invoiceprint[0].projectno}}
                                        <br>
                                        <strong>Date of Recp : </strong><?php echo  date("d-m-Y");?>
                                    </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table style="border-collapse: collapse;" border="1" width="97%" cellspacing="0" cellpadding="0"
                align="center">
                <tbody style="font-size: 14px;">
                    <tr height="30px">
                        <td align="left"><strong>&nbsp;S.No</strong></td>
                        <td align="left"><strong>&nbsp;TEXCO/ UAN No</strong></td>
                        <!--   <td align="left" ><strong>&nbsp;UAN No</strong></td> -->
                        <td align="left"><strong>&nbsp;EMPLOYEE'S NAME</strong></td>
                        <td align="left"><strong>&nbsp;DESIGNATION</strong></td>
                        <td align="left"><strong>&nbsp;DAYS WORKED</strong></td>
                        <td align="left"><strong>&nbsp;BASIC Pay</strong></td>
                        <td align="left"><strong>&nbsp;EMPLOYER PF 12 %</strong></td>
                        <td align="left"><strong>&nbsp;EMPLOYEE PF 12 %</strong></td>
                    </tr>
                    <tr ng-repeat="x in invoiceprint" style="font-size: 14px;">
                        <td style="text-align: center;">&nbsp;{{$index +1}}</td>
                        <td align="center">
                            &nbsp;{{x.texcono}}&nbsp;
                            <hr>
                            {{x.uanno}}&nbsp;
                        </td>
                        <!--  <td>&nbsp;{{x.uanno}}</td> -->
                        <td class="capitalize">&nbsp;{{x.firstname}}</td>
                        <td style="text-align: center;">&nbsp;{{x.jobcode}}</td>
                        <td align="center">&nbsp;{{x.presentdays}}</td>
                        <td align="right">&nbsp;{{x.basic | number:2}}&nbsp;&nbsp;</td>
                        <td align="right">&nbsp;{{x.epf | number:2}} &nbsp;&nbsp;</td>
                        <td align="right">&nbsp;{{x.epf | number:2}} &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: center;" colspan="4">Total</td>
                        <td align="center"><b>&nbsp;{{invoiceprint[0].total[0].presentdays}}&nbsp;&nbsp;</b></td>
                        <td align="right"><b>&nbsp;{{invoiceprint[0].total[0].basic | number:2}}&nbsp;&nbsp;</b></td>
                        <td align="right"><b>&nbsp;{{invoiceprint[0].total[0].epf | number:2}}&nbsp;&nbsp;</b></td>
                        <td align="right"><b>&nbsp;{{invoiceprint[0].total[0].epf | number:2}}&nbsp;&nbsp;</b></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table>
                <tr>
                    <td>
                        GRAND TOTAL : Rs. {{invoiceprint[0].total[0].epf | number:2}}
                    </td>
                </tr>
            </table>
            <br>
            <span style="padding-right: 900px;">*** Computer-generated document, Signature is Not required.</span>
            </td>
            </tr>
            </tbody>
            </table>
            <br><br><br>
        </div>
    </div>
    <!-- Salary Bill Ending -->
    <!--  Salary Claim Starting-->
    <div ng-if="printtype == 2">
        <style> 
            @page {
                size: 10in 12in;
                margin: 27mm 16mm 27mm 16mm;
            }
            table { page-break-inside:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group } 
            table {
                -fs-table-paginate: paginate;
            }
        </style>
        <div ng-repeat="invoiceprint in printdata">
            <table width="97%" align="center" cellpadding="0" cellspacing="0" border="1"
                style="border-collapse: collapse;page-break-after:always;margin-top:2% !important;"
                ng-repeat="l in arlength">
                <tbody>
                    <td width="100%" align="center">
                       <!-- <table width="99%" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td width="20%" align="center">
                                        <img style="height: 50%;margin-top: 5px;"
                                            src="<?php echo base_url("assets/images/clientletter.jpg")?>">
                                    </td>
                                    <td width="100%" align="center">
                                        <b>TAMILNADU EX-SERVICEMEN’S CORPORATION LIMITED (TEXCO)</b><br>
                                        <b>(A Government of Tamil Nadu Undertaking)</b><br>
                                        <b>Major Parameswaran Memorial Building,</b><br>
                                        <b>No.2, West Mada Street, Srinagar Colony, Saidapet, Chennai-600 015.</b><br>
                                        <b>Phone: 044-22352947, 22301792, 22301793, 22350900 Fax: 044-22301791</b><br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr /> -->
                        <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td colspan='2' align="center">
                                        <div ng-show="invoiceprint[0].printcount>0"> Duplicate Copy</div>
                                        <div ng-show="invoiceprint[0].printcount==0">Original Copy</div>
                                    </td>
                                </tr>
                                <tr>
                                    <tr>
                                        <br>
                                        <td align="left"><b>Bill No : </b>{{invoiceprint[0].invoiceno}}</td>
                                        <td align="right"> {{invoiceprint[0].createddate}} </td>
                                    </tr>
                                <tr>
                                    <td>
                                        <strong>Month and Year : </strong>{{invoiceprint[0].monthandyear}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Project No :</strong>{{invoiceprint[0].projectno}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <strong>To:</strong>
                                    </td>
                                </tr>
                                <tr ng-hide="invoiceprint[0].designation == '' || invoiceprint[0].designation == null">
                                    <td style="font-size: 15px;" width="50%" align="left">
                                        &nbsp; &nbsp; &nbsp;{{invoiceprint[0].designation}},<br>
                                        &nbsp; &nbsp; &nbsp;<span
                                            ng-hide="invoiceprint[0].addressline1 == '' || invoiceprint[0].addressline1 == null">{{invoiceprint[0].addressline1}}</span>
                                        <span
                                            ng-hide="invoiceprint[0].addressline2 == '' || invoiceprint[0].addressline2 == null">,<br>&nbsp;
                                            &nbsp; &nbsp;{{invoiceprint[0].addressline2}} </span>
                                        <span
                                            ng-hide="invoiceprint[0].addressline3 == '' || invoiceprint[0].addressline3 == null">,<br>&nbsp;
                                            &nbsp; &nbsp;{{invoiceprint[0].addressline3}} </span><span
                                            ng-hide="invoiceprint.pincode == '' || invoiceprint[0].pincode == null"> -
                                            {{invoiceprint[0].pincode}} </span>
                                    </td>
                                    <td style="font-size: 15px;" width="50%" align="right">
                                        TEXCO is exempted from the
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>deduction of Income Tax(TDS)
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> Vide CBDT Circular No. 07/2015
                                        &nbsp;&nbsp;<br> Dt. 23.04.2015 of ministry of
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> finance, Department
                                        od Revenue &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody style="text-align: justify;">
                                <tr>
                                    <td>
                                        <br />
                                        <strong>Customer GST IN : </strong>
                                    </td>
                                </tr>
                                <tr style="text-align: center;">
                                    <td>
                                        <br />
                                        <strong>TAX INVOICE</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; TEXCO - Claim for the month of
                                        {{invoiceprint[0].monthandyear}} - {{invoiceprint[0].projectname}}
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        &nbsp; 1. As per the attendance received for the above period towards deployed
                                        at {{invoiceprint[0].projectname}} invoice as follows
                                        <br />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <table width="95%" align="center" cellpadding="0" cellspacing="2" border="0">
                            <tbody>
                                <tr>
                                    <td valign="top" width="100%">
                                        <table style="border-collapse: collapse;" border="1" width="99%" cellspacing="0"
                                            cellpadding="0" align="center">
                                            <tbody>
                                                <tr height="30px">
                                                    <td align="left"><strong>&nbsp;S.No</strong></td>
                                                    <td align="left"><strong>&nbsp;Description</strong></td>
                                                    <td style="text-align:center"><strong>&nbsp;No of Person</strong>
                                                    </td>
                                                    <td align="left"><strong>&nbsp;Rate</strong></td>
                                                    <td style="text-align:center"><strong>&nbsp;No Of Duties</strong>
                                                    </td>
                                                    <td style="text-align:center"><strong>&nbsp;Rate P.M/Per</strong>
                                                    </td>
                                                    <td align="left"><strong>&nbsp;Total (Rs.)</strong></td>
                                                </tr>
                                                
                                                <tr ng-repeat="l in invoiceprint[0].totalduties">
                                                    <td>&nbsp;<span ng-show="l.name">{{$index+1}}</span></td>
                                                    <td>&nbsp;<span ng-show="l.name">{{l.name}} -
                                                            {{l.jobmastercode}}&nbsp;</span>
                                                        <br>
                                                        <span ng-show="l.jobmastercode=='DVR'"
                                                            ng-repeat="d in invoiceprint.drivers">
                                                            <br>
                                                            &nbsp;{{d.firstname}} - {{d.texcono}}
                                                        </span>
                                                    </td>
                                                    <td style="text-align:center"><span
                                                            ng-show="l.name">&nbsp;{{l.noofperson}}</span></td>
                                                    <td>&nbsp;<span ng-show="l.name">{{l.salary | number:2}}</span></td>
                                                    <td style="text-align:center">&nbsp;<span
                                                            ng-show="l.name">{{l.noofduties}}</span></td>
                                                    <td style="text-align:center">&nbsp;<span
                                                            ng-show="l.name">({{l.salary  | number:2}} *
                                                            {{l.noofduties}}) / {{l.days}} </span> </td>
                                                    <td style="text-align: right;">&nbsp;<span
                                                            ng-show="l.name">{{l.salaryamount | number:2}} &nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="8">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="border-collapse: collapse;" border="1" width="99%" cellspacing="0"
                                            cellpadding="0" align="center">
                                            <tbody>
                                                <tr style="page-break-after:always">
                                                    <td style="text-align: center;" colspan="6"><strong>Sub
                                                            Total</strong></td>
                                                    <td style="text-align: right;">
                                                        {{invoiceprint[0].subtotal | number:2}}
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="6"><strong>Service Charge
                                                            {{invoiceprint[0].servicecharge | number:2}} % </strong>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        {{invoiceprint[0].servicecharges  | number:2 }} &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="6"><strong>Total</strong>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        {{(invoiceprint[0].subtotal + invoiceprint[0].servicecharges)| number:2}}
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="6"><strong><strong>CGST
                                                                {{invoiceprint[0].tax/2 | number:2}} %</strong></td>
                                                    <td style="text-align: right;">
                                                        {{invoiceprint[0].servicetax/2 | number:2}} &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="6"><strong><strong>SGST
                                                                {{invoiceprint[0].tax/2 | number:2}} %</strong></td>
                                                    <td style="text-align: right;">
                                                        {{invoiceprint[0].servicetax/2 | number:2}} &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="6"><strong><strong>IGST 0
                                                                %</strong> </td>
                                                    <td style="text-align: right;">{{0 | number:2}} &nbsp; </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;" colspan="6"><strong>Grand
                                                            Total</strong>
                                                    </td>
                                                    <td style="text-align: right;"><strong>Rs.
                                                            {{invoiceprint[0].subtotal + invoiceprint[0].servicecharges +  invoiceprint[0].servicetax | number:2}}
                                                            &nbsp;</strong>
                                                        <br>
                                                        (or)&nbsp;&nbsp;
                                                        <br>
                                                        <strong>Rs. {{invoiceprint[0].total | number:2}} &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody style="text-align: justify;">
                                <tr>
                                    <td>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;GST Regn No. 33AAACT2912M1ZG&nbsp; &nbsp; PAN
                                        No. AAACT2912M &nbsp; &nbsp; CIN No. U70101TN1986SGC012609 &nbsp; &nbsp; </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; 2. &nbsp;GST has been claimed as per the Central &
                                        State GST Act
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; 3.&nbsp; The bill may be passed and cheque forwarded
                                        at the earliest
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <br />
                        <table width="95%" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%">
                                    <table>
                                        <br /><br>
                                        <tr></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr width="95%">
                                <td width="30%" align="left">
                                    <b>&nbsp;&nbsp;&nbsp;E. & O. E.</b>
                                </td>
                                <td width="70%" align="right">
                                    <b>Chief Accounts officer, TEXCO&nbsp;&nbsp;&nbsp;</b>
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </tbody>
            </table>

            <table width="97%" align="center" cellpadding="0" cellspacing="0" border="1"
                style="border-collapse: collapse;page-break-after:always;margin-top:2% !important;">
                <tbody>
                    <tr>
                        <td width="100%" align="center">
                            <!-- <table width="99%" align="center" cellpadding="0" cellspacing="0" border="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" align="center">
                                            <img style="height: 50%;margin-top: 5px;"
                                                src="<?php echo base_url("assets/images/clientletter.jpg")?>">
                                        </td>
                                        <td width="100%" align="center">
                                            <b>TAMILNADU EX-SERVICEMEN’S CORPORATION LIMITED (TEXCO)</b><br>
                                            <b>(A Government of Tamil Nadu Undertaking)</b><br>
                                            <b>Major Parameswaran Memorial Building,</b><br>
                                            <b>No.2, West Mada Street, Srinagar Colony, Saidapet, Chennai-600
                                                015.</b><br>
                                            <b>Phone: 044-22352947, 22301792, 22301793, 22350900 Fax:
                                                044-22301791</b><br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr /> -->
                            <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tbody>

                                    <tr>
                                        <div ng-init="type=3"></div>

                                        <td align="left"><b>Bill No:</b>{{invoiceprint[0].invoiceno}}
                                        </td>
                                        <td align="right"><b>Dated On:</b> {{invoiceprint[0].createddate}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tbody style="text-align: justify;">
                                    <tr style="text-align: center;">
                                        <td colspan='2' align="center">
                                            <div ng-show="invoiceprint[0].printcount>0"> Duplicate Copy</div>
                                            <div ng-show="invoiceprint[0].printcount==0">Orginal Copy</div>
                                        </td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td>
                                            <br />
                                            <strong>ADVANCE RECEIPT</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <br />
                                            &nbsp; &nbsp; &nbsp; &nbsp; Received a sum of Rs.
                                            {{invoiceprint[0].total | number:2}} only from the
                                            {{invoiceprint[0].projectname}}
                                            towards the above Job Category service chanrges for the period
                                            {{invoiceprint[0].monthandyear}} against the bill no
                                            {{invoiceprint[0].invoiceno}}
                                            for personnel deployed at the {{invoiceprint[0].projectname}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br />
                            <table width="95%" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="50%">
                                        <table>
                                            <br />
                                            <br>
                                            <tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%" align="left">
                                        <b>E. & O. E.</b>
                                    </td>
                                    <td width="70%" align="right">
                                        <b>Chief Accounts officer, TEXCO</b>
                                    </td>
                                    <!-- <td width="100%">
                        <table>
                           <br />
                           <br />
                           <br />
                           <br />
                           <tr>
                              <td width="30%" align="left">
                                 <b>E. & O. E.</b>
                              </td>
                              <td width="70%" align="right">
                                 <b>Chief Accounts officer, TEXCO</b>
                              </td>
                           </tr>
                        </table>
                     </td> -->
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Salary Claim Ending -->
</div>
<script>
(function() {
    var beforePrint = function() {
        //alert('Functionality to run before printing.');
    };

    var afterPrint = function() {
        //  alert('Functionality to run after printing');
        var controllerElement = document.querySelector('[ng-controller="ctrlPrint"]');
        angular.element(controllerElement).scope().printcompleted();
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function(mql) {
            // alert($(mediaQueryList).html());
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }
    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;
}());
</script>