// Initialize the Express
var express = require('express');
var expressApp = express();
var bodyParser = require('body-parser');
var bluebird = require('bluebird');
var responseTime = require("response-time");
var nconf = require('./Utils/EnvironmentUtil');


expressApp.use(bodyParser.json({
	limit: '100mb'
})); 

expressApp.use(bodyParser.urlencoded({
	limit: '100mb',
	extended: true,
	parameterLimit: 100000000
}));

//loading the body parser

expressApp.all('/', function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization");
	res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	next();
});

expressApp.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization");
	res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	next();
});

//Basic Route
expressApp.get('/', function (req, res) {
	res.json({
		test: 'Welcome to Texco!'
	});
});

module.exports.express = expressApp;
module.exports.promise = bluebird;

//Loading the App object to run all the services.
require('./Service/services')({
	express: expressApp,
	promise: bluebird
});

//Starting the Server
var server = expressApp.listen(nconf.get('APP_PORT'), function () {
	var host = server.address().address
	var port = server.address().port
	console.log("server listening at http://%s:%s", host, port)
});
