var crypto=require("crypto");
var nconf= require("./EnvironmentUtil");
//var passwordHash=require("password-hash");

var cryptoInfo={
//module.exports.hashSha256=function(secretKey,data)
            hashSha256 : function(data)
            {
              var secretKey=nconf.get("PASSWORD_HASH_KEY");
              return crypto.createHmac("sha256",secretKey).update(data).digest("hex");
            },
            encryptAes : function(secretKey,data)
            {
              var cipher= crypto.createCipher("aes-256-ctr",secretKey);
              var encryptData= cipher.update(data,"utf8","hex");
              encryptData +=cipher.final("hex");
              return encryptData;
            },
            dercyptAes: function (secretKey,data)
            {
                var decipher = crypto.createDecipher("aes-256-ctr",secretKey)
                var decryptData = decipher.update(data,'hex','utf8')
                decryptData += decipher.final('utf8');
                return decryptData;
            }
            /*module.exports.hashPassword=function(data)
            {
              return passwordHash.generate(data,{algorithm:'sha256',saltLength:10});
            }
            */
}
//Export the class
module.exports=cryptoInfo;
