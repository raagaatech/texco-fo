﻿var app = require('./../app');
var fs = require('fs');  
var jobBal = require('./../BAL/jobBAL');
var jobModel = require('./../Model/job');
var path = require('path');
var mime = require('mime'); 
const XmlReader = require('xml-reader');
var XMLWriter = require('xml-writer');
var js2xmlparser = require("js2xmlparser");
var nconf = require('./../Utils/EnvironmentUtil')
var moment = require('moment');
var settingBal = require('./../BAL/settingBAL');



module.exports = function (app) {

	/*
	     GET Method to get closedate
	*/ 
	app.express.get('/api/job/jobposting/updateinactivedate', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updateinactivedate(request.query.inactivedate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method to Close vacancy list
	*/
	app.express.get('/api/job/jobposting/close', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.closejobpostingdetail().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json("Vacancy Closed Succesfully");
					} else {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json("Vacancy Already Closed");
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     GET Method to get closedate
	*/
	app.express.get('/api/job/jobposting/closedate', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getclosedate().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method to export vacancy list
	*/
	app.express.get('/api/job/export/jobposting', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.exportvacancy().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to Import Data
	*/
	app.express.post('/api/job/import/jobposting/', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.importvacancy(request.body.vacancy).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobactivity
	*/  
	app.express.get('/api/job/jobactivity/applied', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobapplied(request.query.closedate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobactivity by memberid
	*/
	app.express.get('/api/job/jobactivity/member', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobappliedformember(request.query.memberid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobactivity by clientid
	*/
	app.express.get('/api/job/jobactivity/client', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobappliedforclient(request.query.clientid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});


	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/jobposting/detail', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobpostingdetail(request.query.jobpostingdetailid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to create jobmaster
	*/
	app.express.post('/api/job/jobmaster', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var jobmaster = new jobModel.jobmaster(request.body.code, request.body.name, request.body.workinghours, request.body.monthlywages, request.body.servicecharge, request.body.servicetax, request.body.comments, request.body.changedby);
				jobBal.createjobmaster(jobmaster).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Put Method to update jobmaster
	*/
	app.express.put('/api/job/jobmaster', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var jobmaster = new jobModel.jobmaster(request.body.code, request.body.name, request.body.workinghours, request.body.monthlywages, request.body.servicecharge, request.body.servicetax, request.body.comments, request.body.changedby);
				jobBal.updatejobmaster(jobmaster, request.body.jobmasterid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});


	/*
	     get jobmaster
	*/
	app.express.get('/api/job/jobmaster/', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobmaster(0).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     get jobmaster
	*/
	app.express.get('/api/job/jobmaster/:id', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobmaster(request.params.id).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Delete Method to delete
	*/
	app.express.delete('/api/job/jobmaster', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatejobmasterstatus(request.body.jobmasterid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	    Post Method to invoice list
	*/
	app.express.get('/api/job/invoiceList', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getprojectInvoiceList().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/finance/invoiceListXML', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getprojectInvoiceListXML(request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
        Post Method to invoice list
    */
	app.express.get('/api/job/ApprovedInvoices', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getApprovedInvoicesCount().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/TotalInvoices', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getTotalInvoicesCount().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/PendingInvoiceAmount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getPendingInvoiceAmount().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/TotalInvoiceAmount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getTotalInvoiceAmount().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to create jobposting
	*/
	app.express.post('/api/job/jobposting', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var jobposting = new jobModel.jobposting(request.body.clientid, request.body.projectid, request.body.jobs, request.body.changedby, request.body.startdate);
				jobBal.createjobposting(jobposting).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     put Method to update jobposting
	*/
	app.express.put('/api/job/jobposting', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatejobposting(request.body.jobs, request.body.changedby, request.body.jobpostingid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update jobposting
	*/
	app.express.delete('/api/job/jobposting', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatejobpostingstatus(request.body.jobpostingid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/jobposting/', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobposting(0).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/jobposting/:jobpostingid', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobposting(request.params.jobpostingid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});	

	/*
	     Post Method to create jobschedule
	*/
	app.express.post('/api/job/jobschedule', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.createjobschedule(request.body.jobschedule, request.body.changedby).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to create jobactivity
	*/
	app.express.post('/api/job/jobactivity', function (request, response) {
		var fs = require("fs");
        var filepath = nconf.get('VACANCYERRORURL');
		var datas = " Date - "+ new Date() + "  - Before Applying Job Service Call - Before Success - JobService.JS - IP - "+request.body.ipaddress+" Token Value - " +request.headers['authorization']+" - texcono - "+request.body.memberid;
		var stream = fs.createWriteStream(filepath, {'flags': 'a'});
			stream.once('open', function(fd) {
			stream.write(datas);
		}); 
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var jobactivity = new jobModel.jobactivity(request.body.jobpostingdetailid, request.body.memberid, request.body.clientid, request.body.projectid, "", "", "", "", request.body.changedby); 
				jobBal.createjobactivity(jobactivity, request.body.code, request.body.inplace, request.body.othercat, request.body.ipaddress).then(function (result) { 
					var fs = require("fs");
					var filepath = nconf.get('VACANCYERRORURL');
					var datas = " Date - "+ new Date() + "  - After Applying Job Service Call - After Success - JobService.JS - IP - "+request.body.ipaddress+" Token Value - " +request.headers['authorization']+" - texcono - "+request.body.memberid;
					var stream = fs.createWriteStream(filepath, {'flags': 'a'});
						stream.once('open', function(fd) {
						stream.write(datas);
					}); 
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					} 
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update jobactivity
	*/
	app.express.post('/api/job/jobactivity/update', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatejobactivity(request.body.jobactivityid, request.body.jobstatusid, request.body.comments).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
         Get Method for Vacancy list
    */
	app.express.get('/api/job/toplist', function (request, response) {  
		var fs = require("fs");
        var filepath = nconf.get('ERRORURL');
		var datas = " Date - "+ new Date() + "  -  Home Screen Backend Vacancy List\r\n";
		var stream = fs.createWriteStream(filepath, {'flags': 'a'});
			stream.once('open', function(fd) {
			stream.write(datas);
		}); 
		settingBal.getAuthorizationTokenAdd(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjoblist().then(function (result) {
					if (result) {  
						var fs = require("fs");
						var filepath = nconf.get('ERRORURL');
						var datas = " Date - "+ new Date() + "  -  Home Screen Vacancy List Backend Success\r\n";
						var stream = fs.createWriteStream(filepath, {'flags': 'a'});
							stream.once('open', function(fd) {
							stream.write(datas);
						}); 
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
         Get Method to get all Vacancy list
    */ 
	app.express.get('/api/job/list', function (request, response) { 
		var fs = require("fs");
        var filepath = nconf.get('VACANCYERRORURL');
		var datas = " Date - "+ new Date() + "  - Before Vacancy Service Call - JobService.JS  \r\n Token Value - " +request.headers['authorization'];
		var stream = fs.createWriteStream(filepath, {'flags': 'a'});
			stream.once('open', function(fd) {
			stream.write(datas);
		}); 
		settingBal.getAuthorizationTokenAdd(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getalljoblist().then(function (result) {  
					var fs = require("fs");
					var filepath = nconf.get('VACANCYERRORURL');
					var datas = " Date - "+ new Date() + "  - After Vacancy Success Service Call - JobService.JS  \r\n";
					var stream = fs.createWriteStream(filepath, {'flags': 'a'});
						stream.once('open', function(fd) {
						stream.write(datas);
					}); 
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method postedlist
	*/
	app.express.get('/api/job/postedlist', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getpostedlist().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update jobactivity
	*/
	app.express.post('/api/job/jobactivity/status', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatejobactivitystatus(request.body.jobactivityid, request.body.memberid, request.body.jobcode, request.body.confirmdate, request.body.isrejected).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					} 
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
         Get Method for print
    */
	app.express.get('/api/job/apply/print', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) { 
				console.log('request.headers',request.headers['authorization']);
				jobBal.getjobapplyprint(request.query.jobactivityid, request.query.memberid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for job posting print
	*/
	app.express.get('/api/job/posting/print', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobpostingprint(request.query.jobactivityid, request.query.memberid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/apply/printpost', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobpostingprintpost(request.query.memberhistoryid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});
	
	/*
	     Get Method for closedate
	*/
	app.express.get('/api/job/jobpostingdate/close', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobpostingdate().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for jobdetails  weekly update for graph
	*/
	app.express.get('/api/job/recruitment', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getrecruitment().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/ClientsCount', function (request, response) { console.log('request.headers',request.headers);
		jobBal.getClientCount().then(function (result) {
			if (result) {
				response.set('Content-Type', 'application/json');
				response.status(200);
				response.json(result);
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/TotalInvoiceAmountss', function (request, response) {
		jobBal.TotalInvoiceAmountss().then(function (result) {
			if (result) {
				response.set('Content-Type', 'application/json');
				response.status(200);
				response.json(result);
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for client vacancydetails for graph
	*/
	app.express.get('/api/job/client/recruitment', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getclientrecruitment().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method for Approval attendance
	*/
	app.express.post('/api/attendance/approve', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.approveattendance(request.body.attendance).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for Invoice group by jobcode
	*/
	app.express.get('/api/attendance/duties', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getduties(request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});
	/* 
	     Post Method for Invoice generation
	*/ 
	app.express.post('/api/attendance/invoice', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.generateinvoice(request.body.projectid, request.body.monthandyear, request.body.clientid,request.body.status,request.body.billstatus).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for Invoice generation
	*/ 
	app.express.get('/api/attendance/invoice', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getinvoice(request.query.invoiceno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for Invoice group by jobcode
	*/
	app.express.get('/api/attendance/invoice/report', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.invoicereport(request.query.cientid, request.query.projectid, request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update jobactivity
	*/
	app.express.post('/api/job/jobactivity/reject', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.rejectemployer(request.body.jobactivityid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobactivity
	*/ 
	app.express.get('/api/job/jobactivity/applied/report', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobappliedreport(request.query.closedate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobactivity
	*/
	app.express.get('/api/job/jobactivity/employees', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getemployeesinjob().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update jobactivity
	*/
	app.express.post('/api/job/jobactivity/employee/resign', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.employeeresign(request.body.jobactivityid, request.body.reason, request.body.resigndate, request.body.memberid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method postedlist Report
	*/
	app.express.get('/api/job/postedlist/report', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getpostedlistreport(request.query.closedate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update/edit jobposting
	*/
	app.express.post('/api/job/jobpostingdetail', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatejobpostingdetail(request.body.jobs, request.body.changedby, request.body.jobpostingid, request.body.startdate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update/edit jobposting
	*/
	app.express.post('/api/job/update/inplacecategory', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var confirmdate = new Date(request.body.confirmdate);
				jobBal.updateinplacecategory(request.body.jobactivityid, request.body.inplace, request.body.category, request.body.changedby, confirmdate, request.body.jobpostingdetailid, request.body.jobinplace, request.body.isrejected).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					} 
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to cancel jobactivity
	*/
	app.express.post('/api/job/jobactivity/cancel', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.canceljobactivity(request.body.jobactivityid, request.body.memberid, request.body.inplace, request.body.jobpostingdetailid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to cancel jobactivity
	*/
	app.express.post('/api/job/jobactivity/changeproject', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.jobchangeproject(request.body.jobactivityid, request.body.memberid, request.body.canceljobactivityid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to cancel jobactivity
	*/
	app.express.get('/api/job/jobactivity/carryforward', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.carryforward().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Post Method to update jobactivity
	*/
	app.express.post('/api/job/openingdate', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updateopeningdate(request.body.openingdate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method to opening date
	*/
	app.express.get('/api/job/openingdate', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getopeningdate().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method carryforward
	*/
	app.express.get('/api/job/carryforward/', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getcarryforward(0).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});   

	/*
	     Get Method carryforward export
	*/
	app.express.get('/api/job/carryforwardexport/', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getcarryforwardexport(0).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.post('/api/job/carryforward/add', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var jobposting = new jobModel.jobposting(request.body.clientid, request.body.projectid, request.body.jobs, request.body.changedby, request.body.startdate);
				jobBal.createcarryforward(jobposting).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/carryforward/delete', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.deletecarryforward(request.query.projectid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/* Delete All Jobs */
	app.express.get('/api/job/carryforward/deleteall', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.deletecarryforwardall(request.query.projectid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/carryforward/vacancy', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getforwardvacancy().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/carryforward/move', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.movevacancy().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.get('/api/job/salary/generation', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.salarygeneration(request.query.projectid, request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.post('/api/job/salary/generate', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.generatesalary(request.body.projectid, request.body.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/get/payslip', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getpayslip(request.query.projectid, request.query.monthandyear, request.query.payslipno).then(function (result) {
					if (result) { 
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for Invoice group by jobcode
	*/
	app.express.get('/api/attendance/duties/invoice', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getdutiesforinvoice(request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for Invoice group by jobcode
	*/
	app.express.get('/api/attendance/duties/authorize', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getdutiesforauthorize(request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.post('/api/job/invoice/authorize', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.authorizeinvoice(request.body.projectid, request.body.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/get/expayslip', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getexpayslip(request.query.salaryslipid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/get/payslip/preview', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getpayslippreview(request.query.projectid, request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/get/invoice/preview', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getinvoicepreview(request.query.projectid, request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/invoice/reject', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.rejectinvoice(request.body.projectid, request.body.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method for Invoice group by jobcode
	*/
	app.express.get('/api/attendance/duties/bankslip', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getdutiesforbankslip(request.query.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.post('/api/attendance/generate/bankslip', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.generatebankslip(request.body.projectid, request.body.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	/*
	     Get Method jobposting
	*/
	app.express.post('/api/finance/export/bankslip', function (request, response) {
		console.log(__dirname);
		
		// settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
		// 	if(results) {
		// jobBal.exportbankslip(request.body.monthandyear, request.body.projectid).then(function (result) {
		//     if (result) {
		//         var file = result.filepath;
		//         var filename = path.basename(file);
		//         var mimetype = mime.lookup(file);
		//         response.setHeader('Content-disposition', 'attachment; filename=' + filename);
		//         response.setHeader('Content-type', mimetype);
		//         var filestream = fs.createReadStream(file);
		//         filestream.pipe(response);
		//         // console.log(" result ", result.filename);
		//         // response.set('Content-Type', 'application/json')
		//         response.status(200);
		//         response.json(result.filename);
		//     }
		// }).catch(function (err) {
		//     response.set('Content-Type', 'application/json');
		//     response.status(400);
		//     response.json("Error   -- " + err);
		// });
		// 	} else {
		// 		response.set('Content-Type', 'application/json');
		// 		response.status(400);
		// 		response.json("Error   -- Wrong Token");
		// 	}
		// }).catch(function (err) {
		// 	response.set('Content-Type', 'application/json');
		// 	response.status(400);
		// 	response.json("Error   -- " + err);
		// });
	});

	app.express.post('/api/autherizedueamount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.autherizedueamount(request.body.projectid, request.body.monthandyear).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/getdueamount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getdueamount(request.body.clientid, request.body.projectid, request.body.monthandyear, request.body.invoiceno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/paydueamount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.paydueamount(request.body.clientid, request.body.projectid, request.body.monthandyear, request.body.invoiceno, request.body.amount, request.body.changedby).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/updatedueamount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.updatedueamount(request.body.dueid, request.body.changedby).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/exportxmltally', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.exportxmltally(request.body.clientid, request.body.projectid, request.body.monthandyear, request.body.invoiceno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/xmlreader', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				const reader = XmlReader.create();
				const xml = `<?xml version="1.0" encoding="UTF-8"?>
				<message>
					<to>Alice</to>
					<from>Bob</from>
					<heading color="blue">Hello</heading>
					<body color="red">This is a demo!</body>
				</message>`;

				reader.on('done', data => console.log(data));
				reader.parse(xml);
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/xmlwriter', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var texcokeyword = 'TALLY HEADING';
				jobBal.exportxmltally(request.body.invoiceid, request.body.invoiceno, request.body.monthandyear, request.body.clientid, request.body.projectid).then(function (result) {
					jobBal.getCompanyName(texcokeyword).then(function (companyname) {
						var strSVCURRENTCOMPANY = companyname[0].value;
						console.log('result',result[0]);
						if (result) {
							var servicetaxamount = result[0].servicetax / 2;
							if (servicetaxamount) {
								var obj = {
									"HEADER": {
										"TALLYREQUEST": "Import Data"
									},
									"BODY": {
										"IMPORTDATA": {
											"REQUESTDESC": {
												"REPORTNAME": "Vouchers",
												"STATICVARIABLES": {
													"SVCURRENTCOMPANY": strSVCURRENTCOMPANY
												} //STATICVARIABLES END
											}, //REQUESTDESC END
											"REQUESTDATA": {
												"TALLYMESSAGE": [{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"VOUCHER": {
															"@": {
																"REMOTEID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
																"VCHKEY": "fdbc4460-756c-11d6-bd67-008048b5445e-0000a930:00000078",
																"VCHTYPE": "INVOICE",
																"ACTION": "Create",
																"OBJVIEW": "Accounting Voucher View"
															},
															"OLDAUDITENTRYIDS.LIST": {
																"@": {
																	"TYPE": "Number"
																},
																"OLDAUDITENTRYIDS": "-1"
															},
															"DATE": result[0].createddate,
															"GUID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
															"NARRATION": "Security claim for the month " + result[0].monthandyear,
															"VOUCHERTYPENAME": "INVOICE",
															"VOUCHERNUMBER": result[0].invoiceno,
															"PARTYLEDGERNAME": result[0].projectno + "-" + result[0].name,
															"CSTFORMISSUETYPE": "",
															"CSTFORMRECVTYPE": "",
															"FBTPAYMENTTYPE": "Default",
															"PERSISTEDVIEW": "Accounting Voucher View",
															"VCHGSTCLASS": "",
															"ENTEREDBY": "duraian",
															"DIFFACTUALQTY": "No",
															"ISMSTFROMSYNC": "No",
															"ASORIGINAL": "No",
															"AUDITED": "No",
															"FORJOBCOSTING": "No",
															"ISOPTIONAL": "No",
															"EFFECTIVEDATE": result[0].createddate,
															"USEFOREXCISE": "No",
															"ISFORJOBWORKIN": "No",
															"ALLOWCONSUMPTION": "No",
															"USEFORINTEREST": "No",
															"USEFORGAINLOSS": "No",
															"USEFORGODOWNTRANSFER": "No",
															"USEFORCOMPOUND": "No",
															"USEFORSERVICETAX": "No",
															"ISEXCISEVOUCHER": "No",
															"EXCISETAXOVERRIDE": "No",
															"USEFORTAXUNITTRANSFER": "No",
															"EXCISEOPENING": "No",
															"USEFORFINALPRODUCTION": "No",
															"ISTDSOVERRIDDEN": "No",
															"ISTCSOVERRIDDEN": "No",
															"ISTDSTCSCASHVCH": "No",
															"INCLUDEADVPYMTVCH": "No",
															"ISSUBWORKSCONTRACT": "No",
															"ISVATOVERRIDDEN": "No",
															"IGNOREORIGVCHDATE": "No",
															"ISSERVICETAXOVERRIDDEN": "No",
															"ISISDVOUCHER": "No",
															"ISEXCISEOVERRIDDEN": "No",
															"ISEXCISESUPPLYVCH": "No",
															"ISGSTOVERRIDDEN": "No",
															"GSTNOTEXPORTED": "No",
															"ISVATPRINCIPALACCOUNT": "No",
															"ISSHIPPINGWITHINSTATE": "No",
															"ISCANCELLED": "No",
															"HASCASHFLOW": "No",
															"ISPOSTDATED": "No",
															"USETRACKINGNUMBER": "No",
															"ISINVOICE": "No",
															"MFGJOURNAL": "No",
															"HASDISCOUNTS": "No",
															"ASPAYSLIP": "No",
															"ISCOSTCENTRE": "No",
															"ISSTXNONREALIZEDVCH": "No",
															"ISEXCISEMANUFACTURERON": "No",
															"ISBLANKCHEQUE": "No",
															"ISVOID": "No",
															"ISONHOLD": "No",
															"ORDERLINESTATUS": "No",
															"VATISAGNSTCANCSALES": "No",
															"VATISPURCEXEMPTED": "No",
															"ISVATRESTAXINVOICE": "No",
															"VATISASSESABLECALCVCH": "No",
															"ISVATDUTYPAID": "Yes",
															"ISDELIVERYSAMEASCONSIGNEE": "No",
															"ISDISPATCHSAMEASCONSIGNOR": "No",
															"ISDELETED": "No",
															"CHANGEVCHMODE": "No",
															"ALTERID": " 1530883",
															"MASTERID": " 811405",
															"VOUCHERKEY": "186023623524472",
															"EXCLUDEDTAXATIONS.LIST": "      ",
															"OLDAUDITENTRIES.LIST": "      ",
															"ACCOUNTAUDITENTRIES.LIST": "      ",
															"AUDITENTRIES.LIST": "      ",
															"DUTYHEADDETAILS.LIST": "      ",
															"SUPPLEMENTARYDUTYHEADDETAILS.LIST": "      ",
															"INVOICEDELNOTES.LIST": "      ",
															"INVOICEORDERLIST.LIST": "      ",
															"INVOICEINDENTLIST.LIST": "      ",
															"ATTENDANCEENTRIES.LIST": "      ",
															"ORIGINVOICEDETAILS.LIST": "      ",
															"INVOICEEXPORTLIST.LIST": "      ",
															///// for loop
															"ALLLEDGERENTRIES.LIST": [{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": result[0].projectno + "-" + result[0].name,
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": "-" + result[0].totalamount,
																	"VATEXPAMOUNT": "-" + result[0].totalamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": {
																		"NAME": "Arr/258681s",
																		"BILLTYPE": "New Ref",
																		"TDSDEDUCTEEISSPECIALRATE": "No",
																		"AMOUNT": "-" + result[0].totalamount,
																		"INTERESTCOLLECTION.LIST": "      ",
																		"STBILLCATEGORIES.LIST": "      "
																	},
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Revenue From Security Projects", //2nd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": result[0].subtotal,
																	"VATEXPAMOUNT": result[0].subtotal,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "State GST Payable", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": servicetaxamount,
																	"VATEXPAMOUNT": servicetaxamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Central GST Payable", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": servicetaxamount,
																	"VATEXPAMOUNT": servicetaxamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Revenue From Service Charges", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": result[0].servicecharges,
																	"VATEXPAMOUNT": result[0].servicecharges,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																}
															],
															"PAYROLLMODEOFPAYMENT.LIST": "     ",
															"ATTDRECORDS.LIST": "      ",
															"TEMPGSTRATEDETAILS.LIST": "        "
														} //VOUCHER END
													}, //TALLYMESSAGE END 
													{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"COMPANY": {
															"REMOTECMPINFO.LIST": {
																"@": {
																	"MERGE": "Yes"
																},
																"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
																"REMOTECMPNAME": strSVCURRENTCOMPANY,
																"REMOTECMPSTATE": "TamilNadu"
															}
														}
													},
													{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"COMPANY": {
															"REMOTECMPINFO.LIST": {
																"@": {
																	"MERGE": "Yes"
																},
																"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
																"REMOTECMPNAME": strSVCURRENTCOMPANY,
																"REMOTECMPSTATE": "TamilNadu"
															}
														}
													}
												]
											} //REQUEST DATA END
										} //IMPORT DATA END
									} // BODY END                 
								};
							} else if (result[0].taxtype == 2) {
								var obj = {
									"HEADER": {
										"TALLYREQUEST": "Import Data"
									},
									"BODY": {
										"IMPORTDATA": {
											"REQUESTDESC": {
												"REPORTNAME": "Vouchers",
												"STATICVARIABLES": {
													"SVCURRENTCOMPANY": strSVCURRENTCOMPANY
												} //STATICVARIABLES END
											}, //REQUESTDESC END
											"REQUESTDATA": {
												"TALLYMESSAGE": [{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"VOUCHER": {
															"@": {
																"REMOTEID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
																"VCHKEY": "fdbc4460-756c-11d6-bd67-008048b5445e-0000a930:00000078",
																"VCHTYPE": "INVOICE",
																"ACTION": "Create",
																"OBJVIEW": "Accounting Voucher View"
															},
															"OLDAUDITENTRYIDS.LIST": {
																"@": {
																	"TYPE": "Number"
																},
																"OLDAUDITENTRYIDS": "-1"
															},
															"DATE": result[0].createddate,
															"GUID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
															"NARRATION": "Security claim for the month " + result[0].monthandyear,
															// "CLASSNAME" : "DefaultVoucherClass",
															"VOUCHERTYPENAME": "INVOICE",
															"VOUCHERNUMBER": result[0].invoiceno,
															"PARTYLEDGERNAME": result[0].projectno + "-" + result[0].name,
															"CSTFORMISSUETYPE": "",
															"CSTFORMRECVTYPE": "",
															"FBTPAYMENTTYPE": "Default",
															"PERSISTEDVIEW": "Accounting Voucher View",
															"VCHGSTCLASS": "",
															"ENTEREDBY": "duraian",
															"DIFFACTUALQTY": "No",
															"ISMSTFROMSYNC": "No",
															"ASORIGINAL": "No",
															"AUDITED": "No",
															"FORJOBCOSTING": "No",
															"ISOPTIONAL": "No",
															"EFFECTIVEDATE": result[0].createddate,
															"USEFOREXCISE": "No",
															"ISFORJOBWORKIN": "No",
															"ALLOWCONSUMPTION": "No",
															"USEFORINTEREST": "No",
															"USEFORGAINLOSS": "No",
															"USEFORGODOWNTRANSFER": "No",
															"USEFORCOMPOUND": "No",
															"USEFORSERVICETAX": "No",
															"ISEXCISEVOUCHER": "No",
															"EXCISETAXOVERRIDE": "No",
															"USEFORTAXUNITTRANSFER": "No",
															"EXCISEOPENING": "No",
															"USEFORFINALPRODUCTION": "No",
															"ISTDSOVERRIDDEN": "No",
															"ISTCSOVERRIDDEN": "No",
															"ISTDSTCSCASHVCH": "No",
															"INCLUDEADVPYMTVCH": "No",
															"ISSUBWORKSCONTRACT": "No",
															"ISVATOVERRIDDEN": "No",
															"IGNOREORIGVCHDATE": "No",
															"ISSERVICETAXOVERRIDDEN": "No",
															"ISISDVOUCHER": "No",
															"ISEXCISEOVERRIDDEN": "No",
															"ISEXCISESUPPLYVCH": "No",
															"ISGSTOVERRIDDEN": "No",
															"GSTNOTEXPORTED": "No",
															"ISVATPRINCIPALACCOUNT": "No",
															"ISSHIPPINGWITHINSTATE": "No",
															"ISCANCELLED": "No",
															"HASCASHFLOW": "No",
															"ISPOSTDATED": "No",
															"USETRACKINGNUMBER": "No",
															"ISINVOICE": "No",
															"MFGJOURNAL": "No",
															"HASDISCOUNTS": "No",
															"ASPAYSLIP": "No",
															"ISCOSTCENTRE": "No",
															"ISSTXNONREALIZEDVCH": "No",
															"ISEXCISEMANUFACTURERON": "No",
															"ISBLANKCHEQUE": "No",
															"ISVOID": "No",
															"ISONHOLD": "No",
															"ORDERLINESTATUS": "No",
															"VATISAGNSTCANCSALES": "No",
															"VATISPURCEXEMPTED": "No",
															"ISVATRESTAXINVOICE": "No",
															"VATISASSESABLECALCVCH": "No",
															"ISVATDUTYPAID": "Yes",
															"ISDELIVERYSAMEASCONSIGNEE": "No",
															"ISDISPATCHSAMEASCONSIGNOR": "No",
															"ISDELETED": "No",
															"CHANGEVCHMODE": "No",
															"ALTERID": " 1530883",
															"MASTERID": " 811405",
															"VOUCHERKEY": "186023623524472",
															"EXCLUDEDTAXATIONS.LIST": "      ",
															"OLDAUDITENTRIES.LIST": "      ",
															"ACCOUNTAUDITENTRIES.LIST": "      ",
															"AUDITENTRIES.LIST": "      ",
															"DUTYHEADDETAILS.LIST": "      ",
															"SUPPLEMENTARYDUTYHEADDETAILS.LIST": "      ",
															"INVOICEDELNOTES.LIST": "      ",
															"INVOICEORDERLIST.LIST": "      ",
															"INVOICEINDENTLIST.LIST": "      ",
															"ATTENDANCEENTRIES.LIST": "      ",
															"ORIGINVOICEDETAILS.LIST": "      ",
															"INVOICEEXPORTLIST.LIST": "      ",
															///// for loop
															"ALLLEDGERENTRIES.LIST": [{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": result[0].projectno + "-" + result[0].name,
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": "-" + result[0].totalamount,
																	"VATEXPAMOUNT": "-" + result[0].totalamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": {
																		"NAME": "Arr/258681s",
																		"BILLTYPE": "New Ref",
																		"TDSDEDUCTEEISSPECIALRATE": "No",
																		"AMOUNT": "-" + result[0].totalamount,
																		"INTERESTCOLLECTION.LIST": "      ",
																		"STBILLCATEGORIES.LIST": "      "
																	},
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "

																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Revenue From Security Projects", //2nd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": result[0].subtotal,
																	"VATEXPAMOUNT": result[0].subtotal,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},

																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "State GST Payable", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": servicetaxamount,
																	"VATEXPAMOUNT": servicetaxamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Central GST Payable", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": servicetaxamount,
																	"VATEXPAMOUNT": servicetaxamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Revenue From Service Charges", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": result[0].servicecharges,
																	"VATEXPAMOUNT": result[0].servicecharges,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																}



															],
															"PAYROLLMODEOFPAYMENT.LIST": "     ",
															"ATTDRECORDS.LIST": "      ",
															"TEMPGSTRATEDETAILS.LIST": "        "

														} //VOUCHER END

													}, //TALLYMESSAGE END 
													{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"COMPANY": {
															"REMOTECMPINFO.LIST": {
																"@": {
																	"MERGE": "Yes"
																},
																"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
																"REMOTECMPNAME": strSVCURRENTCOMPANY,
																"REMOTECMPSTATE": "TamilNadu"
															}

														}
													},
													{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"COMPANY": {
															"REMOTECMPINFO.LIST": {
																"@": {
																	"MERGE": "Yes"
																},
																"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
																"REMOTECMPNAME": strSVCURRENTCOMPANY,
																"REMOTECMPSTATE": "TamilNadu"
															}

														}
													}

												]

											} //REQUEST DATA END
										} //IMPORT DATA END
									} // BODY END                 
								};
							} else {
								var obj = {
									"HEADER": {
										"TALLYREQUEST": "Import Data"
									},
									"BODY": {
										"IMPORTDATA": {
											"REQUESTDESC": {
												"REPORTNAME": "Vouchers",
												"STATICVARIABLES": {
													"SVCURRENTCOMPANY": strSVCURRENTCOMPANY
												} //STATICVARIABLES END

											}, //REQUESTDESC END
											"REQUESTDATA": {
												"TALLYMESSAGE": [{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"VOUCHER": {
															"@": {
																"REMOTEID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
																"VCHKEY": "fdbc4460-756c-11d6-bd67-008048b5445e-0000a930:00000078",
																"VCHTYPE": "INVOICE",
																"ACTION": "Create",
																"OBJVIEW": "Accounting Voucher View"
															},
															"OLDAUDITENTRYIDS.LIST": {
																"@": {
																	"TYPE": "Number"
																},
																"OLDAUDITENTRYIDS": "-1"

															},
															"DATE": result[0].createddate,
															"GUID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
															"NARRATION": "Security claim for the month " + result[0].monthandyear,
															// "CLASSNAME" : "DefaultVoucherClass",
															"PARTYLEDGERNAME": result[0].projectno + "-" + result[0].name,
															"VOUCHERTYPENAME": "INVOICE",
															"VOUCHERNUMBER": result[0].invoiceno,
															"CSTFORMISSUETYPE": "",
															"CSTFORMRECVTYPE": "",
															"FBTPAYMENTTYPE": "Default",
															"PERSISTEDVIEW": "Accounting Voucher View",
															"VCHGSTCLASS": "",
															"ENTEREDBY": "CAO",
															"DIFFACTUALQTY": "No",
															"ISMSTFROMSYNC": "No",
															"ASORIGINAL": "No",
															"AUDITED": "No",
															"FORJOBCOSTING": "No",
															"ISOPTIONAL": "No",
															"EFFECTIVEDATE": result[0].createddate,
															"USEFOREXCISE": "No",
															"ISFORJOBWORKIN": "No",
															"ALLOWCONSUMPTION": "No",
															"USEFORINTEREST": "No",
															"USEFORGAINLOSS": "No",
															"USEFORGODOWNTRANSFER": "No",
															"USEFORCOMPOUND": "No",
															"USEFORSERVICETAX": "No",
															"ISONHOLD" : "No",
															"ISBOENOTAPPLICABLE" :"No",
															"ISEXCISEVOUCHER": "No",
															"EXCISETAXOVERRIDE": "No",
															"USEFORTAXUNITTRANSFER": "No","IGNOREPOSVALIDATION" : "No",
															"EXCISEOPENING": "No",
															"USEFORFINALPRODUCTION": "No",
															"ISTDSOVERRIDDEN": "No",
															"ISTCSOVERRIDDEN": "No",
															"ISTDSTCSCASHVCH": "No",  
															"INCLUDEADVPYMTVCH":"No",
															"ISVATPAIDATCUSTOMS":"No",
															"ISDECLAREDTOCUSTOMS":"No",
															"ISSUBWORKSCONTRACT": "No",
															"ISVATOVERRIDDEN": "No",
															"IGNOREORIGVCHDATE": "No",
															"ISSERVICETAXOVERRIDDEN": "No",
															"ISISDVOUCHER": "No",
															"ISEXCISEOVERRIDDEN": "No",
															"ISEXCISESUPPLYVCH": "No",
															"ISGSTOVERRIDDEN": "No",
															"GSTNOTEXPORTED": "No",
															"ISVATPRINCIPALACCOUNT": "No",
															"ISSHIPPINGWITHINSTATE": "No",
															"ISCANCELLED": "No",
															"HASCASHFLOW": "No",
															"ISPOSTDATED": "No",
															"USETRACKINGNUMBER": "No",
															"ISINVOICE": "No",
															"MFGJOURNAL": "No",
															"HASDISCOUNTS": "No",
															"ASPAYSLIP": "No",
															"ISCOSTCENTRE": "No",
															"ISSTXNONREALIZEDVCH": "No",
															"ISEXCISEMANUFACTURERON": "No",
															"ISBLANKCHEQUE": "No",
															"ISVOID": "No",
															"ISONHOLD": "No",
															"ORDERLINESTATUS": "No",
															"VATISAGNSTCANCSALES": "No",
															"VATISPURCEXEMPTED": "No",
															"ISVATRESTAXINVOICE": "No",
															"VATISASSESABLECALCVCH": "No",
															"ISVATDUTYPAID": "Yes",
															"ISDELIVERYSAMEASCONSIGNEE": "No",
															"ISDISPATCHSAMEASCONSIGNOR": "No",
															"ISDELETED": "No",
															"CHANGEVCHMODE": "No",
															"ALTERID": " 1530883",
															"MASTERID": " 811405",
															"VOUCHERKEY": "186023623524472",
															"EXCLUDEDTAXATIONS.LIST": "      ",
															"OLDAUDITENTRIES.LIST": "      ",
															"ACCOUNTAUDITENTRIES.LIST": "      ",
															"AUDITENTRIES.LIST": "      ",
															"DUTYHEADDETAILS.LIST": "      ",
															"SUPPLEMENTARYDUTYHEADDETAILS.LIST": "      ",
															"INVOICEDELNOTES.LIST": "      ",
															"INVOICEORDERLIST.LIST": "      ",
															"INVOICEINDENTLIST.LIST": "      ",
															"ATTENDANCEENTRIES.LIST": "      ",
															"ORIGINVOICEDETAILS.LIST": "      ",
															"INVOICEEXPORTLIST.LIST": "      ",
															///// for loop
															"ALLLEDGERENTRIES.LIST": [{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": result[0].projectno + "-" + result[0].name,
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "Yes",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "Yes",
																	"ISLASTDEEMEDPOSITIVE": "Yes",
																	"AMOUNT": "-" + result[0].totalamount,
																	"VATEXPAMOUNT": "-" + result[0].totalamount,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": {
																		"NAME": "Arr/258681s",
																		"BILLTYPE": "New Ref",
																		"TDSDEDUCTEEISSPECIALRATE": "No",
																		"AMOUNT": "-" + result[0].totalamount,
																		"INTERESTCOLLECTION.LIST": "      ",
																		"STBILLCATEGORIES.LIST": "      "
																	},
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Revenue From Security Projects", //2nd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "No",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "No",
																	"ISLASTDEEMEDPOSITIVE": "No",
																	"ISCAPVATTAXALTERED": "No",
																	"ISCAPVATNOTCLAIMED": "No",
																	"AMOUNT": result[0].subtotal,
																	"VATEXPAMOUNT": result[0].subtotal,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																},
																{
																	"OLDAUDITENTRYIDS.LIST": {
																		"@": {
																			"TYPE": "Number"
																		},
																		"OLDAUDITENTRYIDS": "-1"
																	},
																	"LEDGERNAME": "Revenue From Service Charges", //3rd
																	"GSTCLASS": "",
																	"ISDEEMEDPOSITIVE": "No",
																	"LEDGERFROMITEM": "No",
																	"REMOVEZEROENTRIES": "No",
																	"ISPARTYLEDGER": "No",
																	"ISLASTDEEMEDPOSITIVE": "No", "ISCAPVATTAXALTERED": "No",
																	"ISCAPVATNOTCLAIMED": "No",
																	"AMOUNT": result[0].servicecharges,
																	"VATEXPAMOUNT": result[0].servicecharges,
																	"SERVICETAXDETAILS.LIST": "       ",
																	"BANKALLOCATIONS.LIST": "       ",
																	"BILLALLOCATIONS.LIST": "       ",
																	"INTERESTCOLLECTION.LIST": "       ",
																	"OLDAUDITENTRIES.LIST": "       ",
																	"ACCOUNTAUDITENTRIES.LIST": "       ",
																	"AUDITENTRIES.LIST": "       ",
																	"INPUTCRALLOCS.LIST": "       ",
																	"DUTYHEADDETAILS.LIST": "       ",
																	"EXCISEDUTYHEADDETAILS.LIST": "       ",
																	"RATEDETAILS.LIST": "       ",
																	"SUMMARYALLOCS.LIST": "       ",
																	"STPYMTDETAILS.LIST": "       ",
																	"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
																	"TAXBILLALLOCATIONS.LIST": "       ",
																	"TAXOBJECTALLOCATIONS.LIST": "       ",
																	"TDSEXPENSEALLOCATIONS.LIST": "       ",
																	"VATSTATUTORYDETAILS.LIST": "       ",
																	"OSTTRACKALLOCATIONS.LIST": "       ",
																	"REFVOUCHERDETAILS.LIST": "       ",
																	"INVOICEWISEDETAILS.LIST": "       ",
																	"VATITCDETAILS.LIST": "       ",
																	"ADVANCETAXDETAILS.LIST": "       "
																}
															],
															"PAYROLLMODEOFPAYMENT.LIST": "     ",
															"ATTDRECORDS.LIST": "      ",
															"TEMPGSTRATEDETAILS.LIST": "        "

														} //VOUCHER END

													}, //TALLYMESSAGE END 
													{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"COMPANY": {
															"REMOTECMPINFO.LIST": {
																"@": {
																	"MERGE": "Yes"
																},
																"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
																"REMOTECMPNAME": strSVCURRENTCOMPANY,
																"REMOTECMPSTATE": "TamilNadu"
															}

														}
													},
													{
														"@": {
															"xmlns:UDF": "TallyUDF"
														},
														"COMPANY": {
															"REMOTECMPINFO.LIST": {
																"@": {
																	"MERGE": "Yes"
																},
																"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
																"REMOTECMPNAME": strSVCURRENTCOMPANY,
																"REMOTECMPSTATE": "TamilNadu"
															}
														}
													}
												]
											} //REQUEST DATA END
										} //IMPORT DATA END
									} // BODY END                 
								};
							}  
							//console.log('obj', obj);
							var filepath = nconf.get('DOWNLOADURLXML');
							fs.writeFile(filepath + "AccountingVoucher-" + request.body.invoiceno + ".xml", js2xmlparser.parse("ENVELOPE", obj), function (err, data) {
								if (err) console.log('errr', err);
								response.set('Content-Type', 'application/json');
								response.status(200);
								response.json(obj);
								response.status(200);
							});
						}
					}).catch(function (err) {
						response.set('Content-Type', 'application/json');
						response.status(400);
						response.json("Error   -- " + err);
					});
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});
	
	app.express.post('/api/finance/xmlwriterAll', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var strSVCURRENTCOMPANY = "TEXCO 2017-2018";
				var texcokeyword = 'TALLY HEADING';
				var invoicedatas = [];
				jobBal.exportxmltallyAll(request.body.monthandyear).then(function (result) {
					jobBal.getCompanyName(texcokeyword).then(function (result) {
						console.log('result', result);
						if (result) {
							var servicetaxamount = result[0].servicetax / 2;
							for (var j = 0; j < result.length; j++) {
								var invoicedatass = {
									"TALLYMESSAGE": [{
											"@": {
												"xmlns:UDF": "TallyUDF"
											},
											"VOUCHER": {
												"@": {
													"REMOTEID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
													"VCHKEY": "fdbc4460-756c-11d6-bd67-008048b5445e-0000a930:00000078",
													"VCHTYPE": "INVOICE",
													"ACTION": "Create",
													"OBJVIEW": "Accounting Voucher View"
												},
												"OLDAUDITENTRYIDS.LIST": {
													"@": {
														"TYPE": "Number"
													},
													"OLDAUDITENTRYIDS": "-1"

												},
												"DATE": result[0].createddate,
												"GUID": "fdbc4460-756c-11d6-bd67-008048b5445e-000c618d",
												"NARRATION": "Security claim for the month " + result[0].monthandyear,
												// "CLASSNAME" : "DefaultVoucherClass",
												"VOUCHERTYPENAME": "INVOICE",
												"VOUCHERNUMBER": result[0].invoiceno,
												"PARTYLEDGERNAME": result[0].projectno + "-" + result[0].name,
												"CSTFORMISSUETYPE": "",
												"CSTFORMRECVTYPE": "",
												"FBTPAYMENTTYPE": "Default",
												"PERSISTEDVIEW": "Accounting Voucher View",
												"VCHGSTCLASS": "",
												"ENTEREDBY": "duraian",
												"DIFFACTUALQTY": "No",
												"ISMSTFROMSYNC": "No",
												"ASORIGINAL": "No",
												"AUDITED": "No",
												"FORJOBCOSTING": "No",
												"ISOPTIONAL": "No",
												"EFFECTIVEDATE": result[0].createddate,
												"USEFOREXCISE": "No",
												"ISFORJOBWORKIN": "No",
												"ALLOWCONSUMPTION": "No",
												"USEFORINTEREST": "No",
												"USEFORGAINLOSS": "No",
												"USEFORGODOWNTRANSFER": "No",
												"USEFORCOMPOUND": "No",
												"USEFORSERVICETAX": "No",
												"ISEXCISEVOUCHER": "No",
												"EXCISETAXOVERRIDE": "No",
												"USEFORTAXUNITTRANSFER": "No",
												"EXCISEOPENING": "No",
												"USEFORFINALPRODUCTION": "No",
												"ISTDSOVERRIDDEN": "No",
												"ISTCSOVERRIDDEN": "No",
												"ISTDSTCSCASHVCH": "No",
												"INCLUDEADVPYMTVCH": "No",
												"ISSUBWORKSCONTRACT": "No",
												"ISVATOVERRIDDEN": "No",
												"IGNOREORIGVCHDATE": "No",
												"ISSERVICETAXOVERRIDDEN": "No",
												"ISISDVOUCHER": "No",
												"ISEXCISEOVERRIDDEN": "No",
												"ISEXCISESUPPLYVCH": "No",
												"ISGSTOVERRIDDEN": "No",
												"GSTNOTEXPORTED": "No",
												"ISVATPRINCIPALACCOUNT": "No",
												"ISSHIPPINGWITHINSTATE": "No",
												"ISCANCELLED": "No",
												"HASCASHFLOW": "No",
												"ISPOSTDATED": "No",
												"USETRACKINGNUMBER": "No",
												"ISINVOICE": "No",
												"MFGJOURNAL": "No",
												"HASDISCOUNTS": "No",
												"ASPAYSLIP": "No",
												"ISCOSTCENTRE": "No",
												"ISSTXNONREALIZEDVCH": "No",
												"ISEXCISEMANUFACTURERON": "No",
												"ISBLANKCHEQUE": "No",
												"ISVOID": "No",
												"ISONHOLD": "No",
												"ORDERLINESTATUS": "No",
												"VATISAGNSTCANCSALES": "No",
												"VATISPURCEXEMPTED": "No",
												"ISVATRESTAXINVOICE": "No",
												"VATISASSESABLECALCVCH": "No",
												"ISVATDUTYPAID": "Yes",
												"ISDELIVERYSAMEASCONSIGNEE": "No",
												"ISDISPATCHSAMEASCONSIGNOR": "No",
												"ISDELETED": "No",
												"CHANGEVCHMODE": "No",
												"ALTERID": " 1530883",
												"MASTERID": " 811405",
												"VOUCHERKEY": "186023623524472",
												"EXCLUDEDTAXATIONS.LIST": "      ",
												"OLDAUDITENTRIES.LIST": "      ",
												"ACCOUNTAUDITENTRIES.LIST": "      ",
												"AUDITENTRIES.LIST": "      ",
												"DUTYHEADDETAILS.LIST": "      ",
												"SUPPLEMENTARYDUTYHEADDETAILS.LIST": "      ",
												"INVOICEDELNOTES.LIST": "      ",
												"INVOICEORDERLIST.LIST": "      ",
												"INVOICEINDENTLIST.LIST": "      ",
												"ATTENDANCEENTRIES.LIST": "      ",
												"ORIGINVOICEDETAILS.LIST": "      ",
												"INVOICEEXPORTLIST.LIST": "      ",
												///// for loop
												"ALLLEDGERENTRIES.LIST": [{
														"OLDAUDITENTRYIDS.LIST": {
															"@": {
																"TYPE": "Number"
															},
															"OLDAUDITENTRYIDS": "-1"
														},
														"LEDGERNAME": result[0].projectno + "-" + result[0].name,
														"GSTCLASS": "",
														"ISDEEMEDPOSITIVE": "Yes",
														"LEDGERFROMITEM": "No",
														"REMOVEZEROENTRIES": "No",
														"ISPARTYLEDGER": "Yes",
														"ISLASTDEEMEDPOSITIVE": "Yes",
														"AMOUNT": result[0].totalamount,
														"VATEXPAMOUNT": result[0].totalamount,
														"SERVICETAXDETAILS.LIST": "       ",
														"BANKALLOCATIONS.LIST": "       ",
														"BILLALLOCATIONS.LIST": {
															"NAME": "Arr/258681s",
															"BILLTYPE": "New Ref",
															"TDSDEDUCTEEISSPECIALRATE": "No",
															"AMOUNT": result[0].totalamount,
															"INTERESTCOLLECTION.LIST": "      ",
															"STBILLCATEGORIES.LIST": "      "
														},
														"INTERESTCOLLECTION.LIST": "       ",
														"OLDAUDITENTRIES.LIST": "       ",
														"ACCOUNTAUDITENTRIES.LIST": "       ",
														"AUDITENTRIES.LIST": "       ",
														"INPUTCRALLOCS.LIST": "       ",
														"DUTYHEADDETAILS.LIST": "       ",
														"EXCISEDUTYHEADDETAILS.LIST": "       ",
														"RATEDETAILS.LIST": "       ",
														"SUMMARYALLOCS.LIST": "       ",
														"STPYMTDETAILS.LIST": "       ",
														"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
														"TAXBILLALLOCATIONS.LIST": "       ",
														"TAXOBJECTALLOCATIONS.LIST": "       ",
														"TDSEXPENSEALLOCATIONS.LIST": "       ",
														"VATSTATUTORYDETAILS.LIST": "       ",
														"OSTTRACKALLOCATIONS.LIST": "       ",
														"REFVOUCHERDETAILS.LIST": "       ",
														"INVOICEWISEDETAILS.LIST": "       ",
														"VATITCDETAILS.LIST": "       ",
														"ADVANCETAXDETAILS.LIST": "       "

													},
													{
														"OLDAUDITENTRYIDS.LIST": {
															"@": {
																"TYPE": "Number"
															},
															"OLDAUDITENTRYIDS": "-1"
														},
														"LEDGERNAME": "Revenue From Security Projects", //2nd
														"GSTCLASS": "",
														"ISDEEMEDPOSITIVE": "Yes",
														"LEDGERFROMITEM": "No",
														"REMOVEZEROENTRIES": "No",
														"ISPARTYLEDGER": "Yes",
														"ISLASTDEEMEDPOSITIVE": "Yes",
														"AMOUNT": result[0].subtotal,
														"VATEXPAMOUNT": result[0].subtotal,
														"SERVICETAXDETAILS.LIST": "       ",
														"BANKALLOCATIONS.LIST": "       ",
														"BILLALLOCATIONS.LIST": "       ",
														"INTERESTCOLLECTION.LIST": "       ",
														"OLDAUDITENTRIES.LIST": "       ",
														"ACCOUNTAUDITENTRIES.LIST": "       ",
														"AUDITENTRIES.LIST": "       ",
														"INPUTCRALLOCS.LIST": "       ",
														"DUTYHEADDETAILS.LIST": "       ",
														"EXCISEDUTYHEADDETAILS.LIST": "       ",
														"RATEDETAILS.LIST": "       ",
														"SUMMARYALLOCS.LIST": "       ",
														"STPYMTDETAILS.LIST": "       ",
														"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
														"TAXBILLALLOCATIONS.LIST": "       ",
														"TAXOBJECTALLOCATIONS.LIST": "       ",
														"TDSEXPENSEALLOCATIONS.LIST": "       ",
														"VATSTATUTORYDETAILS.LIST": "       ",
														"OSTTRACKALLOCATIONS.LIST": "       ",
														"REFVOUCHERDETAILS.LIST": "       ",
														"INVOICEWISEDETAILS.LIST": "       ",
														"VATITCDETAILS.LIST": "       ",
														"ADVANCETAXDETAILS.LIST": "       "
													},

													{
														"OLDAUDITENTRYIDS.LIST": {
															"@": {
																"TYPE": "Number"
															},
															"OLDAUDITENTRYIDS": "-1"
														},
														"LEDGERNAME": "State GST Payable", //3rd
														"GSTCLASS": "",
														"ISDEEMEDPOSITIVE": "Yes",
														"LEDGERFROMITEM": "No",
														"REMOVEZEROENTRIES": "No",
														"ISPARTYLEDGER": "Yes",
														"ISLASTDEEMEDPOSITIVE": "Yes",
														"AMOUNT": servicetaxamount,
														"VATEXPAMOUNT": servicetaxamount,
														"SERVICETAXDETAILS.LIST": "       ",
														"BANKALLOCATIONS.LIST": "       ",
														"BILLALLOCATIONS.LIST": "       ",
														"INTERESTCOLLECTION.LIST": "       ",
														"OLDAUDITENTRIES.LIST": "       ",
														"ACCOUNTAUDITENTRIES.LIST": "       ",
														"AUDITENTRIES.LIST": "       ",
														"INPUTCRALLOCS.LIST": "       ",
														"DUTYHEADDETAILS.LIST": "       ",
														"EXCISEDUTYHEADDETAILS.LIST": "       ",
														"RATEDETAILS.LIST": "       ",
														"SUMMARYALLOCS.LIST": "       ",
														"STPYMTDETAILS.LIST": "       ",
														"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
														"TAXBILLALLOCATIONS.LIST": "       ",
														"TAXOBJECTALLOCATIONS.LIST": "       ",
														"TDSEXPENSEALLOCATIONS.LIST": "       ",
														"VATSTATUTORYDETAILS.LIST": "       ",
														"OSTTRACKALLOCATIONS.LIST": "       ",
														"REFVOUCHERDETAILS.LIST": "       ",
														"INVOICEWISEDETAILS.LIST": "       ",
														"VATITCDETAILS.LIST": "       ",
														"ADVANCETAXDETAILS.LIST": "       "
													},
													{
														"OLDAUDITENTRYIDS.LIST": {
															"@": {
																"TYPE": "Number"
															},
															"OLDAUDITENTRYIDS": "-1"
														},
														"LEDGERNAME": "Central GST Payable", //3rd
														"GSTCLASS": "",
														"ISDEEMEDPOSITIVE": "Yes",
														"LEDGERFROMITEM": "No",
														"REMOVEZEROENTRIES": "No",
														"ISPARTYLEDGER": "Yes",
														"ISLASTDEEMEDPOSITIVE": "Yes",
														"AMOUNT": servicetaxamount,
														"VATEXPAMOUNT": servicetaxamount,
														"SERVICETAXDETAILS.LIST": "       ",
														"BANKALLOCATIONS.LIST": "       ",
														"BILLALLOCATIONS.LIST": "       ",
														"INTERESTCOLLECTION.LIST": "       ",
														"OLDAUDITENTRIES.LIST": "       ",
														"ACCOUNTAUDITENTRIES.LIST": "       ",
														"AUDITENTRIES.LIST": "       ",
														"INPUTCRALLOCS.LIST": "       ",
														"DUTYHEADDETAILS.LIST": "       ",
														"EXCISEDUTYHEADDETAILS.LIST": "       ",
														"RATEDETAILS.LIST": "       ",
														"SUMMARYALLOCS.LIST": "       ",
														"STPYMTDETAILS.LIST": "       ",
														"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
														"TAXBILLALLOCATIONS.LIST": "       ",
														"TAXOBJECTALLOCATIONS.LIST": "       ",
														"TDSEXPENSEALLOCATIONS.LIST": "       ",
														"VATSTATUTORYDETAILS.LIST": "       ",
														"OSTTRACKALLOCATIONS.LIST": "       ",
														"REFVOUCHERDETAILS.LIST": "       ",
														"INVOICEWISEDETAILS.LIST": "       ",
														"VATITCDETAILS.LIST": "       ",
														"ADVANCETAXDETAILS.LIST": "       "
													},
													{
														"OLDAUDITENTRYIDS.LIST": {
															"@": {
																"TYPE": "Number"
															},
															"OLDAUDITENTRYIDS": "-1"
														},
														"LEDGERNAME": "Revenue From Service Charges", //3rd
														"GSTCLASS": "",
														"ISDEEMEDPOSITIVE": "Yes",
														"LEDGERFROMITEM": "No",
														"REMOVEZEROENTRIES": "No",
														"ISPARTYLEDGER": "Yes",
														"ISLASTDEEMEDPOSITIVE": "Yes",
														"AMOUNT": result[0].servicecharges,
														"VATEXPAMOUNT": result[0].servicecharges,
														"SERVICETAXDETAILS.LIST": "       ",
														"BANKALLOCATIONS.LIST": "       ",
														"BILLALLOCATIONS.LIST": "       ",
														"INTERESTCOLLECTION.LIST": "       ",
														"OLDAUDITENTRIES.LIST": "       ",
														"ACCOUNTAUDITENTRIES.LIST": "       ",
														"AUDITENTRIES.LIST": "       ",
														"INPUTCRALLOCS.LIST": "       ",
														"DUTYHEADDETAILS.LIST": "       ",
														"EXCISEDUTYHEADDETAILS.LIST": "       ",
														"RATEDETAILS.LIST": "       ",
														"SUMMARYALLOCS.LIST": "       ",
														"STPYMTDETAILS.LIST": "       ",
														"EXCISEPAYMENTALLOCATIONS.LIST": "       ",
														"TAXBILLALLOCATIONS.LIST": "       ",
														"TAXOBJECTALLOCATIONS.LIST": "       ",
														"TDSEXPENSEALLOCATIONS.LIST": "       ",
														"VATSTATUTORYDETAILS.LIST": "       ",
														"OSTTRACKALLOCATIONS.LIST": "       ",
														"REFVOUCHERDETAILS.LIST": "       ",
														"INVOICEWISEDETAILS.LIST": "       ",
														"VATITCDETAILS.LIST": "       ",
														"ADVANCETAXDETAILS.LIST": "       "
													}



												],
												"PAYROLLMODEOFPAYMENT.LIST": "     ",
												"ATTDRECORDS.LIST": "      ",
												"TEMPGSTRATEDETAILS.LIST": "        "

											} //VOUCHER END

										}, //TALLYMESSAGE END 
										{
											"@": {
												"xmlns:UDF": "TallyUDF"
											},
											"COMPANY": {
												"REMOTECMPINFO.LIST": {
													"@": {
														"MERGE": "Yes"
													},
													"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
													"REMOTECMPNAME": strSVCURRENTCOMPANY,
													"REMOTECMPSTATE": "TamilNadu"
												}

											}
										},
										{
											"@": {
												"xmlns:UDF": "TallyUDF"
											},
											"COMPANY": {
												"REMOTECMPINFO.LIST": {
													"@": {
														"MERGE": "Yes"
													},
													"NAME": "fdbc4460-756c-11d6-bd67-008048b5445e",
													"REMOTECMPNAME": strSVCURRENTCOMPANY,
													"REMOTECMPSTATE": "TamilNadu"
												}

											}
										}

									]

								}
								invoicedatas.push(invoicedatass);
							}
							var obj = {
								"HEADER": {
									"TALLYREQUEST": "Import Data"
								},
								"BODY": {
									"IMPORTDATA": {
										"REQUESTDESC": {
											"REPORTNAME": "Vouchers",
											"STATICVARIABLES": {
												"SVCURRENTCOMPANY": strSVCURRENTCOMPANY
											}
										},
										"REQUESTDATA": {
											invoicedatas
										}
									}
								}
							}
							fs.writeFile(xmlfolder + "xmldownload/AccountingVoucher-all.xml", js2xmlparser.parse("ENVELOPE", obj), function (err, data) {
								if (err) console.log(err);
								response.set('Content-Type', 'application/json');
								response.status(200);
								response.json(obj);
								response.status(200);
							}); 
						}
					}).catch(function (err) {
						response.set('Content-Type', 'application/json');
						response.status(400);
						response.json("Error   -- " + err);
					});
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});
	app.express.post('/api/job/dues/importdues/', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.importdueamount(request.body.dues).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.post('/api/job/AttendanceAuthorize', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.AttendanceAuthorizeSupervisor(request.body.projectid, request.body.clientid, request.body.monthandyear, request.body.status, request.body.billstatus, request.body.approvaltype,request.body.payslipno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				}); 
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});   

	app.express.post('/api/job/AttendanceReviewList', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getAuthorizeList(request.body.data.fromdate, request.body.data.todate, request.body.data.regionid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					} 
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});    

	app.express.post('/api/job/AuthorizePayslipList', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getAuthorizedPaySlipList(request.body.data.fromdate, request.body.data.todate, request.body.data.regionid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					} 
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.post('/api/job/CashierAuthorize', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.CashierAuthorizeApproval(request.body.data[0].projects, request.body.data[0].passordernumber, request.body.data[0].status, request.body.data[0].billstatus).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/PassOrderNumber', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getPassOrderNumber().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/PassOrderNumberList', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getPassOrderNumberForList().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.get('/api/job/PassOrderNumberListByDate', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getPassOrderNumberForListByDate(request.query.fromdate,request.query.todate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.get('/api/job/PassOrderNoProjectDetailsByID', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getPassOrderNoProjectDetailsByID(request.query.passordernumber).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});
	
	app.express.get('/api/job/DownloadPayslipByID', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getDownloadPayslipByID(request.query.passordernumber).then(function (result) {
					console.log('result', result);
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/CAOApproval', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.CAOApproval(request.body.data[0].projects, request.body.data[0].status).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/getSalarySlipNo', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getSalarySlipNoDetails(request.body.data.fromdate, request.body.data.todate, request.body.data.regionid,request.body.data.projectid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/getInvoiceNo', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getInvoiceNoDetails(request.body.data.fromdate, request.body.data.todate, request.body.data.regionid,request.body.data.projectid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/getWageCategory', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getWageCategoryDetails().then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/getWageCategoryByProject', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getWageCategoryDetailsByID(request.query.projectid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/createArrearSalarySlip', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.createArrearSalarySlip(request.body.data.payslipnos, request.body.data.category, request.body.data.wageyearid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/createArrearClaimBill', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.createArrearClaimBill(request.body.data.payslipnos, request.body.data.category, request.body.data.wageyearid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.get('/api/job/Payslipprint', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getpayslipprint(request.query.monthandyear,request.query.regionid,request.query.type).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.post('/api/job/UpdatePrintCount', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				if (request.body.type == 3) {
					// console.log('request.body.type', request.body.type);
					jobBal.UpdatePrintCounts(request.body.invoiceid, request.body.type).then(function (result) {
						if (result) {
							response.set('Content-Type', 'application/json');
							response.status(200);
							response.json(result);
						}
					}).catch(function (err) {
						response.set('Content-Type', 'application/json');
						response.status(400);
						response.json("Error   -- " + err);
					});
				} else {
					jobBal.UpdatePrintCount(request.body.payslipno, request.body.type).then(function (result) {
						if (result) {
							response.set('Content-Type', 'application/json');
							response.status(200);
							response.json(result);
						}
					}).catch(function (err) {
						response.set('Content-Type', 'application/json');
						response.status(400);
						response.json("Error   -- " + err);
					});
				}
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/Invoiceprint', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getinvoiceprint(request.query.monthandyear,request.query.regionid,request.query.type).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.post('/api/job/getCombinedClaimsProjects', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getCombinedClaimsProjects(request.body.data.fromdate, request.body.data.todate, request.body.data.regionid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/createCombinedClaims', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				//console.log('request.body.data[0].projects',request.body.data[0].projects);
				jobBal.createCombinedClaims(request.body.data[0].projects).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/ArrearPayslipprint', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getArrearPayslipprintAdmin(request.query.projectid, request.query.monthandyear,request.query.payslipno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/ArrearClaimprint', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getArrearClaimprint(request.query.invoiceno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.get('/api/job/BulkPrint', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.Bulkprint(request.query.fromdate, request.query.todate, request.query.regionid, request.query.type).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/ECRPrint', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.DownloadECRPrint(request.body.fromdate, request.body.todate, request.body.regionid, request.body.type).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.get('/api/job/getProjectDetails', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getProjectDetails(request.query.invoiceno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.get('/api/job/getjobpostingdetails', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getjobpostingdetails(request.query.fromdate,request.query.todate).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});

	app.express.post('/api/job/updatejob', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var podata = request.body.data;
				// console.log('podata',podata);
				jobBal.updatePostingOrder(podata.projectid, podata.projectno, podata.texcono, podata.category, podata.memberhistoryid, podata.startdate, podata.memberid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.post('/api/job/transferposting', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var query1 = {
					memberid: request.body.data.memberid,
					projectid: request.body.data.projectid,
					projectno: request.body.data.projectno,
					texcono: request.body.data.texcono,
					category: request.body.data.category,
					startdate: request.body.data.startdate,
					approvalstatus : 1,
					working_status  : 1,
					active: 1
				}; 
				jobBal.transferPostingOrder(query1,request.body.data.endate,request.body.data.memberhistoryid).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 

	app.express.get('/api/job/authorizeattendance', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				jobBal.getattendancereview(request.query.clientid, request.query.projectid, request.query.monthandyear, request.query.types).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	}); 
	
	app.express.post('/api/job/addHistory', function (request, response) {
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var podata = request.body.data;
				jobBal.addPostingOrder(podata.projectid, podata.projectno, podata.texcono, podata.category, podata.startdate, podata.enddate, podata.serviceno).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) {
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				});
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});  

	app.express.post('/api/job/import/memberhistory/', function (request, response) {  
		request.setTimeout(0);
		console.log('request.heade',request.headers['authorization']);
		settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
			if(results) {
				var podata = request.body.memberdata;
				jobBal.addbulkPostingOrder(podata[0]).then(function (result) {
					if (result) {
						response.set('Content-Type', 'application/json');
						response.status(200);
						response.json(result);
					}
				}).catch(function (err) { 
					console.log('err',err);
					response.set('Content-Type', 'application/json');
					response.status(400);
					response.json("Error   -- " + err);
				}); 
			} else {
				response.set('Content-Type', 'application/json');
				response.status(400);
				response.json("Error   -- Wrong Token");
			}
		}).catch(function (err) {
			response.set('Content-Type', 'application/json');
			response.status(400);
			response.json("Error   -- " + err);
		});
	});  

	// http://sms.afraconnect.com/api/mt/SendSMS?user=texco&password=Adm1nX321&senderid=TEXCOL&channel=Trans&DCS=0&flashsms=0&number=917402220724&text=test message www.AfraConnect.com 9371937116&route=20


	// http://message.raagaatechnologies.com/api/mt/SendSMS?user=texco&password=Adm1nX321&senderid=TEXCOL&channel=Promo&DCS=0&flashsms=0&number=917402220724&text=test&route=20


	// http://message.raagaatechnologies.com/api/mt/SendSMS?user=texco&password=Adm1nX321&senderid=TEXCOL&channel=Trans&DCS=0&flashsms=0&number=917402220724&text=test&route=20

	// app.express.get('http://message.raagaatechnologies.com/api/mt/SendSMS?user=texco&password=Adm1nX321&senderid=TEXCOL&channel=Promo&DCS=0&flashsms=0&number=917402220724&text=test message&route=20', function (request, response) {
	// 	response.set('Content-Type', 'application/json');
	// 	response.status(200);
	// 	response.json('result');
	// });


    app.express.get('/api/ProjectArrearDetailsByID', function (request, response) {
        settingBal.getAuthorizationToken(request.headers['authorization']).then(function (results) {
            if(results) {
                jobBal.getProjectArrearDetailsByID(request.query.projectid,request.query.clientid).then(function (result) {
                    if (result) {
                        response.set('Content-Type', 'application/json');
                        response.status(200);
                        response.json(result);
                    }
                }).catch(function (err) {
                    response.set('Content-Type', 'application/json');
                    response.status(400);
                    response.json("Error   -- " + err);
                });
            } else {
                response.set('Content-Type', 'application/json');
                response.status(400);
                response.json("Error   -- Wrong Token");
            }
        }).catch(function (err) {
            response.set('Content-Type', 'application/json');
            response.status(400);
            response.json("Error   -- " + err);
        });
    });

}


