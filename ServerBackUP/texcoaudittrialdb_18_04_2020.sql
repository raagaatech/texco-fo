-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for texco_audit_trail
CREATE DATABASE IF NOT EXISTS `texco_audit_trail` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `texco_audit_trail`;

-- Dumping structure for table texco_audit_trail.agreement
CREATE TABLE IF NOT EXISTS `agreement` (
  `agreementid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `fromdate` datetime DEFAULT NULL,
  `todate` datetime DEFAULT NULL,
  `servicecharge` varchar(250) DEFAULT NULL,
  `wagetypeid` int(11) DEFAULT NULL,
  `wageyearid` int(11) DEFAULT NULL,
  `wageareaid` int(11) DEFAULT NULL,
  `wagecategoryid` int(11) DEFAULT NULL,
  `agreementstatusid` int(11) DEFAULT NULL,
  `agreementtypeid` int(11) DEFAULT NULL,
  `agtypeid` int(11) DEFAULT NULL,
  `tax` float(8,4) DEFAULT NULL,
  `taxtype` float(8,4) DEFAULT NULL,
  `particularid` float(8,4) DEFAULT NULL,
  `optionaltype` varchar(250) DEFAULT NULL,
  `addendum` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `amstatus` tinyint(4) DEFAULT '0',
  `addendumstatus` tinyint(4) DEFAULT '0',
  `changedby` varchar(50) DEFAULT NULL,
  `modifdttm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.agreement: ~0 rows (approximately)
/*!40000 ALTER TABLE `agreement` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreement` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.agreementdetail
CREATE TABLE IF NOT EXISTS `agreementdetail` (
  `agreementdetailid` int(11) NOT NULL,
  `agreementinfoid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `numberofvacancies` int(5) DEFAULT NULL,
  `salary` int(10) DEFAULT NULL,
  `addendum` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `amstatus` int(11) DEFAULT '0',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.agreementdetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `agreementdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreementdetail` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.agreementdetail_ams
CREATE TABLE IF NOT EXISTS `agreementdetail_ams` (
  `agreementdetailid` int(11) NOT NULL,
  `agreementinfoid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `numberofvacancies` int(5) DEFAULT NULL,
  `salary` int(10) DEFAULT NULL,
  `addendum` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `amstatus` tinyint(4) DEFAULT '0',
  `amstatusupdated` tinyint(4) DEFAULT '0',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.agreementdetail_ams: ~0 rows (approximately)
/*!40000 ALTER TABLE `agreementdetail_ams` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreementdetail_ams` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.agreementinfo
CREATE TABLE IF NOT EXISTS `agreementinfo` (
  `agreementinfoid` int(11) NOT NULL,
  `agreementid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.agreementinfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `agreementinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreementinfo` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.agreement_ams
CREATE TABLE IF NOT EXISTS `agreement_ams` (
  `agreementtempid` int(11) NOT NULL,
  `agreementid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `fromdate` datetime DEFAULT NULL,
  `todate` datetime DEFAULT NULL,
  `servicecharge` varchar(250) DEFAULT NULL,
  `wagetypeid` int(11) DEFAULT NULL,
  `wageyearid` int(11) DEFAULT NULL,
  `wageareaid` int(11) DEFAULT NULL,
  `wagecategoryid` int(11) DEFAULT NULL,
  `agreementstatusid` int(11) DEFAULT NULL,
  `agreementtypeid` int(11) DEFAULT NULL,
  `agtypeid` int(11) DEFAULT NULL,
  `tax` float(8,4) DEFAULT NULL,
  `optionaltype` varchar(250) DEFAULT NULL,
  `taxtype` int(11) DEFAULT NULL,
  `particularid` int(11) DEFAULT NULL,
  `addendum` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `amstatus` tinyint(4) DEFAULT '1',
  `addendumstatus` tinyint(4) DEFAULT '0',
  `changedby` varchar(50) DEFAULT NULL,
  `updatedfields` varchar(500) DEFAULT NULL,
  `modifdttm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.agreement_ams: ~0 rows (approximately)
/*!40000 ALTER TABLE `agreement_ams` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreement_ams` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.agreement_draft
CREATE TABLE IF NOT EXISTS `agreement_draft` (
  `agdraftid` int(11) NOT NULL,
  `agreementid` int(11) DEFAULT NULL,
  `agvalue` varchar(2000) DEFAULT NULL,
  `agtype` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `updatedttm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.agreement_draft: ~0 rows (approximately)
/*!40000 ALTER TABLE `agreement_draft` DISABLE KEYS */;
/*!40000 ALTER TABLE `agreement_draft` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.allemployees
CREATE TABLE IF NOT EXISTS `allemployees` (
  `employeeid` int(11) NOT NULL,
  `projno` varchar(20) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `texcono` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `fathername` varchar(100) DEFAULT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `genderid` tinyint(4) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `doj` datetime DEFAULT NULL,
  `district` varchar(100) DEFAULT NULL,
  `address1` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `village` varchar(100) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `civilqual` varchar(10) DEFAULT NULL,
  `rank` varchar(25) DEFAULT NULL,
  `corps` varchar(25) DEFAULT NULL,
  `trade` varchar(25) DEFAULT NULL,
  `armyqual` varchar(25) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `pfno` varchar(15) DEFAULT NULL,
  `entrydate` datetime DEFAULT NULL,
  `serviceno` varchar(15) DEFAULT NULL,
  `esmidcardno` varchar(15) DEFAULT NULL,
  `nominee` varchar(50) DEFAULT NULL,
  `pfname` varchar(50) DEFAULT NULL,
  `lastaccess` datetime DEFAULT NULL,
  `religion` varchar(15) DEFAULT NULL,
  `community` varchar(20) DEFAULT NULL,
  `nationality` varchar(20) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.allemployees: ~0 rows (approximately)
/*!40000 ALTER TABLE `allemployees` DISABLE KEYS */;
/*!40000 ALTER TABLE `allemployees` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.attendance
CREATE TABLE IF NOT EXISTS `attendance` (
  `attendanceid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `memberid` int(11) DEFAULT NULL,
  `jobpostingdetailid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `monthandyear` varchar(250) DEFAULT NULL,
  `one` decimal(10,2) DEFAULT '0.00',
  `two` decimal(10,2) DEFAULT '0.00',
  `three` decimal(10,2) DEFAULT '0.00',
  `four` decimal(10,2) DEFAULT '0.00',
  `five` decimal(10,2) DEFAULT '0.00',
  `six` decimal(10,2) DEFAULT '0.00',
  `seven` decimal(10,2) DEFAULT '0.00',
  `eight` decimal(10,2) DEFAULT '0.00',
  `nine` decimal(10,2) DEFAULT '0.00',
  `ten` decimal(10,2) DEFAULT '0.00',
  `eleven` decimal(10,2) DEFAULT '0.00',
  `twelve` decimal(10,2) DEFAULT '0.00',
  `thirteen` decimal(10,2) DEFAULT '0.00',
  `fourteen` decimal(10,2) DEFAULT '0.00',
  `fifteen` decimal(10,2) DEFAULT '0.00',
  `sixteen` decimal(10,2) DEFAULT '0.00',
  `seventeen` decimal(10,2) DEFAULT '0.00',
  `eighteen` decimal(10,2) DEFAULT '0.00',
  `nineteen` decimal(10,2) DEFAULT '0.00',
  `twenty` decimal(10,2) DEFAULT '0.00',
  `twentyone` decimal(10,2) DEFAULT '0.00',
  `twentytwo` decimal(10,2) DEFAULT '0.00',
  `twentythree` decimal(10,2) DEFAULT '0.00',
  `twentyfour` decimal(10,2) DEFAULT '0.00',
  `twentyfive` decimal(10,2) DEFAULT '0.00',
  `twentysix` decimal(10,2) DEFAULT '0.00',
  `twentyseven` decimal(10,2) DEFAULT '0.00',
  `twentyeight` decimal(10,2) DEFAULT '0.00',
  `twentynine` decimal(10,2) DEFAULT '0.00',
  `thirty` decimal(10,2) DEFAULT '0.00',
  `thirtyone` decimal(10,2) DEFAULT '0.00',
  `presentdays` tinyint(4) DEFAULT NULL,
  `eddays` decimal(10,2) DEFAULT '0.00',
  `reason` varchar(1000) DEFAULT NULL,
  `datedon` datetime DEFAULT CURRENT_TIMESTAMP,
  `athold` tinyint(4) DEFAULT '0',
  `edhold` tinyint(4) DEFAULT '0',
  `lreserve` tinyint(4) DEFAULT '0',
  `othours` decimal(10,2) DEFAULT '0.00',
  `status` tinyint(4) DEFAULT '0',
  `edstatus` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `cashier_rejected` tinyint(4) DEFAULT '0',
  `cao_rejected` tinyint(4) DEFAULT '0',
  `attendancesubmitted` tinyint(4) DEFAULT '0',
  `attendancereviewed` datetime DEFAULT NULL,
  `attendancesaved` int(11) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.attendance: ~0 rows (approximately)
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.attendance_other_expense
CREATE TABLE IF NOT EXISTS `attendance_other_expense` (
  `attendance_other_expense_id` int(11) NOT NULL,
  `attendanceid` int(11) NOT NULL,
  `memberid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `expense_type` int(11) DEFAULT NULL,
  `monthandyear` varchar(50) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL,
  `modified_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.attendance_other_expense: ~0 rows (approximately)
/*!40000 ALTER TABLE `attendance_other_expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance_other_expense` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.bankslip
CREATE TABLE IF NOT EXISTS `bankslip` (
  `bankslipid` int(11) NOT NULL,
  `passorderno` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1-waiting-2-approved,3-rejected',
  `active` int(11) DEFAULT '1',
  `changedby` varchar(50) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.bankslip: ~0 rows (approximately)
/*!40000 ALTER TABLE `bankslip` DISABLE KEYS */;
/*!40000 ALTER TABLE `bankslip` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.bank_slip_details
CREATE TABLE IF NOT EXISTS `bank_slip_details` (
  `bankdetailsid` int(11) NOT NULL,
  `bankslipid` int(11) DEFAULT NULL,
  `salaryslipno` int(11) DEFAULT NULL,
  `invoiceid` int(11) DEFAULT NULL,
  `createdtime` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.bank_slip_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `bank_slip_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_slip_details` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.carryforward
CREATE TABLE IF NOT EXISTS `carryforward` (
  `carryforwardid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `numberofvacancies` int(11) DEFAULT '0',
  `balancevacancies` int(11) DEFAULT '0',
  `comments` text,
  `inplaceof` text,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.carryforward: ~0 rows (approximately)
/*!40000 ALTER TABLE `carryforward` DISABLE KEYS */;
/*!40000 ALTER TABLE `carryforward` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.category
CREATE TABLE IF NOT EXISTS `category` (
  `categoryid` int(11) NOT NULL,
  `maincategoryid` int(11) NOT NULL,
  `categoryname` varchar(50) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `createdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.category: ~0 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.client
CREATE TABLE IF NOT EXISTS `client` (
  `clientid` int(11) NOT NULL,
  `organization` varchar(250) DEFAULT NULL,
  `contactname` varchar(100) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `addressline1` varchar(100) DEFAULT NULL,
  `addressline2` varchar(100) DEFAULT NULL,
  `addressline3` varchar(100) DEFAULT NULL,
  `pincode` varchar(8) DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `talukid` int(11) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `gstno` varchar(20) DEFAULT NULL,
  `gsttanno` varchar(20) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `tanno` varchar(20) DEFAULT NULL,
  `departmenttypeid` int(11) DEFAULT NULL,
  `department` varchar(150) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `approvalid` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `amstatus` int(11) DEFAULT '0',
  `changedby` varchar(50) DEFAULT NULL,
  `modifdttm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.client: ~6 rows (approximately)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`clientid`, `organization`, `contactname`, `image`, `email`, `mobile`, `phone`, `addressline1`, `addressline2`, `addressline3`, `pincode`, `districtid`, `talukid`, `regionid`, `stateid`, `countryid`, `password`, `gstno`, `gsttanno`, `panno`, `tanno`, `departmenttypeid`, `department`, `deptid`, `approvalid`, `active`, `amstatus`, `changedby`, `modifdttm`) VALUES
	(0, 'THE ASSISTANT DIRECTOR', 'tngov.png', 'adtamildevelopmentkarur@gmail.com', '9976617382', '04324-255077', 'THE ASSISTANT DIRECT', 'DEPARTMENT OF TAMIL DEVELOPMENT, DISTRICT COLLECTOR OFFICE', 'KARUR', '639006', '35', 119, 0, 318, 320, 0, '', '', '', 'CHEA13580A', '329', 0, '327', 324, 1, 127, 2147483647, '2020-04-17 14:39:58', NULL),
	(0, 'THE DISTRICT COLLECTOR', 'tngov.png', NULL, NULL, NULL, 'TALUK OFFICE', 'TIRUPPATTUR', 'SIVAGANGAI', '630211', '45', 179, 11, 318, 320, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 324, 1, 127, 2147483647, '2020-04-17 14:37:26', NULL),
	(3413, 'TASMAC, THIRUBUVANAM', 'THE DISTRICT MANAGER', 'tngov.png', NULL, NULL, NULL, 'TAMIL NADU STATE MARKETING CORPORATION LIMITED', 'THIRUBUVANAM, THIRUVIDAIMARUTHUR (TK)', 'THANJAVUR', '612103', 46, 187, 7, 318, 320, 'e3eb09926b94f992fa1f4939f771961b8fa6c4818254a8c7d5249bf758d7f38f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 324, 1, 0, NULL, '2020-04-17 14:49:17'),
	(3432, 'TAHSILDAR, TALUK OFFICE,  SS', 'THE DISTRICT COLLECTOR', 'tngov.png', NULL, NULL, NULL, 'TALUK OFFICE', 'SIPCOT', 'MANAMADURAI', '630606', 45, 177, 11, 318, 320, 'e3eb09926b94f992fa1f4939f771961b8fa6c4818254a8c7d5249bf758d7f38f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 324, 1, 0, NULL, '2020-04-17 14:49:28'),
	(0, 'TEst', 'test', 'defaultlogo.jpg', 'test@texco.in', '3929923290', '044-28333755', '', '', '', '', 48, 211, 4, 318, 320, 'eef158c999994d6b4fca11c54bf70cdce6219b2f5f27c4e1c8a211b373108a08', '', '', '', '', 331, '', 327, 324, 1, 0, '', '2020-04-17 15:50:49'),
	(3439, 'jkfbfsfsjfkjksdjk', 'jdfjkdjkn', 'defaultlogo.jpg', 'testew@texco.in', '8884874848', '', '', '', '', '', 28, 1185, 3, 318, 320, 'a5401ef548416625c681050716d3acbe053e62a50cad8b90c65405edc84c4884', '', '', '', '', 329, '', 328, 324, 1, 0, '', '2020-04-17 15:54:12');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.client_ams
CREATE TABLE IF NOT EXISTS `client_ams` (
  `clienttempid` int(11) NOT NULL,
  `clientid` int(11) NOT NULL,
  `organization` varchar(250) DEFAULT NULL,
  `contactname` varchar(100) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `addressline1` varchar(100) DEFAULT NULL,
  `addressline2` varchar(100) DEFAULT NULL,
  `addressline3` varchar(100) DEFAULT NULL,
  `pincode` varchar(8) DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `talukid` int(11) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `gstno` varchar(20) DEFAULT NULL,
  `gsttanno` varchar(20) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `tanno` varchar(20) DEFAULT NULL,
  `departmenttypeid` int(11) DEFAULT NULL,
  `department` varchar(150) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `approvalid` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `createdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `amstatus` int(11) DEFAULT NULL,
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL,
  `updatedfields` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.client_ams: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_ams` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_ams` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.closed_project_members
CREATE TABLE IF NOT EXISTS `closed_project_members` (
  `closedid` int(11) NOT NULL,
  `memberid` int(11) DEFAULT NULL,
  `texcono` text,
  `serviceno` text,
  `firstname` text,
  `isselected` int(11) DEFAULT '0',
  `isapplied` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '1',
  `createdttm` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.closed_project_members: ~0 rows (approximately)
/*!40000 ALTER TABLE `closed_project_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `closed_project_members` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.director
CREATE TABLE IF NOT EXISTS `director` (
  `directorid` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `designation` varchar(300) DEFAULT NULL,
  `dtitleid` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.director: ~0 rows (approximately)
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
/*!40000 ALTER TABLE `director` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.document
CREATE TABLE IF NOT EXISTS `document` (
  `documentid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `iconcolour` varchar(100) DEFAULT NULL,
  `documentname` varchar(250) DEFAULT NULL,
  `isgst` varchar(250) DEFAULT NULL,
  `iswage` varchar(250) DEFAULT NULL,
  `ispf` varchar(250) DEFAULT NULL,
  `folderid` int(11) DEFAULT '0',
  `foldername` varchar(250) DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.document: ~0 rows (approximately)
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.documentfolder
CREATE TABLE IF NOT EXISTS `documentfolder` (
  `folderid` int(11) NOT NULL,
  `foldername` varchar(50) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `createddate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.documentfolder: ~0 rows (approximately)
/*!40000 ALTER TABLE `documentfolder` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentfolder` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.due
CREATE TABLE IF NOT EXISTS `due` (
  `dueid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `monthandyear` varchar(250) DEFAULT NULL,
  `invoiceid` int(11) DEFAULT '0',
  `invoiceno` varchar(100) DEFAULT '0',
  `dueopening` decimal(18,2) DEFAULT NULL,
  `duepaid` decimal(18,2) DEFAULT '0.00',
  `duepending` decimal(18,2) DEFAULT NULL,
  `paiddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.due: ~0 rows (approximately)
/*!40000 ALTER TABLE `due` DISABLE KEYS */;
/*!40000 ALTER TABLE `due` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `employeeid` int(11) NOT NULL,
  `employeeno` varchar(150) DEFAULT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `desigid` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `doj` datetime DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.employee: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.employeehistory
CREATE TABLE IF NOT EXISTS `employeehistory` (
  `employeehistoryid` int(11) NOT NULL,
  `projno` varchar(50) DEFAULT NULL,
  `am` varchar(150) DEFAULT NULL,
  `texcono` varchar(150) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `fathername` varchar(200) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `doj` varchar(50) DEFAULT NULL,
  `district` varchar(150) DEFAULT NULL,
  `add1` varchar(150) DEFAULT NULL,
  `add2` varchar(150) DEFAULT NULL,
  `taluk` varchar(150) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `mobileno` int(11) DEFAULT NULL,
  `rank` varchar(50) DEFAULT NULL,
  `corps` varchar(50) DEFAULT NULL,
  `trade` varchar(50) DEFAULT NULL,
  `civilqual` varchar(100) DEFAULT NULL,
  `armyqual` varchar(100) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `pfno` varchar(50) DEFAULT NULL,
  `accountno` varchar(20) DEFAULT NULL,
  `entrydate` varchar(50) DEFAULT NULL,
  `serviceno` varchar(100) DEFAULT NULL,
  `esmidcardno` varchar(100) DEFAULT NULL,
  `nominee` varchar(150) DEFAULT NULL,
  `pfname` varchar(150) DEFAULT NULL,
  `lastaccess` varchar(50) DEFAULT NULL,
  `religion` varchar(150) DEFAULT NULL,
  `community` varchar(150) DEFAULT NULL,
  `nationality` varchar(200) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `createdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.employeehistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `employeehistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `employeehistory` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.experience
CREATE TABLE IF NOT EXISTS `experience` (
  `SERVICENO` varchar(15) DEFAULT NULL,
  `DOJ` varchar(10) DEFAULT NULL,
  `LASTACCESS` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.experience: ~0 rows (approximately)
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `galleryid` int(11) NOT NULL,
  `group` varchar(100) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `createdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.gallery: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
  `invoiceid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `monthandyear` varchar(250) DEFAULT NULL,
  `servicecharge` float(10,2) DEFAULT NULL,
  `tax` float(10,2) DEFAULT NULL,
  `servicecharges` float(10,2) DEFAULT NULL,
  `servicetax` float(10,2) DEFAULT NULL,
  `subtotal` decimal(18,2) DEFAULT NULL,
  `totalamount` decimal(18,2) DEFAULT NULL,
  `invoiceno` int(11) DEFAULT NULL,
  `paymentutrno` varchar(100) DEFAULT '',
  `arrearstatus` int(11) DEFAULT '0' COMMENT '0 - not created, 1- created',
  `invoicestatus` int(11) DEFAULT '1' COMMENT '0 - Waiting for Approval,1 - Supervisor Approved,2 - Cashier Approved,3 - CAO Approved,4 - HOLD,5 - Rejected',
  `paidamount` varchar(100) DEFAULT '',
  `fkinvoiceid` int(11) DEFAULT '0',
  `type` int(11) DEFAULT '0' COMMENT ' 0 -invoice,1 - arrear',
  `printcount` int(11) DEFAULT '0',
  `cashier_rejected` int(11) DEFAULT '0',
  `cao_rejected` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.invoice: ~0 rows (approximately)
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.invoicedetail
CREATE TABLE IF NOT EXISTS `invoicedetail` (
  `invoicedetailid` int(11) NOT NULL,
  `invoiceid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `salary` decimal(18,2) DEFAULT NULL,
  `noofduties` float DEFAULT NULL,
  `salaryamount` decimal(18,2) DEFAULT NULL,
  `monthandyear` varchar(50) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `noofperson` int(11) DEFAULT NULL,
  `fk_invoice_id` int(11) DEFAULT NULL COMMENT 'claim map ',
  `type` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `changedby` varchar(50) DEFAULT NULL,
  `createdttm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.invoicedetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `invoicedetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoicedetail` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobactivity
CREATE TABLE IF NOT EXISTS `jobactivity` (
  `jobactivityid` int(11) NOT NULL,
  `jobpostingdetailid` int(11) DEFAULT NULL,
  `memberid` int(11) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `currentvacancies` varchar(100) DEFAULT NULL,
  `jobstatuscode` int(11) DEFAULT '0',
  `texcono` varchar(150) DEFAULT NULL,
  `registrationno` int(11) DEFAULT NULL,
  `inplace` varchar(1000) DEFAULT '',
  `ipaddress` varchar(20) DEFAULT '',
  `othercat` varchar(1000) DEFAULT '',
  `effectivedate` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `isrejected` int(11) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.jobactivity: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobactivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobactivity` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobactivityhistory
CREATE TABLE IF NOT EXISTS `jobactivityhistory` (
  `jobactivityhistoryid` int(11) NOT NULL,
  `jobactivityid` int(11) DEFAULT NULL,
  `jobstatuscode` int(11) DEFAULT '0',
  `ondate` datetime DEFAULT NULL,
  `comments` varchar(50) DEFAULT NULL,
  `reason` varchar(250) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.jobactivityhistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobactivityhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobactivityhistory` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobeligibility
CREATE TABLE IF NOT EXISTS `jobeligibility` (
  `jobegid` int(11) NOT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `rank` varchar(20) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.jobeligibility: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobeligibility` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobeligibility` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobmaster
CREATE TABLE IF NOT EXISTS `jobmaster` (
  `jobmasterid` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `workinghours` varchar(20) DEFAULT NULL,
  `monthlywages` decimal(18,2) DEFAULT NULL,
  `servicecharge` decimal(18,2) DEFAULT NULL,
  `servicetax` decimal(18,2) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `sortorder` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL,
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.jobmaster: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobmaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobmaster` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobposting
CREATE TABLE IF NOT EXISTS `jobposting` (
  `jobpostingid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.jobposting: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobposting` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobposting` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobpostingdate
CREATE TABLE IF NOT EXISTS `jobpostingdate` (
  `id` int(11) NOT NULL,
  `importdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closedate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activedate` datetime DEFAULT NULL,
  `inactivedate` datetime DEFAULT NULL,
  `opendate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `registrationno` int(11) NOT NULL DEFAULT '1',
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.jobpostingdate: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobpostingdate` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobpostingdate` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.jobpostingdetail
CREATE TABLE IF NOT EXISTS `jobpostingdetail` (
  `jobpostingdetailid` int(11) NOT NULL,
  `jobpostingid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `numberofvacancies` varchar(100) DEFAULT NULL,
  `filledvacancies` varchar(100) DEFAULT NULL,
  `waitingvacancies` varchar(100) DEFAULT NULL,
  `agvacancies` varchar(100) DEFAULT NULL,
  `comments` text,
  `inplace` text,
  `posteddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `close` tinyint(4) DEFAULT '1',
  `closedate` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `appliedstatus` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.jobpostingdetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobpostingdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobpostingdetail` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `logid` int(11) NOT NULL,
  `method` varchar(10) DEFAULT NULL,
  `data` text,
  `createdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.lookupdomain
CREATE TABLE IF NOT EXISTS `lookupdomain` (
  `lkdmnid` int(11) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.lookupdomain: ~0 rows (approximately)
/*!40000 ALTER TABLE `lookupdomain` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookupdomain` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.lookupvalue
CREATE TABLE IF NOT EXISTS `lookupvalue` (
  `lkvalid` int(11) NOT NULL,
  `lkdmncode` varchar(10) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `sortorder` int(11) DEFAULT '0',
  `default` tinyint(4) DEFAULT '1',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.lookupvalue: ~0 rows (approximately)
/*!40000 ALTER TABLE `lookupvalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookupvalue` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.lookupvaluelink
CREATE TABLE IF NOT EXISTS `lookupvaluelink` (
  `linkid` int(11) NOT NULL,
  `from_lkvalid` int(11) DEFAULT NULL,
  `to_lkvalid` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.lookupvaluelink: ~0 rows (approximately)
/*!40000 ALTER TABLE `lookupvaluelink` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookupvaluelink` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.member
CREATE TABLE IF NOT EXISTS `member` (
  `memberid` int(11) NOT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `fathername` varchar(150) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `branchcode` varchar(20) DEFAULT NULL,
  `branchname` varchar(50) DEFAULT NULL,
  `ifsccode` varchar(50) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `village` varchar(200) DEFAULT NULL,
  `talukid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `pincode` varchar(8) DEFAULT NULL,
  `addressstatus` tinyint(4) DEFAULT '0',
  `communicationaddress` varchar(250) DEFAULT NULL,
  `aadhaarno` varchar(100) DEFAULT NULL,
  `genderid` tinyint(4) DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `doj` datetime DEFAULT NULL,
  `accountno` varchar(20) DEFAULT NULL,
  `nominee` varchar(150) DEFAULT NULL,
  `nomineerelationid` int(11) DEFAULT NULL,
  `rankid` int(11) DEFAULT NULL,
  `corpsid` int(11) DEFAULT NULL,
  `tradeid` int(11) DEFAULT NULL,
  `esmidno` varchar(150) DEFAULT NULL,
  `characterid` int(11) DEFAULT NULL,
  `religionid` int(11) DEFAULT NULL,
  `casteid` int(11) DEFAULT NULL,
  `civilqual` varchar(100) DEFAULT NULL,
  `armyqual` varchar(100) DEFAULT NULL,
  `registrationno` varchar(150) DEFAULT NULL,
  `texcono` varchar(150) DEFAULT NULL,
  `uanno` varchar(100) DEFAULT '',
  `panno` varchar(100) DEFAULT '',
  `serviceno` varchar(150) DEFAULT NULL,
  `lastaccess` datetime DEFAULT NULL,
  `dependentstatus` tinyint(4) DEFAULT '0',
  `dependentname` varchar(150) DEFAULT NULL,
  `nationality` varchar(200) DEFAULT NULL,
  `pfno` int(20) DEFAULT NULL,
  `pfname` varchar(200) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `repcostatus` tinyint(4) unsigned zerofill DEFAULT '0001',
  `pfstatus` tinyint(4) DEFAULT '1',
  `reasonforblock` varchar(500) DEFAULT NULL,
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL,
  `texconogdate` datetime DEFAULT NULL,
  `lastattendance` datetime DEFAULT NULL,
  `atmonthandyear` text,
  `attendancesaved` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.member: ~0 rows (approximately)
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
/*!40000 ALTER TABLE `member` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.memberblock
CREATE TABLE IF NOT EXISTS `memberblock` (
  `memberblockid` int(11) NOT NULL,
  `memberid` int(50) DEFAULT NULL,
  `reason` varchar(150) DEFAULT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `ispfblock` int(11) DEFAULT '0',
  `isrepcoblock` int(11) DEFAULT '0',
  `lifetimeblock` int(11) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.memberblock: ~0 rows (approximately)
/*!40000 ALTER TABLE `memberblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `memberblock` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.memberhistory
CREATE TABLE IF NOT EXISTS `memberhistory` (
  `memberhistoryid` int(11) NOT NULL,
  `texcono` varchar(50) DEFAULT NULL,
  `memberid` varchar(50) DEFAULT NULL,
  `projectno` varchar(50) DEFAULT NULL,
  `projectid` int(11) DEFAULT '0',
  `category` varchar(50) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT '0000-00-00 00:00:00',
  `active` int(11) DEFAULT '1',
  `estype` int(11) DEFAULT '1',
  `changedby` varchar(50) DEFAULT NULL,
  `approvalstatus` int(11) DEFAULT '0' COMMENT '0-Waiting for Approval, 1- Client Accepted, 2- Client Rejected, 3- Expired',
  `joining_date` datetime DEFAULT NULL,
  `closing_date` datetime DEFAULT NULL,
  `isrejected` int(11) DEFAULT '0' COMMENT '0 -No,1-Yes',
  `working_status` int(11) DEFAULT NULL COMMENT '0-waiting,1-working,2-closed',
  `isvacancyadded` int(11) DEFAULT '0',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table texco_audit_trail.memberhistory: ~0 rows (approximately)
/*!40000 ALTER TABLE `memberhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `memberhistory` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.member_temp
CREATE TABLE IF NOT EXISTS `member_temp` (
  `memberid` int(11) NOT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `fathername` varchar(150) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `village` varchar(200) DEFAULT NULL,
  `talukid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `pincode` varchar(8) DEFAULT NULL,
  `addressstatus` tinyint(4) DEFAULT '0',
  `communicationaddress` varchar(250) DEFAULT NULL,
  `aadhaarno` varchar(100) DEFAULT NULL,
  `genderid` tinyint(4) DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `doj` datetime DEFAULT NULL,
  `accountno` varchar(20) DEFAULT NULL,
  `nominee` varchar(150) DEFAULT NULL,
  `nomineerelationid` int(11) DEFAULT NULL,
  `rankid` int(11) DEFAULT NULL,
  `corpsid` int(11) DEFAULT NULL,
  `tradeid` int(11) DEFAULT NULL,
  `esmidno` varchar(150) DEFAULT NULL,
  `characterid` int(11) DEFAULT NULL,
  `religionid` int(11) DEFAULT NULL,
  `casteid` int(11) DEFAULT NULL,
  `civilqual` varchar(100) DEFAULT NULL,
  `armyqual` varchar(100) DEFAULT NULL,
  `registrationno` varchar(150) DEFAULT NULL,
  `texcono` varchar(150) DEFAULT NULL,
  `uanno` varchar(100) DEFAULT '',
  `panno` varchar(100) DEFAULT '',
  `serviceno` varchar(150) DEFAULT NULL,
  `lastaccess` datetime DEFAULT NULL,
  `dependentstatus` tinyint(4) DEFAULT '0',
  `dependentname` varchar(150) DEFAULT NULL,
  `nationality` varchar(200) DEFAULT NULL,
  `pfno` int(20) DEFAULT NULL,
  `pfname` varchar(200) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `createdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL,
  `texconogdate` datetime DEFAULT NULL,
  `lastattendance` datetime DEFAULT NULL,
  `atmonthandyear` text,
  `attendancesaved` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.member_temp: ~0 rows (approximately)
/*!40000 ALTER TABLE `member_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_temp` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.officer
CREATE TABLE IF NOT EXISTS `officer` (
  `officerid` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `designation` varchar(1000) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `sortorder` int(10) DEFAULT '1',
  `other` tinyint(4) DEFAULT '1',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.officer: ~0 rows (approximately)
/*!40000 ALTER TABLE `officer` DISABLE KEYS */;
/*!40000 ALTER TABLE `officer` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.pending_dues
CREATE TABLE IF NOT EXISTS `pending_dues` (
  `due_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `ref_no` text,
  `project_name` text,
  `opening_amount` text,
  `pending_amount` text,
  `due_on` datetime DEFAULT NULL,
  `over_due_by_days` text,
  `active` int(11) DEFAULT '1',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`due_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.pending_dues: ~0 rows (approximately)
/*!40000 ALTER TABLE `pending_dues` DISABLE KEYS */;
/*!40000 ALTER TABLE `pending_dues` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.postedlist
CREATE TABLE IF NOT EXISTS `postedlist` (
  `texcono` varchar(7) DEFAULT NULL,
  `serviceno` varchar(11) DEFAULT NULL,
  `name` varchar(31) DEFAULT NULL,
  `projectno` varchar(9) DEFAULT NULL,
  `category` varchar(18) DEFAULT NULL,
  `startdate` varchar(10) DEFAULT NULL,
  `memberid` int(1) DEFAULT NULL,
  `createdtime` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.postedlist: ~0 rows (approximately)
/*!40000 ALTER TABLE `postedlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `postedlist` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.project
CREATE TABLE IF NOT EXISTS `project` (
  `projectid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectno` varchar(20) DEFAULT NULL,
  `name` mediumtext,
  `districtid` int(11) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `statusid` int(11) DEFAULT NULL,
  `talukid` int(11) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `designation` mediumtext,
  `addressline1` mediumtext,
  `addressline2` mediumtext,
  `addressline3` mediumtext,
  `pincode` varchar(8) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `amstatus` int(11) DEFAULT '0',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.project: ~0 rows (approximately)
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.project_ams
CREATE TABLE IF NOT EXISTS `project_ams` (
  `projecttempid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `clientid` int(11) DEFAULT NULL,
  `projectno` varchar(20) DEFAULT NULL,
  `name` mediumtext,
  `districtid` int(11) DEFAULT NULL,
  `regionid` int(11) DEFAULT NULL,
  `statusid` int(11) DEFAULT NULL,
  `talukid` int(11) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `subcategoryid` int(11) DEFAULT NULL,
  `designation` mediumtext,
  `addressline1` mediumtext,
  `addressline2` mediumtext,
  `addressline3` mediumtext,
  `pincode` varchar(8) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `amstatus` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL,
  `updatedfields` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.project_ams: ~0 rows (approximately)
/*!40000 ALTER TABLE `project_ams` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_ams` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.region_details
CREATE TABLE IF NOT EXISTS `region_details` (
  `id` int(11) NOT NULL,
  `rg_id` int(11) DEFAULT NULL,
  `dt_id` int(11) DEFAULT NULL,
  `tk_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `modifiedttm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.region_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `region_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `region_details` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.regno
CREATE TABLE IF NOT EXISTS `regno` (
  `regnoid` int(11) NOT NULL,
  `dummy` varchar(10) DEFAULT NULL,
  `createddate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.regno: ~0 rows (approximately)
/*!40000 ALTER TABLE `regno` DISABLE KEYS */;
/*!40000 ALTER TABLE `regno` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.resetpassword
CREATE TABLE IF NOT EXISTS `resetpassword` (
  `resetpasswordid` int(11) NOT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `requesteddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `expirydate` datetime DEFAULT CURRENT_TIMESTAMP,
  `verificationcode` varchar(10) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifidttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.resetpassword: ~0 rows (approximately)
/*!40000 ALTER TABLE `resetpassword` DISABLE KEYS */;
/*!40000 ALTER TABLE `resetpassword` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.role
CREATE TABLE IF NOT EXISTS `role` (
  `roleid` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.role: ~0 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.rolepermission
CREATE TABLE IF NOT EXISTS `rolepermission` (
  `rolepermissionid` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `selected` tinyint(4) DEFAULT NULL,
  `sortorder` int(11) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.rolepermission: ~0 rows (approximately)
/*!40000 ALTER TABLE `rolepermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `rolepermission` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.salaryslip
CREATE TABLE IF NOT EXISTS `salaryslip` (
  `salaryslipid` int(11) NOT NULL,
  `payslipno` varchar(100) DEFAULT '0',
  `individualpayslipno` varchar(100) DEFAULT '0',
  `clientid` int(50) DEFAULT NULL,
  `projectid` int(50) DEFAULT NULL,
  `memberid` int(50) DEFAULT NULL,
  `jobpostingdetailid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `attendanceid` int(50) DEFAULT NULL,
  `monthandyear` varchar(250) DEFAULT NULL,
  `presentdays` decimal(10,2) DEFAULT NULL,
  `eddays` decimal(10,2) DEFAULT '1.00',
  `othours` decimal(10,2) DEFAULT NULL,
  `wagetypeid` int(50) DEFAULT NULL,
  `wageyearid` int(50) DEFAULT NULL,
  `wageareaid` int(50) DEFAULT NULL,
  `wagecategoryid` int(50) DEFAULT NULL,
  `ncbasic` decimal(10,2) DEFAULT '0.00',
  `edamount` decimal(10,2) DEFAULT '0.00',
  `basic` decimal(10,2) DEFAULT NULL,
  `da` decimal(10,2) DEFAULT NULL,
  `hra` decimal(10,2) DEFAULT NULL,
  `cca` decimal(10,2) DEFAULT NULL,
  `ma` decimal(10,2) DEFAULT NULL,
  `epf` decimal(10,2) DEFAULT NULL,
  `edli` decimal(10,2) DEFAULT NULL,
  `admchr` decimal(10,2) DEFAULT NULL,
  `bonus` decimal(10,2) DEFAULT NULL,
  `gratuity` decimal(10,2) DEFAULT NULL,
  `unifdt` decimal(10,2) DEFAULT NULL,
  `leapay` decimal(10,2) DEFAULT NULL,
  `conveyance` decimal(10,2) DEFAULT NULL,
  `washallow` decimal(10,2) DEFAULT NULL,
  `lic` decimal(10,2) DEFAULT NULL,
  `grossamount` decimal(10,2) DEFAULT NULL,
  `netpay` decimal(10,2) NOT NULL,
  `other1` decimal(10,2) DEFAULT NULL,
  `other2` decimal(10,2) DEFAULT NULL,
  `otherallowance` decimal(10,2) DEFAULT NULL,
  `otherdeductions` decimal(10,2) DEFAULT NULL,
  `ratehd` decimal(10,2) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `status` tinyint(4) DEFAULT '0' COMMENT '0 - Waiting for Approval,1 - Supervisor Approved,2 - Cashier Approved,3 - CAO Approved,4 - HOLD,5 - Rejected',
  `arrearstatus` tinyint(4) unsigned DEFAULT '0' COMMENT '0 - not created,1- created',
  `fk_salary_slip_id` int(11) DEFAULT NULL,
  `bill_type` tinyint(4) DEFAULT '0' COMMENT '0 -Salary Slip , 1- Arrear Salary Slip',
  `ss_print_count` tinyint(4) DEFAULT '0' COMMENT '0 - original, 1- duplicate ',
  `pf_print_count` tinyint(4) DEFAULT '0' COMMENT '0 - original, 1- duplicate ',
  `ind_print_count` tinyint(4) DEFAULT '0' COMMENT '0 - original, 1- duplicate ',
  `cashier_rejected` int(11) DEFAULT '0',
  `cao_rejected` int(11) DEFAULT '0',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.salaryslip: ~0 rows (approximately)
/*!40000 ALTER TABLE `salaryslip` DISABLE KEYS */;
/*!40000 ALTER TABLE `salaryslip` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.salaryslip_difference
CREATE TABLE IF NOT EXISTS `salaryslip_difference` (
  `differenceid` int(11) NOT NULL,
  `salaryslipid` int(50) NOT NULL,
  `payslipno` varchar(100) DEFAULT '0',
  `individualpayslipno` varchar(100) DEFAULT '0',
  `clientid` int(50) DEFAULT NULL,
  `projectid` int(50) DEFAULT NULL,
  `memberid` int(50) DEFAULT NULL,
  `jobpostingdetailid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `attendanceid` int(50) DEFAULT NULL,
  `monthandyear` varchar(250) DEFAULT NULL,
  `presentdays` decimal(10,2) DEFAULT NULL,
  `eddays` decimal(10,2) DEFAULT '1.00',
  `othours` decimal(10,2) DEFAULT NULL,
  `wagetypeid` int(50) DEFAULT NULL,
  `wageyearid` int(50) DEFAULT NULL,
  `wageareaid` int(50) DEFAULT NULL,
  `wagecategoryid` int(50) DEFAULT NULL,
  `ncbasic` decimal(10,2) DEFAULT '0.00',
  `edamount` decimal(10,2) DEFAULT '0.00',
  `basic` decimal(10,2) DEFAULT NULL,
  `da` decimal(10,2) DEFAULT NULL,
  `hra` decimal(10,2) DEFAULT NULL,
  `cca` decimal(10,2) DEFAULT NULL,
  `ma` decimal(10,2) DEFAULT NULL,
  `epf` decimal(10,2) DEFAULT NULL,
  `edli` decimal(10,2) DEFAULT NULL,
  `admchr` decimal(10,2) DEFAULT NULL,
  `bonus` decimal(10,2) DEFAULT NULL,
  `gratuity` decimal(10,2) DEFAULT NULL,
  `unifdt` decimal(10,2) DEFAULT NULL,
  `leapay` decimal(10,2) DEFAULT NULL,
  `conveyance` decimal(10,2) DEFAULT NULL,
  `washallow` decimal(10,2) DEFAULT NULL,
  `lic` decimal(10,2) DEFAULT NULL,
  `grossamount` decimal(10,2) DEFAULT NULL,
  `netpay` decimal(10,2) DEFAULT NULL,
  `other1` decimal(10,2) DEFAULT NULL,
  `other2` decimal(10,2) DEFAULT NULL,
  `ratehd` decimal(10,2) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `status` tinyint(4) DEFAULT '1' COMMENT '0-Rejected, 1- Created, 2 -Supervisor Approved, 3- cashier Approved, 4- CAO Approved',
  `modifdttm` datetime DEFAULT CURRENT_TIMESTAMP,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table texco_audit_trail.salaryslip_difference: ~0 rows (approximately)
/*!40000 ALTER TABLE `salaryslip_difference` DISABLE KEYS */;
/*!40000 ALTER TABLE `salaryslip_difference` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `settingid` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.setting: ~0 rows (approximately)
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.subcategory
CREATE TABLE IF NOT EXISTS `subcategory` (
  `subcategoryid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `subcategoryname` varchar(50) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table texco_audit_trail.subcategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.texconologs
CREATE TABLE IF NOT EXISTS `texconologs` (
  `texconologid` int(11) NOT NULL,
  `texcono` varchar(50) DEFAULT NULL,
  `memberid` int(11) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table texco_audit_trail.texconologs: ~0 rows (approximately)
/*!40000 ALTER TABLE `texconologs` DISABLE KEYS */;
/*!40000 ALTER TABLE `texconologs` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.wages
CREATE TABLE IF NOT EXISTS `wages` (
  `wageid` int(11) NOT NULL,
  `wagetypeid` int(11) DEFAULT NULL,
  `wagecategoryid` int(11) DEFAULT NULL,
  `wageyearid` int(11) DEFAULT NULL,
  `wageareaid` int(11) DEFAULT NULL,
  `jobmasterid` int(11) DEFAULT NULL,
  `particularid` int(11) DEFAULT NULL,
  `particularamount` decimal(18,2) DEFAULT NULL COMMENT 'Basic Amount other Percentage',
  `particularpercent` decimal(18,2) DEFAULT '0.00',
  `active` tinyint(4) DEFAULT '1',
  `modifdttm` datetime DEFAULT NULL,
  `changedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table texco_audit_trail.wages: ~0 rows (approximately)
/*!40000 ALTER TABLE `wages` DISABLE KEYS */;
/*!40000 ALTER TABLE `wages` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.wage_category_master
CREATE TABLE IF NOT EXISTS `wage_category_master` (
  `category_id` int(11) NOT NULL,
  `category_code` varchar(50) DEFAULT NULL,
  `category_description` varchar(50) DEFAULT NULL,
  `wageyearid` int(11) DEFAULT NULL,
  `wagetypeid` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.wage_category_master: ~0 rows (approximately)
/*!40000 ALTER TABLE `wage_category_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `wage_category_master` ENABLE KEYS */;

-- Dumping structure for table texco_audit_trail.wage_login_details
CREATE TABLE IF NOT EXISTS `wage_login_details` (
  `ref_id` int(11) NOT NULL,
  `login_status` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '1',
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table texco_audit_trail.wage_login_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `wage_login_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `wage_login_details` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
