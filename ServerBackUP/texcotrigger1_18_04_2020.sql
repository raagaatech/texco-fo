-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreementdetail_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreementdetail_after_insert` AFTER INSERT ON `agreementdetail` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreementdetail
VALUES(NEW.agreementdetailid,NEW.agreementinfoid,NEW.jobmasterid,NEW.numberofvacancies,NEW.salary,NEW.addendum,NEW.active,NEW.amstatus,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreementdetail_ams_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreementdetail_ams_after_insert` AFTER INSERT ON `agreementdetail_ams` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreementdetail_ams
VALUES(NEW.agreementdetailid,NEW.agreementinfoid,NEW.jobmasterid,NEW.numberofvacancies,NEW.salary,NEW.addendum,NEW.active,NEW.amstatus,NEW.amstatusupdated,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreementdetail_ams_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreementdetail_ams_before_update` BEFORE UPDATE ON `agreementdetail_ams` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreementdetail_ams
VALUES(OLD.agreementdetailid,OLD.agreementinfoid,OLD.jobmasterid,OLD.numberofvacancies,OLD.salary,OLD.addendum,OLD.active,OLD.amstatus,OLD.amstatusupdated,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreementdetail_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreementdetail_before_update` BEFORE UPDATE ON `agreementdetail` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreementdetail
VALUES(OLD.agreementdetailid,OLD.agreementinfoid,OLD.jobmasterid,OLD.numberofvacancies,OLD.salary,OLD.addendum,OLD.active,OLD.amstatus,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreementinfo_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreementinfo_after_insert` AFTER INSERT ON `agreementinfo` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreementinfo
VALUES(NEW.agreementinfoid,NEW.agreementid,NEW.projectid,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreementinfo_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreementinfo_before_update` BEFORE UPDATE ON `agreementinfo` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreementinfo
VALUES(OLD.agreementinfoid,OLD.agreementid,OLD.projectid,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreement_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreement_after_insert` AFTER INSERT ON `agreement` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreement
VALUES(NEW.agreementid,NEW.clientid,NEW.fromdate,NEW.todate,NEW.servicecharge,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.agreementstatusid,NEW.agreementtypeid,NEW.agtypeid,NEW.tax,NEW.taxtype,NEW.particularid,NEW.optionaltype,NEW.addendum,NEW.active,NEW.amstatus,NEW.addendumstatus,NEW.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreement_ams_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreement_ams_after_insert` AFTER INSERT ON `agreement_ams` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreement_ams
VALUES(NEW.agreementtempid,NEW.agreementid,NEW.clientid,NEW.fromdate,NEW.todate,NEW.servicecharge,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.agreementstatusid,NEW.agreementtypeid,NEW.agtypeid,NEW.tax,NEW.taxtype,NEW.particularid,NEW.optionaltype,NEW.addendum,NEW.active,NEW.amstatus,NEW.addendumstatus,NEW.changedby,NEW.updatedfields,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreement_ams_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreement_ams_before_update` BEFORE UPDATE ON `agreement_ams` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreement_ams
VALUES(OLD.agreementtempid,OLD.agreementid,OLD.clientid,OLD.fromdate,OLD.todate,OLD.servicecharge,OLD.wagetypeid,OLD.wageyearid,OLD.wageareaid,OLD.wagecategoryid,OLD.agreementstatusid,OLD.agreementtypeid,OLD.agtypeid,OLD.tax,OLD.taxtype,OLD.particularid,OLD.optionaltype,OLD.addendum,OLD.active,OLD.amstatus,OLD.addendumstatus,OLD.changedby,OLD.updatedfields,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreement_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreement_before_update` BEFORE UPDATE ON `agreement` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreement
VALUES(OLD.agreementid,OLD.clientid,OLD.fromdate,OLD.todate,OLD.servicecharge,OLD.wagetypeid,OLD.wageyearid,OLD.wageareaid,OLD.wagecategoryid,OLD.agreementstatusid,OLD.agreementtypeid,OLD.agtypeid,OLD.tax,OLD.taxtype,OLD.particularid,OLD.optionaltype,OLD.addendum,OLD.active,OLD.amstatus,OLD.addendumstatus,OLD.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreement_draft_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreement_draft_after_insert` AFTER INSERT ON `agreement_draft` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreement_draft
VALUES(NEW.agdraftid,NEW.agreementid,NEW.agvalue,NEW.agtype,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_agreement_draft_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_agreement_draft_before_update` BEFORE UPDATE ON `agreement_draft` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.agreement_draft
VALUES(OLD.agdraftid,OLD.agreementid,OLD.agvalue,OLD.agtype,OLD.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_allemployees_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_allemployees_after_insert` AFTER INSERT ON `allemployees` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.allemployees
VALUES(NEW.employeeid,NEW.projno,NEW.region,NEW.texcono,NEW.name,NEW.fathername,NEW.sex,NEW.genderid,NEW.dob,NEW.doj,NEW.district,NEW.address1,NEW.address2,NEW.village,NEW.pincode,NEW.mobile,NEW.civilqual,NEW.rank,NEW.corps,NEW.trade,NEW.armyqual,NEW.category,NEW.designation,NEW.pfno,NEW.entrydate,NEW.serviceno,NEW.esmidcardno,NEW.nominee,NEW.pfname,NEW.lastaccess,NEW.religion,NEW.community,NEW.nationality,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_allemployees_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_allemployees_before_update` BEFORE UPDATE ON `allemployees` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.allemployees
VALUES(OLD.employeeid,OLD.projno,OLD.region,OLD.texcono,OLD.name,OLD.fathername,OLD.sex,OLD.genderid,OLD.dob,OLD.doj,OLD.district,OLD.address1,OLD.address2,OLD.village,OLD.pincode,OLD.mobile,OLD.civilqual,OLD.rank,OLD.corps,OLD.trade,OLD.armyqual,OLD.category,OLD.designation,OLD.pfno,OLD.entrydate,OLD.serviceno,OLD.esmidcardno,OLD.nominee,OLD.pfname,OLD.lastaccess,OLD.religion,OLD.community,OLD.nationality,OLD.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_attendance_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_attendance_after_insert` AFTER INSERT ON `attendance` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.attendance
VALUES(NEW.attendanceid,NEW.clientid,NEW.projectid,NEW.memberid,NEW.jobpostingdetailid,NEW.jobmasterid,NEW.monthandyear,NEW.one,NEW.two,NEW.three,NEW.four,NEW.five,NEW.six,NEW.seven,NEW.eight,NEW.nine,NEW.ten,NEW.eleven,NEW.twelve,NEW.thirteen,NEW.fourteen,NEW.fifteen,NEW.sixteen,NEW.seventeen,NEW.eighteen,NEW.nineteen,NEW.twenty,NEW.twentyone,NEW.twentytwo,NEW.twentythree,NEW.twentyfour,NEW.twentyfive,NEW.twentysix,NEW.twentyseven,NEW.twentyeight,NEW.twentynine,NEW.thirty,NEW.thirtyone,NEW.presentdays,NEW.eddays,NEW.reason,NEW.datedon,NEW.athold,NEW.edhold,NEW.lreserve,NEW.othours,NEW.status,NEW.edstatus,NEW.active,NEW.cashier_rejected,NEW.cao_rejected,NEW.attendancesubmitted,NEW.attendancereviewed,NEW.attendancesaved,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_attendance_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_attendance_before_update` BEFORE UPDATE ON `attendance` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.attendance
VALUES(OLD.attendanceid,OLD.clientid,OLD.projectid,OLD.memberid,OLD.jobpostingdetailid,OLD.jobmasterid,OLD.monthandyear,OLD.one,OLD.two,OLD.three,OLD.four,OLD.five,OLD.six,OLD.seven,OLD.eight,OLD.nine,OLD.ten,OLD.eleven,OLD.twelve,OLD.thirteen,OLD.fourteen,OLD.fifteen,OLD.sixteen,OLD.seventeen,OLD.eighteen,OLD.nineteen,OLD.twenty,OLD.twentyone,OLD.twentytwo,OLD.twentythree,OLD.twentyfour,OLD.twentyfive,OLD.twentysix,OLD.twentyseven,OLD.twentyeight,OLD.twentynine,OLD.thirty,OLD.thirtyone,OLD.presentdays,OLD.eddays,OLD.reason,OLD.datedon,OLD.athold,OLD.edhold,OLD.lreserve,OLD.othours,OLD.status,OLD.edstatus,OLD.active,OLD.cashier_rejected,OLD.cao_rejected,OLD.attendancesubmitted,OLD.attendancereviewed,OLD.attendancesaved,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_attendance_other_expense_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_attendance_other_expense_after_insert` AFTER INSERT ON `attendance_other_expense` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.attendance_other_expense
VALUES(NEW.attendance_other_expense_id,NEW.attendanceid,NEW.memberid,NEW.projectid,NEW.expense_type,NEW.monthandyear,NEW.amount,NEW.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_attendance_other_expense_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_attendance_other_expense_before_update` BEFORE UPDATE ON `attendance_other_expense` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.attendance_other_expense
VALUES(OLD.attendance_other_expense_id,OLD.attendanceid,OLD.memberid,OLD.projectid,OLD.expense_type,OLD.monthandyear,OLD.amount,OLD.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_bankslip_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_bankslip_after_insert` AFTER INSERT ON `bankslip` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.bankslip
VALUES(NEW.bankslipid,NEW.passorderno,NEW.status,NEW.active,NEW.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_bankslip_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_bankslip_before_update` BEFORE UPDATE ON `bankslip` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.bankslip
VALUES(OLD.bankslipid,OLD.passorderno,OLD.status,OLD.active,OLD.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_bank_slip_details_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_bank_slip_details_after_insert` AFTER INSERT ON `bank_slip_details` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.bank_slip_details
VALUES(NEW.bankdetailsid,NEW.bankslipid,NEW.salaryslipno,NEW.invoiceid,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_bank_slip_details_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_bank_slip_details_before_update` BEFORE UPDATE ON `bank_slip_details` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.bank_slip_details
VALUES(OLD.bankdetailsid,OLD.bankslipid,OLD.salaryslipno,OLD.invoiceid,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_carryforward_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_carryforward_after_insert` AFTER INSERT ON `carryforward` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.carryforward
VALUES(NEW.carryforwardid,NEW.clientid,NEW.projectid,NEW.jobmasterid,NEW.numberofvacancies,NEW.balancevacancies,NEW.comments,NEW.inplaceof,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_carryforward_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_carryforward_before_update` BEFORE UPDATE ON `carryforward` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.carryforward
VALUES(OLD.carryforwardid,OLD.clientid,OLD.projectid,OLD.jobmasterid,OLD.numberofvacancies,OLD.balancevacancies,OLD.comments,OLD.inplaceof,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_category_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_category_after_insert` AFTER INSERT ON `category` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.category
VALUES(NEW.categoryid,NEW.maincategoryid,NEW.categoryname,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_category_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_category_before_update` BEFORE UPDATE ON `category` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.category
VALUES(OLD.categoryid,OLD.maincategoryid,OLD.categoryname,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_client_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_client_after_insert` AFTER INSERT ON `client` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.`client`
VALUES(NEW.clientid,NEW.organization,NEW.contactname,NEW.image,NEW.email,NEW.mobile,NEW.phone,NEW.addressline1,NEW.addressline2
,NEW.addressline3,NEW.pincode,NEW.districtid,NEW.talukid,NEW.regionid,NEW.stateid,NEW.countryid,NEW.password,NEW.gstno,
NEW.gsttanno,NEW.panno,NEW.tanno,NEW.departmenttypeid,NEW.department,
NEW.deptid,NEW.approvalid,NEW.active,NEW.amstatus,NEW.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_client_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_client_before_update` BEFORE UPDATE ON `client` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.`client`
VALUES(OLD.clientid,OLD.organization,OLD.contactname,OLD.image,OLD.email,OLD.mobile,OLD.phone,OLD.addressline1,OLD.addressline2
,OLD.addressline3,OLD.pincode,OLD.districtid,OLD.talukid,OLD.regionid,OLD.stateid,OLD.countryid,OLD.password,OLD.gstno,
OLD.gsttanno,OLD.panno,OLD.tanno,OLD.departmenttypeid,OLD.department,
OLD.deptid,OLD.approvalid,OLD.active,OLD.amstatus,OLD.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_closed_project_members_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_closed_project_members_after_insert` AFTER INSERT ON `closed_project_members` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.closed_project_members
VALUES(NEW.closedid,NEW.memberid,NEW.texcono,NEW.serviceno,NEW.firstname,NEW.isselected,NEW.isapplied,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_closed_project_members_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_closed_project_members_before_update` BEFORE UPDATE ON `closed_project_members` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.closed_project_members
VALUES(OLD.closedid,OLD.memberid,OLD.texcono,OLD.serviceno,OLD.firstname,OLD.isselected,OLD.isapplied,OLD.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_director_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_director_after_insert` AFTER INSERT ON `director` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.director
VALUES(NEW.directorid,NEW.name,NEW.designation,NEW.dtitleid,NEW.email,NEW.mobile,NEW.phone,NEW.address,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_director_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_director_before_update` BEFORE UPDATE ON `director` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.director
VALUES(OLD.directorid,OLD.name,OLD.designation,OLD.dtitleid,OLD.email,OLD.mobile,OLD.phone,OLD.address,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_documentfolder_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_documentfolder_after_insert` AFTER INSERT ON `documentfolder` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.documentfolder
VALUES(NEW.folderid,NEW.foldername,NEW.status,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_documentfolder_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_documentfolder_before_update` BEFORE UPDATE ON `documentfolder` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.documentfolder
VALUES(OLD.folderid,OLD.foldername,OLD.status,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_document_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_document_after_insert` AFTER INSERT ON `document` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.document
VALUES(NEW.documentid,NEW.name,NEW.icon,NEW.iconcolour,NEW.documentname,NEW.isgst,NEW.iswage,NEW.ispf,NEW.folderid,NEW.foldername,NEW.description,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_document_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_document_before_update` BEFORE UPDATE ON `document` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.document
VALUES(OLD.documentid,OLD.name,OLD.icon,OLD.iconcolour,OLD.documentname,OLD.isgst,OLD.iswage,OLD.ispf,OLD.folderid,OLD.foldername,OLD.description,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_due_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_due_after_insert` AFTER INSERT ON `due` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.due
VALUES(NEW.dueid,NEW.clientid,NEW.projectid,NEW.monthandyear,NEW.invoiceid,NEW.invoiceno,NEW.dueopening,NEW.duepaid,NEW.duepending,NEW.paiddate,NEW.status,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_due_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_due_before_update` BEFORE UPDATE ON `due` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.due
VALUES(OLD.dueid,OLD.clientid,OLD.projectid,OLD.monthandyear,OLD.invoiceid,OLD.invoiceno,OLD.dueopening,OLD.duepaid,OLD.duepending,OLD.paiddate,OLD.status,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_employee_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_employee_after_insert` AFTER INSERT ON `employee` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.employee
VALUES(NEW.employeeid,NEW.employeeno,NEW.firstname,NEW.lastname,NEW.regionid,NEW.desigid,NEW.email,NEW.mobile,NEW.phone,NEW.doj,NEW.address,NEW.roleid,NEW.password,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_employee_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_employee_before_update` BEFORE UPDATE ON `employee` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.employee
VALUES(OLD.employeeid,OLD.employeeno,OLD.firstname,OLD.lastname,OLD.regionid,OLD.desigid,OLD.email,OLD.mobile,OLD.phone,OLD.doj,OLD.address,OLD.roleid,OLD.password,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_gallery_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_gallery_after_insert` AFTER INSERT ON `gallery` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.gallery
VALUES(NEW.galleryid,NEW.group,NEW.description,NEW.image,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_gallery_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_gallery_before_update` BEFORE UPDATE ON `gallery` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.gallery
VALUES(OLD.galleryid,OLD.group,OLD.description,OLD.image,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_invoicedetail_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_invoicedetail_after_insert` AFTER INSERT ON `invoicedetail` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.invoicedetail
VALUES(NEW.invoicedetailid,NEW.invoiceid,NEW.projectid,NEW.clientid,NEW.jobmasterid,NEW.salary,NEW.noofduties,NEW.salaryamount,NEW.monthandyear,NEW.days,NEW.noofperson,NEW.fk_invoice_id,NEW.type,NEW.active,NEW.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_invoicedetail_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_invoicedetail_before_update` BEFORE UPDATE ON `invoicedetail` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.invoicedetail
VALUES(OLD.invoicedetailid,OLD.invoiceid,OLD.projectid,OLD.clientid,OLD.jobmasterid,OLD.salary,OLD.noofduties,OLD.salaryamount,OLD.monthandyear,OLD.days,OLD.noofperson,OLD.fk_invoice_id,OLD.type,OLD.active,OLD.changedby,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_invoice_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_invoice_after_insert` AFTER INSERT ON `invoice` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.invoice
VALUES(NEW.invoiceid,NEW.clientid,NEW.projectid,NEW.monthandyear,NEW.servicecharge,NEW.tax,NEW.servicecharges,NEW.servicetax,NEW.subtotal,NEW.totalamount,NEW.invoiceno,NEW.paymentutrno,NEW.arrearstatus,NEW.invoicestatus,NEW.paidamount,NEW.fkinvoiceid,NEW.type,NEW.printcount,NEW.cashier_rejected,NEW.cao_rejected,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_invoice_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_invoice_before_update` BEFORE UPDATE ON `invoice` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.invoice
VALUES(OLD.invoiceid,OLD.clientid,OLD.projectid,OLD.monthandyear,OLD.servicecharge,OLD.tax,OLD.servicecharges,OLD.servicetax,OLD.subtotal,OLD.totalamount,OLD.invoiceno,OLD.paymentutrno,OLD.arrearstatus,OLD.invoicestatus,OLD.paidamount,OLD.fkinvoiceid,OLD.type,OLD.printcount,OLD.cashier_rejected,OLD.cao_rejected,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobactivityhistory_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobactivityhistory_after_insert` AFTER INSERT ON `jobactivityhistory` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobactivityhistory
VALUES(NEW.jobactivityhistoryid,NEW.jobactivityid,NEW.jobstatuscode,NEW.ondate,NEW.comments,NEW.reason,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobactivityhistory_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobactivityhistory_before_update` BEFORE UPDATE ON `jobactivityhistory` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobactivityhistory
VALUES(OLD.jobactivityhistoryid,OLD.jobactivityid,OLD.jobstatuscode,OLD.ondate,OLD.comments,OLD.reason,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobactivity_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobactivity_after_insert` AFTER INSERT ON `jobactivity` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobactivity
VALUES(NEW.jobactivityid,NEW.jobpostingdetailid,NEW.memberid,NEW.clientid,NEW.projectid,NEW.currentvacancies,NEW.jobstatuscode,NEW.texcono,NEW.registrationno,NEW.inplace,NEW.ipaddress,NEW.othercat,NEW.effectivedate,NEW.active,NEW.isrejected,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobactivity_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobactivity_before_update` BEFORE UPDATE ON `jobactivity` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobactivity
VALUES(OLD.jobactivityid,OLD.jobpostingdetailid,OLD.memberid,OLD.clientid,OLD.projectid,OLD.currentvacancies,OLD.jobstatuscode,OLD.texcono,OLD.registrationno,OLD.inplace,OLD.ipaddress,OLD.othercat,OLD.effectivedate,OLD.active,OLD.isrejected,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobeligibility_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobeligibility_after_insert` AFTER INSERT ON `jobeligibility` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobeligibility
VALUES(NEW.jobegid,NEW.jobmasterid,NEW.rank,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobeligibility_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobeligibility_before_update` BEFORE UPDATE ON `jobeligibility` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobeligibility
VALUES(OLD.jobegid,OLD.jobmasterid,OLD.rank,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobmaster_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobmaster_after_insert` AFTER INSERT ON `jobmaster` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobmaster
VALUES(NEW.jobmasterid,NEW.code,NEW.name,NOW.workinghours,NEW.monthlywages,NEW.servicecharge,NEW.servicetax,NEW.comments,NEW.active,NEW.sortorder,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobmaster_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobmaster_before_update` BEFORE UPDATE ON `jobmaster` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobmaster
VALUES(OLD.jobmasterid,OLD.code,OLD.name,NOW.workinghours,OLD.monthlywages,OLD.servicecharge,OLD.servicetax,OLD.comments,OLD.active,OLD.sortorder,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobpostingdate_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobpostingdate_after_insert` AFTER INSERT ON `jobpostingdate` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobpostingdate
VALUES(NEW.id,NEW.importdate,NEW.closedate,NEW.activedate,NEW.inactivedate,NEW.opendate,NEW.registrationno,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobpostingdate_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobpostingdate_before_update` BEFORE UPDATE ON `jobpostingdate` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobpostingdate
VALUES(NEW.id,NEW.importdate,NEW.closedate,NEW.activedate,NEW.inactivedate,NEW.opendate,NEW.registrationno,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobpostingdetail_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobpostingdetail_after_insert` AFTER INSERT ON `jobpostingdetail` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobpostingdetail
VALUES(NEW.jobpostingdetailid,NEW.jobpostingid,NEW.jobmasterid,NEW.numberofvacancies,NEW.filledvacancies,NEW.waitingvacancies,NEW.agvacancies,NEW.comments,NEW.inplace,NEW.posteddate,NEW.startdate,NEW.enddate,NEW.close,NEW.closedate,NEW.active,NEW.appliedstatus,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobpostingdetail_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobpostingdetail_before_update` BEFORE UPDATE ON `jobpostingdetail` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobpostingdetail
VALUES(OLD.jobpostingdetailid,OLD.jobpostingid,OLD.jobmasterid,OLD.numberofvacancies,OLD.filledvacancies,OLD.waitingvacancies,OLD.agvacancies,OLD.comments,OLD.inplace,OLD.posteddate,OLD.startdate,OLD.enddate,OLD.close,OLD.closedate,OLD.active,OLD.appliedstatus,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobposting_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobposting_after_insert` AFTER INSERT ON `jobposting` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobposting
VALUES(NEW.jobpostingid,NEW.clientid,NEW.projectid,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_jobposting_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_jobposting_before_update` BEFORE UPDATE ON `jobposting` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.jobposting
VALUES(OLD.jobpostingid,OLD.clientid,OLD.projectid,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_logs_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_logs_after_insert` AFTER INSERT ON `logs` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.logs
VALUES(NEW.logid,NEW.method,NEW.data,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_logs_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_logs_before_update` BEFORE UPDATE ON `logs` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.logs
VALUES(OLD.logid,OLD.method,OLD.data,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_lookupdomain_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_lookupdomain_after_insert` AFTER INSERT ON `lookupdomain` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.lookupdomain
VALUES(NEW.lkdmnid,NEW.code,NEW.description,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_lookupdomain_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_lookupdomain_before_update` BEFORE UPDATE ON `lookupdomain` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.lookupdomain
VALUES(OLD.lkdmnid,OLD.code,OLD.description,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_lookupvaluelink_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_lookupvaluelink_after_insert` AFTER INSERT ON `lookupvaluelink` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.lookupvaluelink
VALUES(NEW.linkid,NEW.from_lkvalid,NEW.to_lkvalid,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_lookupvaluelink_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_lookupvaluelink_before_update` BEFORE UPDATE ON `lookupvaluelink` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.lookupvaluelink
VALUES(OLD.linkid,OLD.from_lkvalid,OLD.to_lkvalid,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_lookupvalue_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_lookupvalue_after_insert` AFTER INSERT ON `lookupvalue` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.lookupvalue
VALUES(NEW.lkvalid,NEW.lkdmncode,NEW.code,NEW.description,NEW.sortorder,NEW.default,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_lookupvalue_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_lookupvalue_before_update` BEFORE UPDATE ON `lookupvalue` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.lookupvalue
VALUES(OLD.lkvalid,OLD.lkdmncode,OLD.code,OLD.description,OLD.sortorder,OLD.default,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_memberblock_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_memberblock_after_insert` AFTER INSERT ON `memberblock` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.memberblock
VALUES(NEW.memberblockid,NEW.memberid,NEW.reason,NEW.comment,NEW.enddate,NEW.ispfblock,NEW.isrepcoblock,NEW.lifetimeblock,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_memberblock_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_memberblock_before_update` BEFORE UPDATE ON `memberblock` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.memberblock
VALUES(OLD.memberblockid,OLD.memberid,OLD.reason,OLD.comment,OLD.enddate,OLD.ispfblock,OLD.isrepcoblock,OLD.lifetimeblock,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_memberhistory_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_memberhistory_after_insert` AFTER INSERT ON `memberhistory` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.memberhistory
VALUES(NEW.memberhistoryid,NEW.texcono,NEW.memberid,NEW.projectno,NEW.projectid,NEW.category,NEW.startdate,NEW.enddate,NEW.active,NEW.estype,NEW.changedby,NEW.approvalstatus,NEW.joining_date,NEW.closing_date,NEW.isrejected,NEW.working_status,NEW.isvacancyadded,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_memberhistory_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_memberhistory_before_update` BEFORE UPDATE ON `memberhistory` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.memberhistory
VALUES(OLD.memberhistoryid,OLD.texcono,OLD.memberid,OLD.projectno,OLD.projectid,OLD.category,OLD.startdate,OLD.enddate,OLD.active,OLD.estype,OLD.changedby,OLD.approvalstatus,OLD.joining_date,OLD.closing_date,OLD.isrejected,OLD.working_status,OLD.isvacancyadded,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_member_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_member_after_insert` AFTER INSERT ON `member` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.member
VALUES(NEW.memberid,NEW.firstname,NEW.lastname,NEW.fathername,NEW.dob,NEW.email,NEW.mobile,NEW.branchcode,NEW.branchname,NEW.ifsccode,NEW.address,NEW.village,NEW.talukid,NEW.stateid,NEW.countryid,NEW.pincode,NEW.addressstatus,NEW.communicationaddress,NEW.aadhaarno,NEW.genderid,NEW.districtid,NEW.regionid,NEW.doj,NEW.accountno,NEW.nominee,NEW.nomineerelationid,NEW.rankid,NEW.corpsid,NEW.tradeid,NEW.esmidno,NEW.characterid,NEW.regionid,NEW.casteid,NEW.civilqual,NEW.armyqual,NEW.registrationno,NEW.texcono,NEW.uanno,NEW.panno,NEW.serviceno,NEW.lastaccess,NEW.dependentstatus,NEW.dependentname,NEW.nationality,NEW.pfno,NEW.pfname,NEW.active,NEW.repcostatus,NEW.pfstatus,NEW.reasonforblock,NOW(),NEW.changedby,NEW.texconogdate,NEW.lastattendance,NEW.atmonthandyear,NEW.attendancesaved);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_member_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_member_before_update` BEFORE UPDATE ON `member` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.member
VALUES(OLD.memberid,OLD.firstname,OLD.lastname,OLD.fathername,OLD.dob,OLD.email,OLD.mobile,OLD.branchcode,OLD.branchname,OLD.ifsccode,OLD.address,OLD.village,OLD.talukid,OLD.stateid,OLD.countryid,OLD.pincode,OLD.addressstatus,OLD.communicationaddress,OLD.aadhaarno,OLD.genderid,OLD.districtid,OLD.regionid,OLD.doj,OLD.accountno,OLD.nominee,OLD.nomineerelationid,OLD.rankid,OLD.corpsid,OLD.tradeid,OLD.esmidno,OLD.characterid,OLD.regionid,OLD.casteid,OLD.civilqual,OLD.armyqual,OLD.registrationno,OLD.texcono,OLD.uanno,OLD.panno,OLD.serviceno,OLD.lastaccess,OLD.dependentstatus,OLD.dependentname,OLD.nationality,OLD.pfno,OLD.pfname,OLD.active,OLD.repcostatus,OLD.pfstatus,OLD.reasonforblock,NOW(),OLD.changedby,OLD.texconogdate,OLD.lastattendance,OLD.atmonthandyear,OLD.attendancesaved);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_officer_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_officer_after_insert` AFTER INSERT ON `officer` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.officer
VALUES(NEW.officerid,NEW.name,NEW.designation,NEW.email,NEW.mobile,NEW.phone,NEW.address,NEW.sortorder,NEW.other,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_officer_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_officer_before_update` BEFORE UPDATE ON `officer` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.officer
VALUES(OLD.officerid,OLD.name,OLD.designation,OLD.email,OLD.mobile,OLD.phone,OLD.address,OLD.sortorder,OLD.other,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_pending_dues_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_pending_dues_after_insert` AFTER INSERT ON `pending_dues` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.pending_dues
VALUES(NEW.due_id,NEW.date,NEW.ref_no,NEW.project_name,NEW.opening_amount,NEW.pending_amount,NEW.due_on,NEW.over_due_by_days,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_pending_dues_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_pending_dues_before_update` BEFORE UPDATE ON `pending_dues` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.pending_dues
VALUES(OLD.due_id,OLD.date,OLD.ref_no,OLD.project_name,OLD.opening_amount,OLD.pending_amount,OLD.due_on,OLD.over_due_by_days,OLD.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_postedlist_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_postedlist_after_insert` AFTER INSERT ON `postedlist` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.postedlist
VALUES(NEW.texcono,NEW.serviceno,NEW.name,NEW.projectno,NEW.category,NEW.startdate,NEW.memberid,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_postedlist_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_postedlist_before_update` BEFORE UPDATE ON `postedlist` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.postedlist
VALUES(OLD.texcono,OLD.serviceno,OLD.name,OLD.projectno,OLD.category,OLD.startdate,OLD.memberid,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_project_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_project_after_insert` AFTER INSERT ON `project` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.project
VALUES(NEW.projectid,NEW.clientid,NEW.projectno,NEW.name,NEW.districtid,NEW.regionid,NEW.statusid,NEW.talukid,NEW.categoryid,NEW.subcategoryid,NEW.designation,NEW.addressline1,NEW.addressline2,NEW.addressline3,NEW.pincode,NEW.active,NEW.amstatus,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_project_ams_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_project_ams_after_insert` AFTER INSERT ON `project_ams` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.project_ams
VALUES(NEW.projecttempid,NEW.projectid,NEW.clientid,NEW.projectno,NEW.name,NEW.districtid,NEW.regionid,NEW.statusid,NEW.talukid,NEW.categoryid,NEW.subcategoryid,NEW.designation,NEW.addressline1,NEW.addressline2,NEW.addressline3,NEW.pincode,NEW.active,NEW.amstatus,NOW(),NEW.changedby,NEW.updatedfields);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_project_ams_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_project_ams_before_update` BEFORE UPDATE ON `project_ams` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.project_ams
VALUES(OLD.projecttempid,OLD.projectid,OLD.clientid,OLD.projectno,OLD.name,OLD.districtid,OLD.regionid,OLD.statusid,OLD.talukid,OLD.categoryid,OLD.subcategoryid,OLD.designation,OLD.addressline1,OLD.addressline2,OLD.addressline3,OLD.pincode,OLD.active,OLD.amstatus,NOW(),OLD.changedby,OLD.updatedfields);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_project_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_project_before_update` BEFORE UPDATE ON `project` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.project
VALUES(OLD.projectid,OLD.clientid,OLD.projectno,OLD.name,OLD.districtid,OLD.regionid,OLD.statusid,OLD.talukid,OLD.categoryid,OLD.subcategoryid,OLD.designation,OLD.addressline1,OLD.addressline2,OLD.addressline3,OLD.pincode,OLD.active,OLD.amstatus,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_region_details_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_region_details_after_insert` AFTER INSERT ON `region_details` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.region_details
VALUES(NEW.id,NEW.rg_id,NEW.dt_id,NEW.tk_id,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_region_details_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_region_details_before_update` BEFORE UPDATE ON `region_details` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.region_details
VALUES(OLD.id,OLD.rg_id,OLD.dt_id,OLD.tk_id,OLD.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_regno_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_regno_after_insert` AFTER INSERT ON `regno` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.regno
VALUES(NEW.regnoid,NEW.dummy,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_regno_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_regno_before_update` BEFORE UPDATE ON `regno` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.regno
VALUES(OLD.regnoid,OLD.dummy,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_resetpassword_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_resetpassword_after_insert` AFTER INSERT ON `resetpassword` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.resetpassword
VALUES(NEW.resetpasswordid,NEW.employeeid,NEW.clientid,NEW.requesteddate,NEW.expirydate,NEW.verificationcode,NEW.token,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_resetpassword_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_resetpassword_before_update` BEFORE UPDATE ON `resetpassword` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.resetpassword
VALUES(OLD.resetpasswordid,OLD.employeeid,OLD.clientid,OLD.requesteddate,OLD.expirydate,OLD.verificationcode,OLD.token,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_rolepermission_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_rolepermission_after_insert` AFTER INSERT ON `rolepermission` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.rolepermission
VALUES(NEW.rolepermissionid,NEW.roleid,NEW.code,NEW.name,NEW.selected,NEW.sortorder,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_rolepermission_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_rolepermission_before_update` BEFORE UPDATE ON `rolepermission` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.rolepermission
VALUES(OLD.rolepermissionid,OLD.roleid,OLD.code,OLD.name,OLD.selected,OLD.sortorder,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_role_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_role_after_insert` AFTER INSERT ON `role` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.role
VALUES(NEW.roleid,NEW.name,NEW.description,NEW.email,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_role_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_role_before_update` BEFORE UPDATE ON `role` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.role
VALUES(OLD.roleid,OLD.name,OLD.description,OLD.email,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_salaryslip_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_salaryslip_after_insert` AFTER INSERT ON `salaryslip` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.salaryslip
VALUES(NEW.salaryslipid,NEW.payslipno,NEW.individualpayslipno,NEW.clientid,NEW.projectid,NEW.memberid,NEW.jobpostingdetailid,NEW.jobmasterid,NEW.attendanceid,NEW.monthandyear,NEW.presentdays,NEW.eddays,NEW.othours,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.ncbasic,NEW.edamount,NEW.basic,NEW.da,NEW.hra,NEW.cca,NEW.ma,NEW.epf,NEW.edli,NEW.admchr,NEW.bonus,NEW.gratuity,NEW.unifdt,NEW.leapay,NEW.conveyance,NEW.washallow,NEW.lic,NEW.grossamount,NEW.netpay,NEW.other1,NEW.other2,NEW.otherallowance,NEW.otherdeductions,NEW.ratehd,NEW.active,NEW.status,NEW.arrearstatus,NEW.fk_salary_slip_id,NEW.bill_type,NEW.ss_print_count,NEW.pf_print_count,NEW.ind_print_count,NEW.cashier_rejected,NEW.cao_rejected,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_salaryslip_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_salaryslip_before_update` BEFORE UPDATE ON `salaryslip` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.salaryslip
VALUES(OLD.salaryslipid,OLD.payslipno,OLD.individualpayslipno,OLD.clientid,OLD.projectid,OLD.memberid,OLD.jobpostingdetailid,OLD.jobmasterid,OLD.attendanceid,OLD.monthandyear,OLD.presentdays,OLD.eddays,OLD.othours,OLD.wagetypeid,OLD.wageyearid,OLD.wageareaid,OLD.wagecategoryid,OLD.ncbasic,OLD.edamount,OLD.basic,OLD.da,OLD.hra,OLD.cca,OLD.ma,OLD.epf,OLD.edli,OLD.admchr,OLD.bonus,OLD.gratuity,OLD.unifdt,OLD.leapay,OLD.conveyance,OLD.washallow,OLD.lic,OLD.grossamount,OLD.netpay,OLD.other1,OLD.other2,OLD.otherallowance,OLD.otherdeductions,OLD.ratehd,OLD.active,OLD.status,OLD.arrearstatus,OLD.fk_salary_slip_id,OLD.bill_type,OLD.ss_print_count,OLD.pf_print_count,OLD.ind_print_count,OLD.cashier_rejected,OLD.cao_rejected,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_salaryslip_difference_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_salaryslip_difference_after_insert` AFTER INSERT ON `salaryslip_difference` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.salaryslip_difference
VALUES(NEW.differenceid,NEW.salaryslipid,NEW.payslipno,NEW.individualpayslipno,NEW.clientid,NEW.projectid,NEW.memberid,NEW.jobpostingdetailid,NEW.jobmasterid,NEW.attendanceid,NEW.monthandyear,NEW.presentdays,NEW.eddays,NEW.othours,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.ncbasic,NEW.edamount,NEW.basic,NEW.da,NEW.hra,NEW.cca,NEW.ma,NEW.epf,NEW.edli,NEW.admchr,NEW.bonus,NEW.gratuity,NEW.unifdt,NEW.leapay,NEW.conveyance,NEW.washallow,NEW.lic,NEW.grossamount,NEW.netpay,NEW.other1,NEW.other2,NEW.ratehd,NEW.active,NEW.status,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_salaryslip_difference_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_salaryslip_difference_before_update` BEFORE UPDATE ON `salaryslip_difference` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.salaryslip_difference
VALUES(OLD.differenceid,OLD.salaryslipid,OLD.payslipno,OLD.individualpayslipno,OLD.clientid,OLD.projectid,OLD.memberid,OLD.jobpostingdetailid,OLD.jobmasterid,OLD.attendanceid,OLD.monthandyear,OLD.presentdays,OLD.eddays,OLD.othours,OLD.wagetypeid,OLD.wageyearid,OLD.wageareaid,OLD.wagecategoryid,OLD.ncbasic,OLD.edamount,OLD.basic,OLD.da,OLD.hra,OLD.cca,OLD.ma,OLD.epf,OLD.edli,OLD.admchr,OLD.bonus,OLD.gratuity,OLD.unifdt,OLD.leapay,OLD.conveyance,OLD.washallow,OLD.lic,OLD.grossamount,OLD.netpay,OLD.other1,OLD.other2,OLD.ratehd,OLD.active,OLD.status,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_setting_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_setting_after_insert` AFTER INSERT ON `setting` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.setting
VALUES(NEW.settingid,NEW.code,NEW.description,NEW.value,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_setting_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_setting_before_update` BEFORE UPDATE ON `setting` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.setting
VALUES(OLD.settingid,OLD.code,OLD.description,OLD.value,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_subcategory_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_subcategory_after_insert` AFTER INSERT ON `subcategory` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.subcategory
VALUES(NEW.subcategoryid,NEW.categoryid,NEW.subcategoryname,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_subcategory_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_subcategory_before_update` BEFORE UPDATE ON `subcategory` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.subcategory
VALUES(OLD.subcategoryid,OLD.categoryid,OLD.subcategoryname,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_texconologs_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_texconologs_after_insert` AFTER INSERT ON `texconologs` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.texconologs
VALUES(NEW.texconologid,NEW.texcono,NEW.memberid,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_texconologs_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_texconologs_before_update` BEFORE UPDATE ON `texconologs` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.texconologs
VALUES(OLD.texconologid,OLD.texcono,OLD.memberid,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_wages_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_wages_after_insert` AFTER INSERT ON `wages` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.wages
VALUES(NEW.wageid,NEW.wagetypeid,NEW.wagecategoryid,NEW.wageyearid,NEW.wageareaid,NEW.jobmasterid,NEW.particularid,NEW.particularamount,NEW.particularpercent,NEW.active,NOW(),NEW.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_wages_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_wages_before_update` BEFORE UPDATE ON `wages` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.wages
VALUES(OLD.wageid,OLD.wagetypeid,OLD.wagecategoryid,OLD.wageyearid,OLD.wageareaid,OLD.jobmasterid,OLD.particularid,OLD.particularamount,OLD.particularpercent,OLD.active,NOW(),OLD.changedby);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_wage_category_master_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_wage_category_master_after_insert` AFTER INSERT ON `wage_category_master` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.wage_category_master
VALUES(NEW.category_id,NEW.category_code,NEW.category_description,NEW.wageyearid,NEW.wagetypeid,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_wage_category_master_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_wage_category_master_before_update` BEFORE UPDATE ON `wage_category_master` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.wage_category_master
VALUES(NEW.category_id,NEW.category_code,NEW.category_description,NEW.wageyearid,NEW.wagetypeid,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_wage_login_details_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_wage_login_details_after_insert` AFTER INSERT ON `wage_login_details` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.wage_login_details
VALUES(NEW.ref_id,NEW.login_status,NEW.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger texco_uat_live.zz_trigger_wage_login_details_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `zz_trigger_wage_login_details_before_update` BEFORE UPDATE ON `wage_login_details` FOR EACH ROW BEGIN
INSERT INTO texco_audit_trail.wage_login_details
VALUES(OLD.ref_id,OLD.login_status,OLD.active,NOW());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
