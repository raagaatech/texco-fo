agreement  

BEGIN
INSERT INTO texco_audit_trail.agreement
VALUES(NEW.agreementid,NEW.clientid,NEW.fromdate,NEW.todate,NEW.servicecharge,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.agreementstatusid,NEW.agreementtypeid,NEW.agtypeid,NEW.tax,NEW.taxtype,NEW.particularid,NEW.optionaltype,NEW.addendum,NEW.active,NEW.amstatus,NEW.addendumstatus,NEW.changedby,NOW());
END

agreement_ams

BEGIN
INSERT INTO texco_audit_trail.agreement
VALUES(NEW.agreementtempid,NEW.agreementid,NEW.clientid,NEW.fromdate,NEW.todate,NEW.servicecharge,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.agreementstatusid,NEW.agreementtypeid,NEW.agtypeid,NEW.tax,NEW.taxtype,NEW.particularid,NEW.optionaltype,NEW.addendum,NEW.active,NEW.amstatus,NEW.addendumstatus,NEW.changedby,NEW.updatedfields,NOW());
END

agreementdetail

BEGIN
INSERT INTO texco_audit_trail.agreementdetail
VALUES(NEW.agreementdetailid,NEW.agreementinfoid,NEW.jobmasterid,NEW.numberofvacancies,NEW.salary,NEW.addendum,NEW.active,NEW.amstatus,NOW(),NEW.changedby);
END

agreementdetail_ams

BEGIN
INSERT INTO texco_audit_trail.agreementdetail_ams
VALUES(NEW.agreementdetailid,NEW.agreementinfoid,NEW.jobmasterid,NEW.numberofvacancies,NEW.salary,NEW.addendum,NEW.active,NEW.amstatus,NEW.amstatusupdated,NOW(),NEW.changedby);
END

agreementinfo

BEGIN
INSERT INTO texco_audit_trail.agreementinfo
VALUES(NEW.agreementinfoid,NEW.agreementid,NEW.projectid,NEW.active,NOW(),NEW.changedby);
END

agreement_draft

BEGIN
INSERT INTO texco_audit_trail.agreement_draft
VALUES(NEW.agdraftid,NEW.agreementid,NEW.agvalue,NEW.agtype,NEW.active,NOW());
END

allemployees

BEGIN
INSERT INTO texco_audit_trail.allemployees
VALUES(NEW.employeeid,NEW.projno,NEW.region,NEW.texcono,NEW.name,NEW.fathername,NEW.sex,NEW.genderid,NEW.dob,NEW.doj,NEW.district,NEW.address1,NEW.address2,NEW.village,NEW.pincode,NEW.mobile,NEW.civilqual,NEW.rank,NEW.corps,NEW.trade,NEW.armyqual,NEW.category,NEW.designation,NEW.pfno,NEW.entrydate,NEW.serviceno,NEW.esmidcardno,NEW.nominee,NEW.pfname,NEW.lastaccess,NEW.religion,NEW.community,NEW.nationality,NEW.active,NOW());
END


attendance

BEGIN
INSERT INTO texco_audit_trail.attendance
VALUES(NEW.attendanceid,NEW.clientid,NEW.projectid,NEW.memberid,NEW.jobpostingdetailid,NEW.jobmasterid,NEW.monthandyear,NEW.one,NEW.two,NEW.three,NEW.four,NEW.five,NEW.six,NEW.seven,NEW.eight,NEW.nine,NEW.ten,NEW.eleven,NEW.twelve,NEW.thirteen,NEW.fourteen,NEW.fifteen,NEW.sixteen,NEW.seventeen,NEW.eighteen,NEW.nineteen,NEW.twenty,NEW.twentyone,NEW.twentytwo,NEW.twentythree,NEW.twentyfour,NEW.twentyfive,NEW.twentysix,NEW.twentyseven,NEW.twentyeight,NEW.twentynine,NEW.thirty,NEW.thirtyone,NEW.presentdays,NEW.eddays,NEW.reason,NEW.datedon,NEW.athold,NEW.edhold,NEW.lreserve,NEW.othours,NEW.status,NEW.edstatus,NEW.active,NEW.cashier_rejected,NEW.cao_rejected,NEW.attendancesubmitted,NEW.attendancereviewed,NEW.attendancesaved,NOW(),NEW.changedby);
END

attendance_other_expense

BEGIN
INSERT INTO texco_audit_trail.attendance_other_expense
VALUES(NEW.attendance_other_expense_id,NEW.attendanceid,NEW.memberid,NEW.projectid,NEW.expense_type,NEW.monthandyear,NEW.amount,NEW.changedby,NOW());
END

bankslip

BEGIN
INSERT INTO texco_audit_trail.bankslip
VALUES(NEW.bankslipid,NEW.passorderno,NEW.status,NEW.active,NEW.changedby,NOW());
END

bank_slip_details

BEGIN
INSERT INTO texco_audit_trail.bank_slip_details
VALUES(NEW.bankdetailsid,NEW.bankslipid,NEW.salaryslipno,NEW.invoiceid,NOW());
END

carryforward

BEGIN
INSERT INTO texco_audit_trail.carryforward
VALUES(NEW.carryforwardid,NEW.clientid,NEW.projectid,NEW.jobmasterid,NEW.numberofvacancies,NEW.balancevacancies,NEW.comments,NEW.inplaceof,NEW.active,NOW(),NEW.changedby);
END


category

BEGIN
INSERT INTO texco_audit_trail.category
VALUES(NEW.categoryid,NEW.maincategoryid,NEW.categoryname,NEW.active,NOW(),NEW.changedby);
END

closed_project_members

BEGIN
INSERT INTO texco_audit_trail.closed_project_members
VALUES(NEW.closedid,NEW.memberid,NEW.texcono,NEW.serviceno,NEW.firstname,NEW.isselected,NEW.isapplied,NEW.active,NOW());
END


director

BEGIN
INSERT INTO texco_audit_trail.director
VALUES(NEW.directorid,NEW.name,NEW.designation,NEW.dtitleid,NEW.email,NEW.mobile,NEW.phone,NEW.address,NEW.active,NOW(),NEW.changedby);
END


document

BEGIN
INSERT INTO texco_audit_trail.document
VALUES(NEW.documentid,NEW.name,NEW.icon,NEW.iconcolour,NEW.documentname,NEW.isgst,NEW.iswage,NEW.ispf,NEW.folderid,NEW.foldername,NEW.description,NEW.active,NOW(),NEW.changedby);
END


documentfolder

BEGIN
INSERT INTO texco_audit_trail.documentfolder
VALUES(NEW.folderid,NEW.foldername,NEW.status,NOW());
END

due

BEGIN
INSERT INTO texco_audit_trail.due
VALUES(NEW.dueid,NEW.clientid,NEW.projectid,NEW.monthandyear,NEW.invoiceid,NEW.invoiceno,NEW.dueopening,NEW.duepaid,NEW.duepending,NEW.paiddate,NEW.status,NEW.active,NOW(),NEW.changedby);
END

employee

BEGIN
INSERT INTO texco_audit_trail.employee
VALUES(NEW.employeeid,NEW.employeeno,NEW.firstname,NEW.lastname,NEW.regionid,NEW.desigid,NEW.email,NEW.mobile,NEW.phone,NEW.doj,NEW.address,NEW.roleid,NEW.password,NEW.active,NOW(),NEW.changedby);
END

gallery

BEGIN
INSERT INTO texco_audit_trail.gallery
VALUES(NEW.galleryid,NEW.group,NEW.description,NEW.image,NEW.active,NOW(),NEW.changedby);
END

invoice

BEGIN
INSERT INTO texco_audit_trail.invoice
VALUES(NEW.invoiceid,NEW.clientid,NEW.projectid,NEW.monthandyear,NEW.servicecharge,NEW.tax,NEW.servicecharges,NEW.servicetax,NEW.subtotal,NEW.totalamount,NEW.invoiceno,NEW.paymentutrno,NEW.arrearstatus,NEW.invoicestatus,NEW.paidamount,NEW.fkinvoiceid,NEW.type,NEW.printcount,NEW.cashier_rejected,NEW.cao_rejected,NEW.active,NOW(),NEW.changedby);
END

invoicedetail

BEGIN
INSERT INTO texco_audit_trail.invoicedetail
VALUES(NEW.invoicedetailid,NEW.invoiceid,NEW.projectid,NEW.clientid,NEW.jobmasterid,NEW.salary,NEW.noofduties,NEW.salaryamount,NEW.monthandyear,NEW.days,NEW.noofperson,NEW.fk_invoice_id,NEW.type,NEW.active,NEW.changedby,NOW());
END


jobactivity

BEGIN
INSERT INTO texco_audit_trail.jobactivity
VALUES(NEW.jobactivityid,NEW.jobpostingdetailid,NEW.memberid,NEW.clientid,NEW.projectid,NEW.currentvacancies,NEW.jobstatuscode,NEW.texcono,NEW.registrationno,NEW.inplace,NEW.ipaddress,NEW.othercat,NEW.effectivedate,NEW.active,NEW.isrejected,NOW(),NEW.changedby);
END

jobactivityhistory

BEGIN
INSERT INTO texco_audit_trail.jobactivityhistory
VALUES(NEW.jobactivityhistoryid,NEW.jobactivityid,NEW.jobstatuscode,NEW.ondate,NEW.comments,NEW.reason,NEW.active,NOW(),NEW.changedby);
END


jobeligibility

BEGIN
INSERT INTO texco_audit_trail.jobeligibility
VALUES(NEW.jobegid,NEW.jobmasterid,NEW.rank,NEW.active,NOW(),NEW.changedby);
END

jobmaster

BEGIN
INSERT INTO texco_audit_trail.jobmaster
VALUES(NEW.jobmasterid,NEW.code,NEW.name,NOW.workinghours,NEW.monthlywages,NEW.servicecharge,NEW.servicetax,NEW.comments,NEW.active,NEW.sortorder,NOW(),NEW.changedby);
END

jobposting

BEGIN
INSERT INTO texco_audit_trail.jobposting
VALUES(NEW.jobpostingid,NEW.clientid,NEW.projectid,NEW.active,NOW(),NEW.changedby);
END

jobpostingdate

BEGIN
INSERT INTO texco_audit_trail.jobpostingdate
VALUES(NEW.id,NEW.importdate,NEW.closedate,NEW.activedate,NEW.inactivedate,NEW.opendate,NEW.registrationno,NOW());
END

jobpostingdetail

BEGIN
INSERT INTO texco_audit_trail.jobpostingdetail
VALUES(NEW.jobpostingdetailid,NEW.jobpostingid,NEW.jobmasterid,NEW.numberofvacancies,NEW.filledvacancies,NEW.waitingvacancies,NEW.agvacancies,NEW.comments,NEW.inplace,NEW.posteddate,NEW.startdate,NEW.enddate,NEW.close,NEW.closedate,NEW.active,NEW.appliedstatus,NOW(),NEW.changedby);
END


logs

BEGIN
INSERT INTO texco_audit_trail.logs
VALUES(NEW.logid,NEW.method,NEW.data,NOW(),NEW.changedby);
END

lookupdomain

BEGIN
INSERT INTO texco_audit_trail.lookupdomain
VALUES(NEW.lkdmnid,NEW.code,NEW.description,NOW(),NEW.changedby);
END

lookupvalue

BEGIN
INSERT INTO texco_audit_trail.lookupvalue
VALUES(NEW.lkvalid,NEW.lkdmncode,NEW.code,NEW.description,NEW.sortorder,NEW.default,NEW.active,NOW(),NEW.changedby);
END

lookupvaluelink

BEGIN
INSERT INTO texco_audit_trail.lookupvaluelink
VALUES(NEW.linkid,NEW.from_lkvalid,NEW.to_lkvalid,NEW.active,NOW(),NEW.changedby);
END

member

BEGIN
INSERT INTO texco_audit_trail.member
VALUES(NEW.memberid,NEW.firstname,NEW.lastname,NEW.fathername,NEW.dob,NEW.email,NEW.mobile,NEW.branchcode,NEW.branchname,NEW.ifsccode,NEW.address,NEW.village,NEW.talukid,NEW.stateid,NEW.countryid,NEW.pincode,NEW.addressstatus,NEW.communicationaddress,NEW.aadhaarno,NEW.genderid,NEW.districtid,NEW.regionid,NEW.doj,NEW.accountno,NEW.nominee,NEW.nomineerelationid,NEW.rankid,NEW.corpsid,NEW.tradeid,NEW.esmidno,NEW.characterid,NEW.regionid,NEW.casteid,NEW.civilqual,NEW.armyqual,NEW.registrationno,NEW.texcono,NEW.uanno,NEW.panno,NEW.serviceno,NEW.lastaccess,NEW.dependentstatus,NEW.dependentname,NEW.nationality,NEW.pfno,NEW.pfname,NEW.active,NEW.repcostatus,NEW.pfstatus,NEW.reasonforblock,NOW(),NEW.changedby,NEW.texconogdate,NEW.lastattendance,NEW.atmonthandyear,NEW.attendancesaved);
END

memberblock

BEGIN
INSERT INTO texco_audit_trail.memberblock
VALUES(NEW.memberblockid,NEW.memberid,NEW.reason,NEW.comment,NEW.enddate,NEW.ispfblock,NEW.isrepcoblock,NEW.lifetimeblock,NEW.active,NOW(),NEW.changedby);
END


memberhistory

BEGIN
INSERT INTO texco_audit_trail.memberhistory
VALUES(NEW.memberhistoryid,NEW.texcono,NEW.memberid,NEW.projectno,NEW.projectid,NEW.category,NEW.startdate,NEW.enddate,NEW.active,NEW.estype,NEW.changedby,NEW.approvalstatus,NEW.joining_date,NEW.closing_date,NEW.isrejected,NEW.working_status,NEW.isvacancyadded,NOW());
END

officer

BEGIN
INSERT INTO texco_audit_trail.officer
VALUES(NEW.officerid,NEW.name,NEW.designation,NEW.email,NEW.mobile,NEW.phone,NEW.address,NEW.sortorder,NEW.other,NEW.active,NOW(),NEW.changedby);
END

pending_dues

BEGIN
INSERT INTO texco_audit_trail.pending_dues
VALUES(NEW.due_id,NEW.date,NEW.ref_no,NEW.project_name,NEW.opening_amount,NEW.pending_amount,NEW.due_on,NEW.over_due_by_days,NEW.active,NOW());
END

postedlist

BEGIN
INSERT INTO texco_audit_trail.postedlist
VALUES(NEW.texcono,NEW.serviceno,NEW.name,NEW.projectno,NEW.category,NEW.startdate,NEW.memberid,NOW());
END

project  

BEGIN
INSERT INTO texco_audit_trail.project
VALUES(NEW.projectid,NEW.clientid,NEW.projectno,NEW.name,NEW.districtid,NEW.regionid,NEW.statusid,NEW.talukid,NEW.categoryid,NEW.subcategoryid,NEW.designation,NEW.addressline1,NEW.addressline2,NEW.addressline3,NEW.pincode,NEW.active,NEW.amstatus,NOW(),NEW.changedby);
END

project_ams  

BEGIN
INSERT INTO texco_audit_trail.project_ams
VALUES(NEW.projecttempid,NEW.projectid,NEW.clientid,NEW.projectno,NEW.name,NEW.districtid,NEW.regionid,NEW.statusid,NEW.talukid,NEW.categoryid,NEW.subcategoryid,NEW.designation,NEW.addressline1,NEW.addressline2,NEW.addressline3,NEW.pincode,NEW.active,NEW.amstatus,NOW(),NEW.changedby,NEW.updatedfields);
END

region_details

BEGIN
INSERT INTO texco_audit_trail.region_details
VALUES(NEW.id,NEW.rg_id,NEW.dt_id,NEW.tk_id,NEW.active,NOW());
END

regno

BEGIN
INSERT INTO texco_audit_trail.regno
VALUES(NEW.regnoid,NEW.dummy,NOW());
END


resetpassword

BEGIN
INSERT INTO texco_audit_trail.resetpassword
VALUES(NEW.resetpasswordid,NEW.employeeid,NEW.clientid,NEW.requesteddate,NEW.expirydate,NEW.verificationcode,NEW.token,NEW.active,NOW(),NEW.changedby);
END

role

BEGIN
INSERT INTO texco_audit_trail.role
VALUES(NEW.roleid,NEW.name,NEW.description,NEW.email,NEW.active,NOW(),NEW.changedby);
END


rolepermission

BEGIN
INSERT INTO texco_audit_trail.rolepermission
VALUES(NEW.rolepermissionid,NEW.roleid,NEW.code,NEW.name,NEW.selected,NEW.sortorder,NEW.active,NOW(),NEW.changedby);
END

salaryslip

BEGIN
INSERT INTO texco_audit_trail.salaryslip
VALUES(NEW.salaryslipid,NEW.payslipno,NEW.individualpayslipno,NEW.clientid,NEW.projectid,NEW.memberid,NEW.jobpostingdetailid,NEW.jobmasterid,NEW.attendanceid,NEW.monthandyear,NEW.presentdays,NEW.eddays,NEW.othours,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.ncbasic,NEW.edamount,NEW.basic,NEW.da,NEW.hra,NEW.cca,NEW.ma,NEW.epf,NEW.edli,NEW.admchr,NEW.bonus,NEW.gratuity,NEW.unifdt,NEW.leapay,NEW.conveyance,NEW.washallow,NEW.lic,NEW.grossamount,NEW.netpay,NEW.other1,NEW.other2,NEW.otherallowance,NEW.otherdeductions,NEW.ratehd,NEW.active,NEW.status,NEW.arrearstatus,NEW.fk_salary_slip_id,NEW.bill_type,NEW.ss_print_count,NEW.pf_print_count,NEW.ind_print_count,NEW.cashier_rejected,NEW.cao_rejected,NOW(),NEW.changedby);
END

salaryslip_difference

BEGIN
INSERT INTO texco_audit_trail.salaryslip_difference
VALUES(NEW.differenceid,NEW.salaryslipid,NEW.payslipno,NEW.individualpayslipno,NEW.clientid,NEW.projectid,NEW.memberid,NEW.jobpostingdetailid,NEW.jobmasterid,NEW.attendanceid,NEW.monthandyear,NEW.presentdays,NEW.eddays,NEW.othours,NEW.wagetypeid,NEW.wageyearid,NEW.wageareaid,NEW.wagecategoryid,NEW.ncbasic,NEW.edamount,NEW.basic,NEW.da,NEW.hra,NEW.cca,NEW.ma,NEW.epf,NEW.edli,NEW.admchr,NEW.bonus,NEW.gratuity,NEW.unifdt,NEW.lepay,NEW.conveyance,NEW.washallow,NEW.lic,NEW.grossamount,NEW.netpay,NEW.other1,NEW.other2,NEW.ratehd,NEW.active,NEW.status,NOW(),NEW.changedby);
END

setting

BEGIN
INSERT INTO texco_audit_trail.setting
VALUES(NEW.settingid,NEW.code,NEW.description,NEW.value,NEW.active,NOW(),NEW.changedby);
END

subcategory

BEGIN
INSERT INTO texco_audit_trail.subcategory
VALUES(NEW.subcategoryid,NEW.categoryid,NEW.subcategoryname,NEW.active,NOW(),NEW.changedby);
END

texconologs

BEGIN
INSERT INTO texco_audit_trail.texconologs
VALUES(NEW.texconologid,NEW.texcono,NEW.memberid,NEW.active,NOW(),NEW.changedby);
END



wages

BEGIN
INSERT INTO texco_audit_trail.wages
VALUES(NEW.wageid,NEW.wagetypeid,NEW.wagecategoryid,NEW.wageyearid,NEW.wageareaid,NEW.jobmasterid,NEW.particularid,NEW.particularamount,NEW.particularpercent,NEW.active,NOW(),NEW.changedby);
END

wage_category_master

BEGIN
INSERT INTO texco_audit_trail.wage_category_master
VALUES(NEW.category_id,NEW.category_code,NEW.category_description,NEW.wageyearid,NEW.wagetypeid,NEW.active,NOW());
END

wage_login_details

BEGIN
INSERT INTO texco_audit_trail.wage_login_details
VALUES(NEW.ref_id,NEW.login_status,NEW.active,NOW());
END