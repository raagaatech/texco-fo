var app = angular.module('appMemberHistoryForm', []).constant('_', window._);
app.constant("moment", moment);
app.controller('ctrlMemberHistoryForm', function($scope,$http) {

    $scope.members = [];
    $scope.selected = {};
    $scope.objmember = {};
    $scope.isshow = false;
    $scope.isDisabled = true;
    $scope.repcoblocks = [];
    $scope.jobmasters = [];

    // Get membershistory
    $http({ 
        method: 'GET', 
        headers : {
            "Authorization" : atoken
        },
        url: api_url + "/member/memberhistoryDetails",
    }).then(function successCallback(response) {
        if (response.status = 200) {
            $scope.members = response.data;
            if ($scope.members.length > 0) {
                $scope.select($scope.members[0]);
            }
        }
    }, function errorCallback(response) {
        Materialize.toast('Something has gone wrong!', 3000, 'red');
    }); 

    $scope.projectnos = [];
    $http({
        method: 'GET',
        headers: {
            'Authorization' : atoken
        }, 
        url: api_url + "/prjectnos",
    }).then(function successCallback(response) {
        if (response.status = 200) {
            $scope.projectnos = response.data;
        }
    }, function errorCallback(response) {
        Materialize.toast('Something has gone wrong!', 3000, 'red');
    }); 

    $http({
		method: 'GET', 
		headers : {
			"Authorization" : atoken
		},
		url: api_url + "/job/jobmaster",
	}).then(function successCallback(response) {
		if (response.status = 200) {
			$scope.jobmasters = response.data;
		}
	}, function errorCallback(response) {

    });
    

    //Get memberinfo
    $scope.searchmember = function(serviceno) {
        $scope.servicenovalid = {};
        $("#failure").html("");
        $http({
            url: api_url + "/memberinfo?texserno=" + serviceno,
            data: $.param({
                texserno: serviceno
            }), 
            headers : {
                "Authorization" : atoken
            },
            method: 'GET',
        }).success(function(response, result) {
            if (result = 200) {
                $scope.objmember.serviceno = response.serviceno;
                $scope.objmember.currenttexcono = response.texcono;
                $scope.objmember.texcono = response.texcono;
                $scope.objmember.mobile = response.mobile;
                $scope.objmember.firstname = response.firstname;
                $scope.objmember.memberid = response.memberid;
                $scope.objmember.memhistoryid = 0;
                $scope.objmember.category = '';
                $scope.objmember.projectno = '';
                $scope.isDisabled = false;
            }
        }).error(function(error) {
            Materialize.toast(error, 3000, 'red');
        });
    }

    $scope.select = function(member) { 
        debugger
        $scope.selected = member;
        //$scope.selected.startdate = moment(member.startdate).format('DD-MM-YYYY');
        // $scope.selected.enddate = moment(member.enddate).format('DD-MM-YYYY');
        $scope.objmember.memhistoryid = member.memhistoryid;
        $scope.objmember.memberid = member.memberid;
        $scope.objmember.firstname = member.firstname;
        $scope.objmember.mobile = member.mobile;
        $scope.objmember.texcono = member.texcono;
        $scope.objmember.jobmasterid = member.jobmasterid;
        $scope.objmember.category = member.category;
        $scope.objmember.projectno = member.projectno;
        $scope.objmember.projectid = member.projectid;
        $scope.objmember.currenttexcono = member.currenttexcono;
        $scope.objmember.serviceno = member.serviceno;  
        $scope.objmember.servicenos = member.serviceno;
        $scope.objmember.startdate = member.startdate;
        $scope.objmember.enddate =member.enddate;
    } 

    $scope.addmemberhistory = function() {
        $scope.isshow = true;
        $scope.objmember = {};
        $scope.submitted = false;
        $('#mcaption').text("Add MemberHistory");
        $('#modal2').modal('open');
        $("#failure").html("");
    } 

    $scope.getprojectno = function(projectid) { 
       
        var filteredrenewal = _.filter($scope.projectnos, function(item) {
            if(item.projectid == projectid) { 
                console.log('item.projectno', item.projectno);
                $scope.objmember.projectno = item.projectno;
            } 
        });  
    } 

    $scope.getjobmasterno = function(jobmasterid) {
        var filteredrenewal = _.filter($scope.jobmasters, function(item) {
            if(item.jobmasterid == jobmasterid) {
                $scope.objmember.category = item.code;
            }
        });  
    }

    $scope.savememberhistory = function(data) {
        debugger
        var method;
        if (data.memhistoryid > 0) {
            method = 'PUT';
        } else {
            method = 'POST';
        }
        $scope.searchmemberByID(data);
    };

    $scope.getmembersDetails = function() {
        $http({ 
            method: 'GET', 
            headers : {
                "Authorization" : atoken
            },
            url: api_url + "/member/memberhistoryDetails",
        }).then(function successCallback(response) {
            if (response.status = 200) {
                $scope.members = response.data;
                if ($scope.members.length > 0) {
                    $scope.select($scope.members[0]);
                }
            }
        }, function errorCallback(response) {
            Materialize.toast('Something has gone wrong!', 3000, 'red');
        }); 
    }

    $scope.searchmemberByID = function(data) {
        // console.log('data',data);
        $http({
            url: api_url + "/memberinfo?texserno=" + data.serviceno,
            data: $.param({
                texserno: data.serviceno
            }), 
            headers : {
                "Authorization" : atoken
            },
            method: 'GET',
        }).success(function(response, result) {
            if (result = 200) {
                debugger
                $scope.isDisabled = false;
                var method;
                if (data.memhistoryid > 0) {
                    method = 'PUT';
                } else {
                    method = 'POST';
                }
                $http({
                    url: api_url + "/members/memberhistory",
                    data: $.param({
                        "data": data
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization' : atoken
                    }, 
                    method: method,
                }).success(function(response, result) {
                    if (result = 200) {
                        $('#modal2').modal('close');
                        Materialize.toast('Memberhistory Saved Successfully!', 3000, 'green');
                        $scope.getmembersDetails();
                    }
                }).error(function(error) {
                    Materialize.toast(error, 5000, 'red');
                });
            }
        }).error(function(error) {
            Materialize.toast(error, 3000, 'red');
            $scope.isDisabled = true;
        });
    }

    $scope.editmemberhistory = function () { 
        $scope.isshow = false;
        $scope.submitted = false;
        $scope.functions = 'add';
        $scope.select($scope.selected);
        $('#mcaption').text("Edit MemberHistory");
        $('#modal2').modal('open');
        $("#failure").html("");
    } 

    $scope.removememberhistory = function() {
        if (confirm("Are you sure to delete this detail?")) {
            $http({
                url: api_url + "/members/history/updatestatus",
                data: $.param({
                    "memhistoryid": $scope.objmember.memhistoryid,
                    "changedby": "",
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "Authorization" : atoken
                    
                },
                method: "DELETE",
            }).success(function(response, result) {
                if (result = 200) {
                    Materialize.toast('Memberhistory Deleted Successfully', 3000, 'green');
                    $scope.getmembersDetails();
                }
            }).error(function(error) {
                $('#failure').html(error);
            });
        };
    }

});